﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.Text.RegularExpressions

Public Class XtraPieceEdit
    Dim pId As String
    Dim ulf As UserLookAndFeel
    Dim previousBranchName As String
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraBranchEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Dim xe As XtraBranch = New XtraBranch
        pId = XtraPieceMaster.PieceID

        If pId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If

    End Sub
    Private Sub SetDefaultValue()
        TextPCode.Enabled = True
        TextPCode.Text = ""
        TextPDes.Text = ""
        TextPRate.Text = ""
    End Sub
    Private Sub SetFormValue()
        TextPCode.Enabled = False
        Dim ds As DataSet = New DataSet
        Dim adap As SqlDataAdapter
        Dim adap1 As OleDbDataAdapter

        Dim sSql As String = "select * from tblPiece where Pcode ='" & pId & "'"
        If Common.servername = "Access" Then
            adap1 = New OleDbDataAdapter(sSql, Common.con1)
            adap1.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If

        TextPCode.Text = pId
        TextPDes.Text = ds.Tables(0).Rows(0).Item("Pname").ToString.Trim
        TextPRate.Text = ds.Tables(0).Rows(0).Item("Prate").ToString.Trim
        'previousBranchName = ds.Tables(0).Rows(0).Item("BRANCHNAME").ToString.Trim
    End Sub
   
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextPCode.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Piece Code cannot be empty</size>", "<size=9>Error</size>")
            TextPCode.Select()
            Exit Sub
        End If
        If TextPDes.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Description cannot be empty</size>", "<size=9>Error</size>")
            TextPDes.Select()
            Exit Sub
        End If
    
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim sSql, sSql1 As String
        If pId.Length = 0 Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            sSql = "select Pcode from tblPiece where Pcode = '" & TextPCode.Text.Trim & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Duplicate Piece Code</size>", "<size=9>Error</size>")
                TextPCode.Select()
                Exit Sub
            End If
            sSql = "INSERT into tblPiece (Pcode, Pname, Prate) VALUES('" & TextPCode.Text.Trim & "','" & TextPDes.Text.Trim & "','" & TextPRate.Text.Trim & "') "
        Else
            sSql = "Update tblPiece set Prate='" & TextPRate.Text.Trim & "', Pname ='" & TextPDes.Text.Trim & "' where Pcode ='" & pId & "'"
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()            
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.loadDevice()
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class