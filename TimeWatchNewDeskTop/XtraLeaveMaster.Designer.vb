﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLeaveMaster
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraLeaveMaster))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colLEAVEFIELD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVECODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colLEAVEDESCRIPTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colLEAVETYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colISOFFINCLUDE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.colISHOLIDAYINCLUDE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISLEAVEACCRUAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRESENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVELIMIT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFIXED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisMonthly = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisCompOffType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExpiryDays = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblLeaveMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.SSSDBDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblLeaveMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.LookAndFeel.SkinName = "iMaginary"
        Me.SplitContainerControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1, Me.RepositoryItemCheckEdit1, Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colLEAVEFIELD, Me.colLEAVECODE, Me.colLEAVEDESCRIPTION, Me.colLEAVETYPE, Me.colISOFFINCLUDE, Me.colISHOLIDAYINCLUDE, Me.colISLEAVEACCRUAL, Me.colSMIN, Me.colSMAX, Me.colPRESENT, Me.colLEAVE, Me.colLEAVELIMIT, Me.colFIXED, Me.colisMonthly, Me.colisCompOffType, Me.colLastModifiedDate, Me.colLastModifiedBy, Me.colExpiryDays})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Leave"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsEditForm.EditFormColumnCount = 1
        Me.GridView1.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'colLEAVEFIELD
        '
        Me.colLEAVEFIELD.Caption = "Leave Field"
        Me.colLEAVEFIELD.FieldName = "LEAVEFIELD"
        Me.colLEAVEFIELD.Name = "colLEAVEFIELD"
        Me.colLEAVEFIELD.Visible = True
        Me.colLEAVEFIELD.VisibleIndex = 0
        '
        'colLEAVECODE
        '
        Me.colLEAVECODE.Caption = "Leave Code"
        Me.colLEAVECODE.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colLEAVECODE.FieldName = "LEAVECODE"
        Me.colLEAVECODE.Name = "colLEAVECODE"
        Me.colLEAVECODE.Visible = True
        Me.colLEAVECODE.VisibleIndex = 1
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RepositoryItemTextEdit1.MaxLength = 3
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colLEAVEDESCRIPTION
        '
        Me.colLEAVEDESCRIPTION.Caption = "Description"
        Me.colLEAVEDESCRIPTION.ColumnEdit = Me.RepositoryItemTextEdit2
        Me.colLEAVEDESCRIPTION.FieldName = "LEAVEDESCRIPTION"
        Me.colLEAVEDESCRIPTION.Name = "colLEAVEDESCRIPTION"
        Me.colLEAVEDESCRIPTION.Visible = True
        Me.colLEAVEDESCRIPTION.VisibleIndex = 2
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RepositoryItemTextEdit2.Mask.EditMask = "[A-z ]*"
        Me.RepositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.RepositoryItemTextEdit2.MaxLength = 20
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'colLEAVETYPE
        '
        Me.colLEAVETYPE.Caption = "Leave Type"
        Me.colLEAVETYPE.ColumnEdit = Me.RepositoryItemComboBox1
        Me.colLEAVETYPE.FieldName = "LEAVETYPE"
        Me.colLEAVETYPE.Name = "colLEAVETYPE"
        Me.colLEAVETYPE.Visible = True
        Me.colLEAVETYPE.VisibleIndex = 3
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Leave", "Absent", "Present"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'colISOFFINCLUDE
        '
        Me.colISOFFINCLUDE.Caption = "Is WO Include"
        Me.colISOFFINCLUDE.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colISOFFINCLUDE.FieldName = "ISOFFINCLUDE"
        Me.colISOFFINCLUDE.Name = "colISOFFINCLUDE"
        Me.colISOFFINCLUDE.Visible = True
        Me.colISOFFINCLUDE.VisibleIndex = 4
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        Me.RepositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.RepositoryItemCheckEdit1.NullText = "N"
        Me.RepositoryItemCheckEdit1.ValueChecked = "Y"
        Me.RepositoryItemCheckEdit1.ValueGrayed = "N"
        Me.RepositoryItemCheckEdit1.ValueUnchecked = "N"
        '
        'colISHOLIDAYINCLUDE
        '
        Me.colISHOLIDAYINCLUDE.Caption = "Is Holiday Include"
        Me.colISHOLIDAYINCLUDE.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colISHOLIDAYINCLUDE.FieldName = "ISHOLIDAYINCLUDE"
        Me.colISHOLIDAYINCLUDE.Name = "colISHOLIDAYINCLUDE"
        Me.colISHOLIDAYINCLUDE.Visible = True
        Me.colISHOLIDAYINCLUDE.VisibleIndex = 5
        '
        'colISLEAVEACCRUAL
        '
        Me.colISLEAVEACCRUAL.Caption = "Is Leave Accrual"
        Me.colISLEAVEACCRUAL.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colISLEAVEACCRUAL.FieldName = "ISLEAVEACCRUAL"
        Me.colISLEAVEACCRUAL.Name = "colISLEAVEACCRUAL"
        Me.colISLEAVEACCRUAL.Visible = True
        Me.colISLEAVEACCRUAL.VisibleIndex = 6
        '
        'colSMIN
        '
        Me.colSMIN.FieldName = "SMIN"
        Me.colSMIN.Name = "colSMIN"
        '
        'colSMAX
        '
        Me.colSMAX.FieldName = "SMAX"
        Me.colSMAX.Name = "colSMAX"
        '
        'colPRESENT
        '
        Me.colPRESENT.FieldName = "PRESENT"
        Me.colPRESENT.Name = "colPRESENT"
        '
        'colLEAVE
        '
        Me.colLEAVE.FieldName = "LEAVE"
        Me.colLEAVE.Name = "colLEAVE"
        '
        'colLEAVELIMIT
        '
        Me.colLEAVELIMIT.FieldName = "LEAVELIMIT"
        Me.colLEAVELIMIT.Name = "colLEAVELIMIT"
        '
        'colFIXED
        '
        Me.colFIXED.FieldName = "FIXED"
        Me.colFIXED.Name = "colFIXED"
        '
        'colisMonthly
        '
        Me.colisMonthly.Caption = "Is Monthly"
        Me.colisMonthly.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colisMonthly.FieldName = "isMonthly"
        Me.colisMonthly.Name = "colisMonthly"
        Me.colisMonthly.Visible = True
        Me.colisMonthly.VisibleIndex = 7
        '
        'colisCompOffType
        '
        Me.colisCompOffType.Caption = "Is CompOff Type"
        Me.colisCompOffType.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colisCompOffType.FieldName = "isCompOffType"
        Me.colisCompOffType.Name = "colisCompOffType"
        Me.colisCompOffType.Visible = True
        Me.colisCompOffType.VisibleIndex = 8
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colExpiryDays
        '
        Me.colExpiryDays.FieldName = "ExpiryDays"
        Me.colExpiryDays.Name = "colExpiryDays"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblLeaveMasterBindingSource
        '
        Me.TblLeaveMasterBindingSource.DataMember = "tblLeaveMaster"
        Me.TblLeaveMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SSSDBDataSetBindingSource
        '
        Me.SSSDBDataSetBindingSource.DataSource = Me.SSSDBDataSet
        Me.SSSDBDataSetBindingSource.Position = 0
        '
        'TblLeaveMasterTableAdapter
        '
        Me.TblLeaveMasterTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'XtraLeaveMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraLeaveMaster"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblLeaveMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents SSSDBDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblLeaveMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter
    Friend WithEvents colLEAVEFIELD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVECODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVEDESCRIPTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOFFINCLUDE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISHOLIDAYINCLUDE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISLEAVEACCRUAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVETYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRESENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVELIMIT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFIXED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisMonthly As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisCompOffType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExpiryDays As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblLeaveMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit

End Class
