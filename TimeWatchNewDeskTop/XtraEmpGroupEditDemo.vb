﻿Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.ComponentModel
Imports System.Text
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraRichEdit.API.Word

Public Class XtraEmpGroupEditDemo

    Dim servername As String
    Dim con As SqlConnection
    Dim con1 As OleDbConnection
    Dim ConnectionString As String
    Dim adap1, adap As SqlDataAdapter
    Dim ds As DataSet
    Dim cmd As New SqlCommand
    Dim ulf As UserLookAndFeel

    Dim GrpId As String = ""

    Public Sub New(a As String)
        'MsgBox(a)
        GrpId = a
        InitializeComponent()

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True


        Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str As String
        Dim str1() As String
        Do While sr.Peek <> -1
            str = sr.ReadLine
            str1 = str.Split(",")
            servername = str1(0)
        Loop
        sr.Close()
        fs.Close()

        If Common.servername = "Access" Then
            ''ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            con = New SqlConnection(ConnectionString)
            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If

    End Sub
    Private Sub XtraTestDemo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEdit1.Text = GrpId
        'PopupContainerEdit1.QueryResultValue = (PopupContainerEdit1.QueryResultValue + popupContainerEdit1_QueryResultValue())
        'PopupContainerEdit1.QueryPopUp = (PopupContainerEdit1.QueryPopUp + popupContainerEdit1_QueryPopUp())
    End Sub


    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        con.Open()
        cmd = New SqlCommand("insert into EmployeeGroup (GroupId, GroupName) values('" & TextEdit1.Text & "','" & TextEdit2.Text & "')", con)
        cmd.ExecuteNonQuery()
        con.Close()

    End Sub

    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            For Each text As String In texts
                Dim rowHandle As Integer = GridView1.LocateByValue("SHIFT", text.Trim)
                MsgBox(GridView1.LocateByValue("SHIFT", text.Trim))
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub

    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        'Dim selectedRows() As Integer = GridView1.GetSelectedRows
        'Dim sb As StringBuilder = New StringBuilder
        'For Each selectionRow As Integer In selectedRows
        '    'Dim f As XtraEmpGroupEdit = CType(GridView1.GetRow(selectionRow), XtraEmpGroupEdit)
        '    'Dim f As GridView1.SSSDBDataSetTableAdapters.row = CType(GridView1.GetRow(selectionRow), GridRow)
        '    Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
        '    If (sb.ToString.Length > 0) Then
        '        sb.Append(", ")
        '    End If
        '    sb.Append(a.Item("SHIFT"))
        'Next
        'e.Value = sb.ToString
    End Sub

    Private Sub GridView1_SelectionChanged(sender As System.Object, e As DevExpress.Data.SelectionChangedEventArgs) Handles GridView1.SelectionChanged
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            'Dim f As XtraEmpGroupEdit = CType(GridView1.GetRow(selectionRow), XtraEmpGroupEdit)
            'Dim f As GridView1.SSSDBDataSetTableAdapters.row = CType(GridView1.GetRow(selectionRow), GridRow)
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("SHIFT"))
        Next
        PopupContainerEdit1.Text = sb.ToString
    End Sub
End Class
