﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraDBBackUpSetting
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraDBBackUpSetting))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.BtnAccessDB = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBoxEditNEpaliMonthDOB = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliYearDOB = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliDateDOB = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleSave = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.TextAccessBKLocation = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextBKFor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtAutoBackUpTimerTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleBKAuto = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextBKTimerDays = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.ToggleBKDelete = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextBKPath = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBKType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.ComboBoxEditNEpaliMonthDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliYearDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDateDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.TextAccessBKLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBKFor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtAutoBackUpTimerTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleBKAuto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBKTimerDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ToggleBKDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBKPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBKType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GroupControl3)
        Me.PanelControl1.Controls.Add(Me.PanelControl2)
        Me.PanelControl1.Controls.Add(Me.GroupControl2)
        Me.PanelControl1.Controls.Add(Me.GroupControl1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(577, 568)
        Me.PanelControl1.TabIndex = 26
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.BtnAccessDB)
        Me.GroupControl3.Controls.Add(Me.SimpleButton1)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEditNEpaliMonthDOB)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEditNepaliYearDOB)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEditNepaliDateDOB)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEditNepaliYear)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEditNEpaliMonth)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEditNepaliDate)
        Me.GroupControl3.Controls.Add(Me.DateEdit3)
        Me.GroupControl3.Controls.Add(Me.DateEdit2)
        Me.GroupControl3.Controls.Add(Me.LabelControl15)
        Me.GroupControl3.Controls.Add(Me.LabelControl16)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl3.Location = New System.Drawing.Point(2, 343)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(573, 130)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.Text = "Manual Log Backup"
        '
        'BtnAccessDB
        '
        Me.BtnAccessDB.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BtnAccessDB.Appearance.Options.UseFont = True
        Me.BtnAccessDB.Location = New System.Drawing.Point(281, 81)
        Me.BtnAccessDB.Name = "BtnAccessDB"
        Me.BtnAccessDB.Size = New System.Drawing.Size(114, 23)
        Me.BtnAccessDB.TabIndex = 45
        Me.BtnAccessDB.Text = "BackUp Access DB"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(161, 81)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(114, 23)
        Me.SimpleButton1.TabIndex = 44
        Me.SimpleButton1.Text = "BackUp Now"
        '
        'ComboBoxEditNEpaliMonthDOB
        '
        Me.ComboBoxEditNEpaliMonthDOB.Location = New System.Drawing.Point(209, 55)
        Me.ComboBoxEditNEpaliMonthDOB.Name = "ComboBoxEditNEpaliMonthDOB"
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonthDOB.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonthDOB.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonthDOB.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonthDOB.TabIndex = 43
        '
        'ComboBoxEditNepaliYearDOB
        '
        Me.ComboBoxEditNepaliYearDOB.Location = New System.Drawing.Point(281, 55)
        Me.ComboBoxEditNepaliYearDOB.Name = "ComboBoxEditNepaliYearDOB"
        Me.ComboBoxEditNepaliYearDOB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYearDOB.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYearDOB.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYearDOB.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYearDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYearDOB.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYearDOB.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYearDOB.TabIndex = 42
        '
        'ComboBoxEditNepaliDateDOB
        '
        Me.ComboBoxEditNepaliDateDOB.Location = New System.Drawing.Point(161, 55)
        Me.ComboBoxEditNepaliDateDOB.Name = "ComboBoxEditNepaliDateDOB"
        Me.ComboBoxEditNepaliDateDOB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDateDOB.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDateDOB.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDateDOB.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDateDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDateDOB.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDateDOB.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDateDOB.TabIndex = 41
        '
        'ComboBoxEditNepaliYear
        '
        Me.ComboBoxEditNepaliYear.Location = New System.Drawing.Point(281, 28)
        Me.ComboBoxEditNepaliYear.Name = "ComboBoxEditNepaliYear"
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYear.TabIndex = 40
        '
        'ComboBoxEditNEpaliMonth
        '
        Me.ComboBoxEditNEpaliMonth.Location = New System.Drawing.Point(209, 28)
        Me.ComboBoxEditNEpaliMonth.Name = "ComboBoxEditNEpaliMonth"
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonth.TabIndex = 39
        '
        'ComboBoxEditNepaliDate
        '
        Me.ComboBoxEditNepaliDate.Location = New System.Drawing.Point(161, 28)
        Me.ComboBoxEditNepaliDate.Name = "ComboBoxEditNepaliDate"
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDate.TabIndex = 38
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = Nothing
        Me.DateEdit3.Location = New System.Drawing.Point(162, 55)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit3.Properties.Appearance.Options.UseFont = True
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit3.Size = New System.Drawing.Size(135, 20)
        Me.DateEdit3.TabIndex = 37
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(162, 28)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit2.Size = New System.Drawing.Size(135, 20)
        Me.DateEdit2.TabIndex = 36
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(16, 31)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl15.TabIndex = 26
        Me.LabelControl15.Text = "BackUp From"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(16, 60)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl16.TabIndex = 25
        Me.LabelControl16.Text = "BackUp To"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.SimpleSave)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl2.Location = New System.Drawing.Point(2, 306)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(573, 37)
        Me.PanelControl2.TabIndex = 2
        '
        'SimpleSave
        '
        Me.SimpleSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleSave.Appearance.Options.UseFont = True
        Me.SimpleSave.Location = New System.Drawing.Point(493, 6)
        Me.SimpleSave.Name = "SimpleSave"
        Me.SimpleSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleSave.TabIndex = 14
        Me.SimpleSave.Text = "Save"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.TextAccessBKLocation)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.TextBKFor)
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.TxtAutoBackUpTimerTime)
        Me.GroupControl2.Controls.Add(Me.LabelControl6)
        Me.GroupControl2.Controls.Add(Me.ToggleBKAuto)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.TextBKTimerDays)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.LabelControl5)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(2, 138)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(573, 168)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Auto Log Backup"
        '
        'TextAccessBKLocation
        '
        Me.TextAccessBKLocation.Location = New System.Drawing.Point(161, 135)
        Me.TextAccessBKLocation.Name = "TextAccessBKLocation"
        Me.TextAccessBKLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAccessBKLocation.Properties.Appearance.Options.UseFont = True
        Me.TextAccessBKLocation.Properties.MaxLength = 500
        Me.TextAccessBKLocation.Size = New System.Drawing.Size(344, 20)
        Me.TextAccessBKLocation.TabIndex = 32
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(16, 138)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(131, 14)
        Me.LabelControl9.TabIndex = 33
        Me.LabelControl9.Text = "Access BackUp Location"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(261, 112)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(25, 14)
        Me.LabelControl7.TabIndex = 31
        Me.LabelControl7.Text = "Days"
        '
        'TextBKFor
        '
        Me.TextBKFor.Location = New System.Drawing.Point(162, 109)
        Me.TextBKFor.Name = "TextBKFor"
        Me.TextBKFor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextBKFor.Properties.Appearance.Options.UseFont = True
        Me.TextBKFor.Properties.Mask.EditMask = "[0-9]*"
        Me.TextBKFor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextBKFor.Properties.MaxLength = 3
        Me.TextBKFor.Size = New System.Drawing.Size(73, 20)
        Me.TextBKFor.TabIndex = 4
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(17, 112)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(92, 14)
        Me.LabelControl8.TabIndex = 30
        Me.LabelControl8.Text = "Auto BackUp For"
        '
        'TxtAutoBackUpTimerTime
        '
        Me.TxtAutoBackUpTimerTime.EditValue = "10:00"
        Me.TxtAutoBackUpTimerTime.Location = New System.Drawing.Point(162, 83)
        Me.TxtAutoBackUpTimerTime.Name = "TxtAutoBackUpTimerTime"
        Me.TxtAutoBackUpTimerTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtAutoBackUpTimerTime.Properties.Appearance.Options.UseFont = True
        Me.TxtAutoBackUpTimerTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtAutoBackUpTimerTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtAutoBackUpTimerTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtAutoBackUpTimerTime.Properties.MaxLength = 5
        Me.TxtAutoBackUpTimerTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtAutoBackUpTimerTime.TabIndex = 3
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(260, 60)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(25, 14)
        Me.LabelControl6.TabIndex = 28
        Me.LabelControl6.Text = "Days"
        '
        'ToggleBKAuto
        '
        Me.ToggleBKAuto.Location = New System.Drawing.Point(161, 26)
        Me.ToggleBKAuto.Name = "ToggleBKAuto"
        Me.ToggleBKAuto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleBKAuto.Properties.Appearance.Options.UseFont = True
        Me.ToggleBKAuto.Properties.OffText = "No"
        Me.ToggleBKAuto.Properties.OnText = "Yes"
        Me.ToggleBKAuto.Size = New System.Drawing.Size(95, 25)
        Me.ToggleBKAuto.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(16, 31)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl3.TabIndex = 26
        Me.LabelControl3.Text = "Auto BackUp"
        '
        'TextBKTimerDays
        '
        Me.TextBKTimerDays.Location = New System.Drawing.Point(161, 57)
        Me.TextBKTimerDays.Name = "TextBKTimerDays"
        Me.TextBKTimerDays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextBKTimerDays.Properties.Appearance.Options.UseFont = True
        Me.TextBKTimerDays.Properties.Mask.EditMask = "[0-9]*"
        Me.TextBKTimerDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextBKTimerDays.Properties.MaxLength = 3
        Me.TextBKTimerDays.Size = New System.Drawing.Size(73, 20)
        Me.TextBKTimerDays.TabIndex = 2
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(16, 60)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl4.TabIndex = 25
        Me.LabelControl4.Text = "Auto BackUp In "
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(16, 86)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl5.TabIndex = 23
        Me.LabelControl5.Text = "BackUp At"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.ToggleBKDelete)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TextBKPath)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.ComboBKType)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(2, 2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(573, 136)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Log Backup"
        '
        'ToggleBKDelete
        '
        Me.ToggleBKDelete.Location = New System.Drawing.Point(161, 85)
        Me.ToggleBKDelete.Name = "ToggleBKDelete"
        Me.ToggleBKDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleBKDelete.Properties.Appearance.Options.UseFont = True
        Me.ToggleBKDelete.Properties.OffText = "No"
        Me.ToggleBKDelete.Properties.OnText = "Yes"
        Me.ToggleBKDelete.Size = New System.Drawing.Size(95, 25)
        Me.ToggleBKDelete.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(16, 90)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(112, 14)
        Me.LabelControl2.TabIndex = 26
        Me.LabelControl2.Text = "Delete After BackUp"
        '
        'TextBKPath
        '
        Me.TextBKPath.Location = New System.Drawing.Point(161, 59)
        Me.TextBKPath.Name = "TextBKPath"
        Me.TextBKPath.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextBKPath.Properties.Appearance.Options.UseFont = True
        Me.TextBKPath.Properties.MaxLength = 500
        Me.TextBKPath.Size = New System.Drawing.Size(344, 20)
        Me.TextBKPath.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(16, 62)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(111, 14)
        Me.LabelControl1.TabIndex = 25
        Me.LabelControl1.Text = "BackUp File Location"
        '
        'ComboBKType
        '
        Me.ComboBKType.EditValue = "TEXT"
        Me.ComboBKType.Location = New System.Drawing.Point(161, 33)
        Me.ComboBKType.Name = "ComboBKType"
        Me.ComboBKType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBKType.Properties.Appearance.Options.UseFont = True
        Me.ComboBKType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBKType.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBKType.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBKType.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBKType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBKType.Properties.Items.AddRange(New Object() {"TEXT", "DAT"})
        Me.ComboBKType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBKType.Size = New System.Drawing.Size(100, 20)
        Me.ComboBKType.TabIndex = 1
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(16, 36)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(93, 14)
        Me.LabelControl11.TabIndex = 23
        Me.LabelControl11.Text = "BackUp File Type"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(80, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'XtraDBBackUpSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraDBBackUpSetting"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.ComboBoxEditNEpaliMonthDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliYearDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDateDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.TextAccessBKLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBKFor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtAutoBackUpTimerTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleBKAuto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBKTimerDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.ToggleBKDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBKPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBKType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBKType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextBKPath As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ToggleBKDelete As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleBKAuto As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextBKTimerDays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtAutoBackUpTimerTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextBKFor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextAccessBKLocation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEditNepaliYearDOB As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliDateDOB As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents ComboBoxEditNEpaliMonthDOB As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnAccessDB As DevExpress.XtraEditors.SimpleButton

End Class
