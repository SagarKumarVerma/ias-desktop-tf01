﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraProfessionalTaxSlabEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraProfessionalTaxSlabEdit))
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditTaxAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLowerLimit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditUpperLimit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.GridLookUpEditBranch = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.TextEditTaxAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLowerLimit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUpperLimit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditBranch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(68, 135)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 5
        Me.SimpleButtonSave.Text = "Save"
        '
        'TextEditTaxAmt
        '
        Me.TextEditTaxAmt.Location = New System.Drawing.Point(102, 75)
        Me.TextEditTaxAmt.Name = "TextEditTaxAmt"
        Me.TextEditTaxAmt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTaxAmt.Properties.Appearance.Options.UseFont = True
        Me.TextEditTaxAmt.Properties.Mask.EditMask = "#########.##"
        Me.TextEditTaxAmt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditTaxAmt.Properties.MaxLength = 6
        Me.TextEditTaxAmt.Size = New System.Drawing.Size(156, 20)
        Me.TextEditTaxAmt.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(12, 77)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl4.TabIndex = 26
        Me.LabelControl4.Text = "Tax Amount"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 51)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Upper Limit"
        '
        'TextEditLowerLimit
        '
        Me.TextEditLowerLimit.Location = New System.Drawing.Point(102, 23)
        Me.TextEditLowerLimit.Name = "TextEditLowerLimit"
        Me.TextEditLowerLimit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLowerLimit.Properties.Appearance.Options.UseFont = True
        Me.TextEditLowerLimit.Properties.Mask.EditMask = "#########.##"
        Me.TextEditLowerLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditLowerLimit.Properties.MaxLength = 12
        Me.TextEditLowerLimit.Size = New System.Drawing.Size(156, 20)
        Me.TextEditLowerLimit.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl1.TabIndex = 19
        Me.LabelControl1.Text = "Lower Limit"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(149, 135)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Close"
        '
        'TextEditUpperLimit
        '
        Me.TextEditUpperLimit.Location = New System.Drawing.Point(102, 49)
        Me.TextEditUpperLimit.Name = "TextEditUpperLimit"
        Me.TextEditUpperLimit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUpperLimit.Properties.Appearance.Options.UseFont = True
        Me.TextEditUpperLimit.Properties.Mask.EditMask = "#########.##"
        Me.TextEditUpperLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditUpperLimit.Properties.MaxLength = 12
        Me.TextEditUpperLimit.Size = New System.Drawing.Size(156, 20)
        Me.TextEditUpperLimit.TabIndex = 2
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(13, 104)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl23.TabIndex = 41
        Me.LabelControl23.Text = "Location"
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'GridLookUpEditBranch
        '
        Me.GridLookUpEditBranch.Location = New System.Drawing.Point(102, 101)
        Me.GridLookUpEditBranch.Name = "GridLookUpEditBranch"
        Me.GridLookUpEditBranch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditBranch.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditBranch.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditBranch.Properties.DataSource = Me.TblbranchBindingSource
        Me.GridLookUpEditBranch.Properties.DisplayMember = "BRANCHCODE"
        Me.GridLookUpEditBranch.Properties.NullText = ""
        Me.GridLookUpEditBranch.Properties.ValueMember = "BRANCHCODE"
        Me.GridLookUpEditBranch.Properties.View = Me.GridView7
        Me.GridLookUpEditBranch.Size = New System.Drawing.Size(156, 20)
        Me.GridLookUpEditBranch.TabIndex = 4
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Location Code"
        Me.GridColumn1.FieldName = "BRANCHCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Location Name"
        Me.GridColumn2.FieldName = "BRANCHNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'XtraProfessionalTaxSlabEdit
        '
        Me.AcceptButton = Me.SimpleButtonSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 207)
        Me.ControlBox = False
        Me.Controls.Add(Me.GridLookUpEditBranch)
        Me.Controls.Add(Me.LabelControl23)
        Me.Controls.Add(Me.TextEditUpperLimit)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.TextEditTaxAmt)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TextEditLowerLimit)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraProfessionalTaxSlabEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Income Tax Slab"
        CType(Me.TextEditTaxAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLowerLimit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUpperLimit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditBranch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditTaxAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLowerLimit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditUpperLimit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents GridLookUpEditBranch As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
End Class
