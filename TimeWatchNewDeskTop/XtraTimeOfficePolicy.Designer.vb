﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraTimeOfficePolicy
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraTimeOfficePolicy))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblSetupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSETUPID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPERMISLATEARR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPERMISEARLYDEP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDUPLICATECHECKMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOVERSTAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colS_END = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colS_OUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOTOUTMINUSSHIFTENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOTWRKGHRSMINUSSHIFTHRS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOTEARLYCOMEPLUSLATEDEP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOTALL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOTEARLYCOMING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTEARLYDUR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTLATECOMINGDUR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTRESTRICTENDDUR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTEARLYDEPARTUREDUR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEDUCTWOOT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEDUCTHOLIDAYOT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISPRESENTONWOPRESENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISPRESENTONHLDPRESENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMAXWRKDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCHECKLATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCHECKEARLY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHALF = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISAUTOABSENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAUTOSHIFT_LOW = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAUTOSHIFT_UP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISAUTOSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISHALFDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISSHORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTWO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colWOINCLUDE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIsOutWork = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNightShiftFourPunch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLinesPerPage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSkipAfterDepartment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmeals_rate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMART = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISHELP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colATT_ACC = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTROUND = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPREWO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISAWA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISPREWO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeaveFinancialYear = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOnline = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAutoDownload = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMIS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOwMinus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLateVerification = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMSPassword = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMSUserID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMSMessage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTimerDur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBackDays = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTimeSpanEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblSetupTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblSetupTableAdapter()
        Me.TblSetup1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblSetup1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblSetupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeSpanEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblSetupBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.RepositoryItemTimeEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemTimeSpanEdit1, Me.RepositoryItemTimeEdit2, Me.RepositoryItemComboBox1, Me.RepositoryItemTextEdit3, Me.RepositoryItemDateEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblSetupBindingSource
        '
        Me.TblSetupBindingSource.DataMember = "tblSetup"
        Me.TblSetupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSETUPID, Me.colPERMISLATEARR, Me.colPERMISEARLYDEP, Me.colDUPLICATECHECKMIN, Me.colISOVERSTAY, Me.colS_END, Me.colS_OUT, Me.colISOTOUTMINUSSHIFTENDTIME, Me.colISOTWRKGHRSMINUSSHIFTHRS, Me.colISOTEARLYCOMEPLUSLATEDEP, Me.colISOTALL, Me.colISOTEARLYCOMING, Me.colOTEARLYDUR, Me.colOTLATECOMINGDUR, Me.colOTRESTRICTENDDUR, Me.colOTEARLYDEPARTUREDUR, Me.colDEDUCTWOOT, Me.colDEDUCTHOLIDAYOT, Me.colISPRESENTONWOPRESENT, Me.colISPRESENTONHLDPRESENT, Me.colMAXWRKDURATION, Me.colTIME1, Me.colCHECKLATE, Me.colCHECKEARLY, Me.colTIME, Me.colHALF, Me.colSHORT, Me.colISAUTOABSENT, Me.colAUTOSHIFT_LOW, Me.colAUTOSHIFT_UP, Me.colISAUTOSHIFT, Me.colISHALFDAY, Me.colISSHORT, Me.colTWO, Me.colWOINCLUDE, Me.colIsOutWork, Me.colNightShiftFourPunch, Me.colLinesPerPage, Me.colSkipAfterDepartment, Me.colmeals_rate, Me.colINOUT, Me.colSMART, Me.colISHELP, Me.colATT_ACC, Me.colOTROUND, Me.colPREWO, Me.colISAWA, Me.colISPREWO, Me.colLeaveFinancialYear, Me.colOnline, Me.colAutoDownload, Me.colMIS, Me.colOwMinus, Me.colLateVerification, Me.colSMSPassword, Me.colSMSUserID, Me.colSMSMessage, Me.colTimerDur, Me.colBackDays})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Policy"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsEditForm.EditFormColumnCount = 1
        Me.GridView1.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSETUPID, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSETUPID
        '
        Me.colSETUPID.FieldName = "SETUPID"
        Me.colSETUPID.Name = "colSETUPID"
        '
        'colPERMISLATEARR
        '
        Me.colPERMISLATEARR.FieldName = "PERMISLATEARR"
        Me.colPERMISLATEARR.Name = "colPERMISLATEARR"
        Me.colPERMISLATEARR.Width = 101
        '
        'colPERMISEARLYDEP
        '
        Me.colPERMISEARLYDEP.FieldName = "PERMISEARLYDEP"
        Me.colPERMISEARLYDEP.Name = "colPERMISEARLYDEP"
        Me.colPERMISEARLYDEP.Width = 108
        '
        'colDUPLICATECHECKMIN
        '
        Me.colDUPLICATECHECKMIN.FieldName = "DUPLICATECHECKMIN"
        Me.colDUPLICATECHECKMIN.Name = "colDUPLICATECHECKMIN"
        Me.colDUPLICATECHECKMIN.Width = 104
        '
        'colISOVERSTAY
        '
        Me.colISOVERSTAY.FieldName = "ISOVERSTAY"
        Me.colISOVERSTAY.Name = "colISOVERSTAY"
        Me.colISOVERSTAY.Width = 101
        '
        'colS_END
        '
        Me.colS_END.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colS_END.FieldName = "S_END"
        Me.colS_END.Name = "colS_END"
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colS_OUT
        '
        Me.colS_OUT.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colS_OUT.FieldName = "S_OUT"
        Me.colS_OUT.Name = "colS_OUT"
        '
        'colISOTOUTMINUSSHIFTENDTIME
        '
        Me.colISOTOUTMINUSSHIFTENDTIME.FieldName = "ISOTOUTMINUSSHIFTENDTIME"
        Me.colISOTOUTMINUSSHIFTENDTIME.Name = "colISOTOUTMINUSSHIFTENDTIME"
        '
        'colISOTWRKGHRSMINUSSHIFTHRS
        '
        Me.colISOTWRKGHRSMINUSSHIFTHRS.FieldName = "ISOTWRKGHRSMINUSSHIFTHRS"
        Me.colISOTWRKGHRSMINUSSHIFTHRS.Name = "colISOTWRKGHRSMINUSSHIFTHRS"
        Me.colISOTWRKGHRSMINUSSHIFTHRS.Width = 119
        '
        'colISOTEARLYCOMEPLUSLATEDEP
        '
        Me.colISOTEARLYCOMEPLUSLATEDEP.FieldName = "ISOTEARLYCOMEPLUSLATEDEP"
        Me.colISOTEARLYCOMEPLUSLATEDEP.Name = "colISOTEARLYCOMEPLUSLATEDEP"
        '
        'colISOTALL
        '
        Me.colISOTALL.FieldName = "ISOTALL"
        Me.colISOTALL.Name = "colISOTALL"
        '
        'colISOTEARLYCOMING
        '
        Me.colISOTEARLYCOMING.FieldName = "ISOTEARLYCOMING"
        Me.colISOTEARLYCOMING.Name = "colISOTEARLYCOMING"
        '
        'colOTEARLYDUR
        '
        Me.colOTEARLYDUR.FieldName = "OTEARLYDUR"
        Me.colOTEARLYDUR.Name = "colOTEARLYDUR"
        '
        'colOTLATECOMINGDUR
        '
        Me.colOTLATECOMINGDUR.FieldName = "OTLATECOMINGDUR"
        Me.colOTLATECOMINGDUR.Name = "colOTLATECOMINGDUR"
        '
        'colOTRESTRICTENDDUR
        '
        Me.colOTRESTRICTENDDUR.FieldName = "OTRESTRICTENDDUR"
        Me.colOTRESTRICTENDDUR.Name = "colOTRESTRICTENDDUR"
        '
        'colOTEARLYDEPARTUREDUR
        '
        Me.colOTEARLYDEPARTUREDUR.FieldName = "OTEARLYDEPARTUREDUR"
        Me.colOTEARLYDEPARTUREDUR.Name = "colOTEARLYDEPARTUREDUR"
        '
        'colDEDUCTWOOT
        '
        Me.colDEDUCTWOOT.FieldName = "DEDUCTWOOT"
        Me.colDEDUCTWOOT.Name = "colDEDUCTWOOT"
        '
        'colDEDUCTHOLIDAYOT
        '
        Me.colDEDUCTHOLIDAYOT.FieldName = "DEDUCTHOLIDAYOT"
        Me.colDEDUCTHOLIDAYOT.Name = "colDEDUCTHOLIDAYOT"
        '
        'colISPRESENTONWOPRESENT
        '
        Me.colISPRESENTONWOPRESENT.FieldName = "ISPRESENTONWOPRESENT"
        Me.colISPRESENTONWOPRESENT.Name = "colISPRESENTONWOPRESENT"
        '
        'colISPRESENTONHLDPRESENT
        '
        Me.colISPRESENTONHLDPRESENT.FieldName = "ISPRESENTONHLDPRESENT"
        Me.colISPRESENTONHLDPRESENT.Name = "colISPRESENTONHLDPRESENT"
        '
        'colMAXWRKDURATION
        '
        Me.colMAXWRKDURATION.FieldName = "MAXWRKDURATION"
        Me.colMAXWRKDURATION.Name = "colMAXWRKDURATION"
        '
        'colTIME1
        '
        Me.colTIME1.FieldName = "TIME1"
        Me.colTIME1.Name = "colTIME1"
        '
        'colCHECKLATE
        '
        Me.colCHECKLATE.FieldName = "CHECKLATE"
        Me.colCHECKLATE.Name = "colCHECKLATE"
        '
        'colCHECKEARLY
        '
        Me.colCHECKEARLY.FieldName = "CHECKEARLY"
        Me.colCHECKEARLY.Name = "colCHECKEARLY"
        '
        'colTIME
        '
        Me.colTIME.FieldName = "TIME"
        Me.colTIME.Name = "colTIME"
        '
        'colHALF
        '
        Me.colHALF.FieldName = "HALF"
        Me.colHALF.Name = "colHALF"
        '
        'colSHORT
        '
        Me.colSHORT.FieldName = "SHORT"
        Me.colSHORT.Name = "colSHORT"
        '
        'colISAUTOABSENT
        '
        Me.colISAUTOABSENT.FieldName = "ISAUTOABSENT"
        Me.colISAUTOABSENT.Name = "colISAUTOABSENT"
        '
        'colAUTOSHIFT_LOW
        '
        Me.colAUTOSHIFT_LOW.FieldName = "AUTOSHIFT_LOW"
        Me.colAUTOSHIFT_LOW.Name = "colAUTOSHIFT_LOW"
        '
        'colAUTOSHIFT_UP
        '
        Me.colAUTOSHIFT_UP.FieldName = "AUTOSHIFT_UP"
        Me.colAUTOSHIFT_UP.Name = "colAUTOSHIFT_UP"
        '
        'colISAUTOSHIFT
        '
        Me.colISAUTOSHIFT.FieldName = "ISAUTOSHIFT"
        Me.colISAUTOSHIFT.Name = "colISAUTOSHIFT"
        '
        'colISHALFDAY
        '
        Me.colISHALFDAY.FieldName = "ISHALFDAY"
        Me.colISHALFDAY.Name = "colISHALFDAY"
        '
        'colISSHORT
        '
        Me.colISSHORT.FieldName = "ISSHORT"
        Me.colISSHORT.Name = "colISSHORT"
        '
        'colTWO
        '
        Me.colTWO.FieldName = "TWO"
        Me.colTWO.Name = "colTWO"
        '
        'colWOINCLUDE
        '
        Me.colWOINCLUDE.FieldName = "WOINCLUDE"
        Me.colWOINCLUDE.Name = "colWOINCLUDE"
        '
        'colIsOutWork
        '
        Me.colIsOutWork.FieldName = "IsOutWork"
        Me.colIsOutWork.Name = "colIsOutWork"
        '
        'colNightShiftFourPunch
        '
        Me.colNightShiftFourPunch.FieldName = "NightShiftFourPunch"
        Me.colNightShiftFourPunch.Name = "colNightShiftFourPunch"
        '
        'colLinesPerPage
        '
        Me.colLinesPerPage.FieldName = "LinesPerPage"
        Me.colLinesPerPage.Name = "colLinesPerPage"
        '
        'colSkipAfterDepartment
        '
        Me.colSkipAfterDepartment.FieldName = "SkipAfterDepartment"
        Me.colSkipAfterDepartment.Name = "colSkipAfterDepartment"
        '
        'colmeals_rate
        '
        Me.colmeals_rate.FieldName = "meals_rate"
        Me.colmeals_rate.Name = "colmeals_rate"
        '
        'colINOUT
        '
        Me.colINOUT.FieldName = "INOUT"
        Me.colINOUT.Name = "colINOUT"
        '
        'colSMART
        '
        Me.colSMART.FieldName = "SMART"
        Me.colSMART.Name = "colSMART"
        '
        'colISHELP
        '
        Me.colISHELP.FieldName = "ISHELP"
        Me.colISHELP.Name = "colISHELP"
        '
        'colATT_ACC
        '
        Me.colATT_ACC.FieldName = "ATT_ACC"
        Me.colATT_ACC.Name = "colATT_ACC"
        '
        'colOTROUND
        '
        Me.colOTROUND.FieldName = "OTROUND"
        Me.colOTROUND.Name = "colOTROUND"
        '
        'colPREWO
        '
        Me.colPREWO.FieldName = "PREWO"
        Me.colPREWO.Name = "colPREWO"
        '
        'colISAWA
        '
        Me.colISAWA.FieldName = "ISAWA"
        Me.colISAWA.Name = "colISAWA"
        '
        'colISPREWO
        '
        Me.colISPREWO.FieldName = "ISPREWO"
        Me.colISPREWO.Name = "colISPREWO"
        '
        'colLeaveFinancialYear
        '
        Me.colLeaveFinancialYear.FieldName = "LeaveFinancialYear"
        Me.colLeaveFinancialYear.Name = "colLeaveFinancialYear"
        '
        'colOnline
        '
        Me.colOnline.FieldName = "Online"
        Me.colOnline.Name = "colOnline"
        Me.colOnline.Visible = True
        Me.colOnline.VisibleIndex = 0
        '
        'colAutoDownload
        '
        Me.colAutoDownload.FieldName = "AutoDownload"
        Me.colAutoDownload.Name = "colAutoDownload"
        Me.colAutoDownload.Visible = True
        Me.colAutoDownload.VisibleIndex = 1
        '
        'colMIS
        '
        Me.colMIS.FieldName = "MIS"
        Me.colMIS.Name = "colMIS"
        '
        'colOwMinus
        '
        Me.colOwMinus.FieldName = "OwMinus"
        Me.colOwMinus.Name = "colOwMinus"
        '
        'colLateVerification
        '
        Me.colLateVerification.FieldName = "LateVerification"
        Me.colLateVerification.Name = "colLateVerification"
        '
        'colSMSPassword
        '
        Me.colSMSPassword.FieldName = "SMSPassword"
        Me.colSMSPassword.Name = "colSMSPassword"
        '
        'colSMSUserID
        '
        Me.colSMSUserID.FieldName = "SMSUserID"
        Me.colSMSUserID.Name = "colSMSUserID"
        '
        'colSMSMessage
        '
        Me.colSMSMessage.FieldName = "SMSMessage"
        Me.colSMSMessage.Name = "colSMSMessage"
        '
        'colTimerDur
        '
        Me.colTimerDur.FieldName = "TimerDur"
        Me.colTimerDur.Name = "colTimerDur"
        Me.colTimerDur.Visible = True
        Me.colTimerDur.VisibleIndex = 2
        '
        'colBackDays
        '
        Me.colBackDays.FieldName = "BackDays"
        Me.colBackDays.Name = "colBackDays"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RepositoryItemTextEdit1.MaxLength = 3
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.MaxLength = 5
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.DisplayFormat.FormatString = "HH:mm"
        Me.RepositoryItemTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit2.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTextEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemTimeSpanEdit1
        '
        Me.RepositoryItemTimeSpanEdit1.AutoHeight = False
        Me.RepositoryItemTimeSpanEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeSpanEdit1.DisplayFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeSpanEdit1.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeSpanEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom
        Me.RepositoryItemTimeSpanEdit1.Name = "RepositoryItemTimeSpanEdit1"
        Me.RepositoryItemTimeSpanEdit1.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.SpinButtons
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.MaxLength = 5
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"DAY", "NIGHT", "HALFDAY"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.Mask.EditMask = "([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]"
        Me.RepositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(104, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblSetupTableAdapter
        '
        Me.TblSetupTableAdapter.ClearBeforeFill = True
        '
        'TblSetup1TableAdapter1
        '
        Me.TblSetup1TableAdapter1.ClearBeforeFill = True
        '
        'XtraTimeOfficePolicy
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraTimeOfficePolicy"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblSetupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeSpanEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTimeSpanEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblSetupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents colSETUPID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPERMISLATEARR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPERMISEARLYDEP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDUPLICATECHECKMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOVERSTAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colS_END As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colS_OUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOTOUTMINUSSHIFTENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOTWRKGHRSMINUSSHIFTHRS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOTEARLYCOMEPLUSLATEDEP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOTALL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOTEARLYCOMING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTEARLYDUR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTLATECOMINGDUR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTRESTRICTENDDUR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTEARLYDEPARTUREDUR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEDUCTWOOT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEDUCTHOLIDAYOT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISPRESENTONWOPRESENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISPRESENTONHLDPRESENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMAXWRKDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCHECKLATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCHECKEARLY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHALF As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISAUTOABSENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAUTOSHIFT_LOW As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAUTOSHIFT_UP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISAUTOSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISHALFDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISSHORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTWO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colWOINCLUDE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIsOutWork As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNightShiftFourPunch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLinesPerPage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSkipAfterDepartment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmeals_rate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMART As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISHELP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colATT_ACC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTROUND As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPREWO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISAWA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISPREWO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeaveFinancialYear As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOnline As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAutoDownload As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMIS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOwMinus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLateVerification As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMSPassword As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMSUserID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMSMessage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTimerDur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBackDays As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblSetupTableAdapter As iAS.SSSDBDataSetTableAdapters.tblSetupTableAdapter
    Friend WithEvents TblSetup1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblSetup1TableAdapter
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit

End Class
