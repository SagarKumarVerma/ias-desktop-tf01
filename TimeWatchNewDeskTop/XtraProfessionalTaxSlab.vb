﻿Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraProfessionalTaxSlab
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared PTax_ID_NO As String
    Public Sub New()
        InitializeComponent()
        'Me.Width = Common.NavWidth 'Me.Parent.Width
        'Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        'SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        ''not to do in all pages
        'Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        'Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraCompany_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'DevExpress.XtraGrid.Localization.GridLocalizer.Active = New MyLocalizer 'for e.error text for grid validation
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        LoadTaxGrid()
    End Sub
    Private Sub LoadTaxGrid()
        Dim gridselet As String = "select * from PROFESSIONALTAX"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            Dim WTDataTable As New DataTable("tblCompany")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            Dim WTDataTable As New DataTable("tblCompany")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        End If
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(0).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("companycode", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(1).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            'XtraMessageBox.Show(ulf, "<size=10>Company Name cannot empty</size>", "<size=9>Error</size>", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.ErrorText = "<size=10>" & Common.res_man.GetString("companyname", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & "</size>" & ","
        End If
        Dim ssql As String
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet
            ds = New DataSet
            ssql = "select COMPANYCODE from tblCompany where COMPANYCODE = '" & row(0).ToString & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                e.Valid = False
                e.ErrorText = "<size=10>Duplicate Company Code ,"
                Exit Sub
            End If
        End If
        'If Common.USERTYPE = "H" Then
        '
        'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            'insert
            ssql = "insert into tblCompany (COMPANYNAME, COMPANYADDRESS, SHORTNAME, PANNUM, TANNUMBER, LCNO, PFNO, GSTIN, LastModifiedBy, LastModifiedDate, COMPANYCODE) values ('" & row("COMPANYNAME").ToString & "', '" & row("COMPANYADDRESS").ToString & "', '" & row("SHORTNAME").ToString & "', '" & row("PANNUM").ToString & "', '" & row("TANNUMBER").ToString & "', '" & row("LCNO").ToString & "', '" & row("PFNO").ToString & "', '" & row("GSTIN").ToString & "', '" & Common.USER_R & "', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', '" & row("COMPANYCODE").ToString & "')"
        Else
            'update
            ssql = "update tblCompany SET COMPANYNAME='" & row("COMPANYNAME").ToString & "', COMPANYADDRESS='" & row("COMPANYADDRESS").ToString & "', SHORTNAME='" & row("SHORTNAME").ToString & "', PANNUM='" & row("PANNUM").ToString & "', TANNUMBER='" & row("TANNUMBER").ToString & "', LCNO='" & row("LCNO").ToString & "', PFNO='" & row("PFNO").ToString & "', GSTIN='" & row("GSTIN").ToString & "', LastModifiedBy='" & Common.USER_R & "', LastModifiedDate='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' where COMPANYCODE = '" & row("COMPANYCODE").ToString & "'"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(ssql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(ssql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'LoadCompGrid()
        'End If
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("ID_NO").ToString.Trim
                Dim sSql As String
                'Dim adap As SqlDataAdapter
                'Dim adapA As OleDbDataAdapter
                'Dim ds As DataSet
                'Dim sSql As String = "select count(*) from TblEmployee where COMPANYCODE = '" & CellId & "'"
                'ds = New DataSet
                'If Common.servername = "Access" Then
                '    adapA = New OleDbDataAdapter(sSql, Common.con1)
                '    adapA.Fill(ds)
                'Else
                '    adap = New SqlDataAdapter(sSql, Common.con)
                '    adap.Fill(ds)
                'End If
                'If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                '    XtraMessageBox.Show(ulf, "<size=10>Company already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '    Me.Validate()
                '    e.Handled = True
                '    Exit Sub
                'Else
                Try
                    sSql = "delete from PROFESSIONALTAX where ID_NO =" & CellId
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                Catch ex As Exception

                End Try
                'LoadCompGrid()
                'End If
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
        End If
    End Sub

    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            PTax_ID_NO = row("ID_NO").ToString.Trim
        Catch ex As Exception
            PTax_ID_NO = ""
        End Try
        e.Allow = False
        XtraProfessionalTaxSlabEdit.ShowDialog()
        LoadTaxGrid()
    End Sub
End Class
