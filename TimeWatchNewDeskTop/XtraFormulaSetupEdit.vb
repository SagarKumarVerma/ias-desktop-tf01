﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraFormulaSetupEdit
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim frID As String
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub

    Private Sub XtraFormulaSetupEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        frID = XtraFormulaSetup.FormulaId
        If frID.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If
    End Sub
    Private Sub SetDefaultValue()
        ComboBoxEditFrmCode.Properties.Items.Clear()
        'For ascii = 65 To 90
        '    ComboBoxEditFrmCode.Properties.Items.Add(Chr(ascii))
        'Next
        ComboBoxEditFrmCode.Properties.Items.Add("A")
        ComboBoxEditFrmCode.Properties.Items.Add("B")
        ComboBoxEditFrmCode.Properties.Items.Add("C")
        ComboBoxEditFrmCode.Properties.Items.Add("D")
        ComboBoxEditFrmCode.Properties.Items.Add("E")
        ComboBoxEditFrmCode.Properties.Items.Add("F")
        ComboBoxEditFrmCode.Properties.Items.Add("G")
        ComboBoxEditFrmCode.Properties.Items.Add("H")
        ComboBoxEditFrmCode.Properties.Items.Add("I")
        ComboBoxEditFrmCode.Properties.Items.Add("J")
        ComboBoxEditFrmCode.Properties.Items.Add("K")
        ComboBoxEditFrmCode.Properties.Items.Add("L")
        ComboBoxEditFrmCode.Properties.Items.Add("M")
        ComboBoxEditFrmCode.Properties.Items.Add("N")
        ComboBoxEditFrmCode.Properties.Items.Add("O")
        ComboBoxEditFrmCode.Properties.Items.Add("P")
        ComboBoxEditFrmCode.Properties.Items.Add("Q")
        ComboBoxEditFrmCode.Properties.Items.Add("R")
        ComboBoxEditFrmCode.Properties.Items.Add("S")
        ComboBoxEditFrmCode.Properties.Items.Add("T")
        ComboBoxEditFrmCode.Properties.Items.Add("U")
        ComboBoxEditFrmCode.Properties.Items.Add("V")
        ComboBoxEditFrmCode.Properties.Items.Add("W")
        ComboBoxEditFrmCode.Properties.Items.Add("X")
        ComboBoxEditFrmCode.Properties.Items.Add("Y")
        ComboBoxEditFrmCode.Properties.Items.Add("Z")
       


        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim ssql As String = "select * from PAY_FORMULA"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ComboBoxEditFrmCode.Properties.Items.Remove(ds.Tables(0).Rows(i).Item("Code").ToString.Trim)
            Next
        End If
        ComboBoxEditFrmCode.Enabled = True
        ComboBoxEditFrmCode.SelectedIndex = 0
        TextEditFORM.Text = ""
    End Sub
    Private Sub SetFormValue()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "SELECT * from PAY_FORMULA where code='" & frID & "'"

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            ComboBoxEditFrmCode.EditValue = ds.Tables(0).Rows(0).Item("code").ToString.Trim
            TextEditFORM.Text = ds.Tables(0).Rows(0).Item("FORM").ToString.Trim & " "
        End If
        ComboBoxEditFrmCode.Enabled = False
    End Sub

    Private Sub SimpleButtonBasic_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonBasic.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonBasic.Text
    End Sub

    Private Sub SimpleButtonDA_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDA.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDA.Text
    End Sub

    Private Sub SimpleButtonHRA_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonHRA.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonHRA.Text
    End Sub

    Private Sub SimpleButtonCONV_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonCONV.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonCONV.Text
    End Sub

    Private Sub SimpleButtonMED_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonMED.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonMED.Text
    End Sub

    Private Sub SimpleButtonPRE_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonPRE.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonPRE.Text
    End Sub

    Private Sub SimpleButtonABS_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonABS.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonABS.Text
    End Sub

    Private Sub SimpleButtonHLD_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonHLD.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonHLD.Text
    End Sub

    Private Sub SimpleButtonLate_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonLate.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonLate.Text
    End Sub

    Private Sub SimpleButtonEARLY_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARLY.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARLY.Text
    End Sub

    Private Sub SimpleButtonESI_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonESI.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonESI.Text
    End Sub

    Private Sub SimpleButtonCL_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonCL.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonCL.Text
    End Sub

    Private Sub SimpleButtonSL_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSL.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonSL.Text
    End Sub

    Private Sub SimpleButtonPL_EL_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonPL_EL.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonPL_EL.Text
    End Sub

    Private Sub SimpleButtonOTHER_LV_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonOTHER_LV.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonOTHER_LV.Text
    End Sub

    Private Sub SimpleButtonLEAVE_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonLEAVE.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonLEAVE.Text
    End Sub

    Private Sub SimpleButtonTDAYS_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonTDAYS.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonTDAYS.Text
    End Sub

    Private Sub SimpleButtonT_LATE_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonT_LATE.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonT_LATE.Text
    End Sub

    Private Sub SimpleButtonT_EARLY_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonT_EARLY.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonT_EARLY.Text
    End Sub

    Private Sub SimpleButtonOT_RATE_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonOT_RATE.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonOT_RATE.Text
    End Sub

    Private Sub SimpleButtonMON_DAY_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonMON_DAY.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonMON_DAY.Text
    End Sub

    Private Sub SimpleButtonOT_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonOT.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonOT.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_1.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_1.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_2.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_2.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_3.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_3.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_4.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_4.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_5.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_5.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_6_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_6.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_6.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_7_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_7.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_7.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_8_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_8.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_8.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_9_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_9.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_9.Text
    End Sub

    Private Sub SimpleButtonDEDUCT_10_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDEDUCT_10.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonDEDUCT_10.Text
    End Sub

    Private Sub SimpleButtonTDS_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonTDS.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonTDS.Text
    End Sub

    Private Sub SimpleButtonEARN_1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_1.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_1.Text
    End Sub

    Private Sub SimpleButtonEARN_2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_2.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_2.Text
    End Sub

    Private Sub SimpleButtonEARN_3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_3.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_3.Text
    End Sub

    Private Sub SimpleButtonEARN_4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_4.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_4.Text
    End Sub

    Private Sub SimpleButtonEARN_5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_5.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_5.Text
    End Sub

    Private Sub SimpleButtonEARN_6_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_6.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_6.Text
    End Sub

    Private Sub SimpleButtonEARN_7_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_7.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_7.Text
    End Sub

    Private Sub SimpleButtonEARN_8_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_8.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_8.Text
    End Sub

    Private Sub SimpleButtonEARN_9_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_9.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_9.Text
    End Sub

    Private Sub SimpleButtonEARN_10_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonEARN_10.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonEARN_10.Text
    End Sub

    Private Sub SimpleButtonIF_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonIF.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButtonIF.Text
    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton3.Text
    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton4.Text
    End Sub

    Private Sub SimpleButton5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton5.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton5.Text
    End Sub

    Private Sub SimpleButton6_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton6.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton6.Text
    End Sub

    Private Sub SimpleButton7_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton7.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton7.Text
    End Sub

    Private Sub SimpleButton8_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton8.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton8.Text
    End Sub

    Private Sub SimpleButton9_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton9.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton9.Text
    End Sub

    Private Sub SimpleButton10_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton10.Click
        TextEditFORM.Text = TextEditFORM.Text & SimpleButton10.Text
    End Sub

    Private Sub SimpleButtonReset_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonReset.Click
        TextEditFORM.Text = ""
    End Sub

    Private Sub SimpleButtonClose_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonClose.Click
        Me.Close()
    End Sub

    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        If TextEditFORM.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Formula cannot be empty.</size>", "<size=9>Error</size>")
            TextEditFORM.Select()
            Exit Sub
        End If
        Dim code As String = ComboBoxEditFrmCode.EditValue.ToString
        Dim FORM As String = TextEditFORM.Text.Trim & " "
        Dim sSql As String
        If frID = "" Then
            sSql = "insert INTO PAY_FORMULA (CODE, FORM) VALUES('" & code & "','" & FORM & "')"
        Else
            sSql = "update PAY_FORMULA set FORM ='" & FORM & "' where code='" & code & "'"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Me.Close()
    End Sub
End Class