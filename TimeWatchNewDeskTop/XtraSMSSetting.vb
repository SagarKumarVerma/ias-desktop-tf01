﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Public Class XtraSMSSetting
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub XtraSMSSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Widths
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        setDefault()
    End Sub

    Private Sub CheckCurrentDate_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckCurrentDate.CheckedChanged
        If CheckCurrentDate.Checked = True Then
            LabelControl3.Text = "SMS Triger After Shift Start Time"
            TextEditSMSTrigerAfterShiftStartTime.Visible = True
            TextEditPreviousdaySMSTriggerTime.Visible = False
        Else
            LabelControl3.Text = "Previous day SMS Trigger Time"
            TextEditSMSTrigerAfterShiftStartTime.Visible = False
            TextEditPreviousdaySMSTriggerTime.Visible = True
        End If
    End Sub
    Private Sub setDefault()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from tblSMS"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            TextKey.EditValue = ds.Tables(0).Rows(0).Item("SMSKey").ToString.Trim
            TextSenderId.Text = ds.Tables(0).Rows(0).Item("SenderID").ToString.Trim
            If ds.Tables(0).Rows(0).Item("IsAbsent").ToString.Trim = "Y" Then
                ToggleAbsentSMS.IsOn = True
            Else
                ToggleAbsentSMS.IsOn = False
            End If

            If ds.Tables(0).Rows(0).Item("AbsentSMSFor").ToString.Trim = "C" Then
                CheckCurrentDate.Checked = True
            Else
                CheckCurrentDate.Checked = False
            End If

            TextEditSMSTrigerAfterShiftStartTime.Text = ds.Tables(0).Rows(0).Item("AbsentSMSAfter").ToString.Trim
            TextEditPreviousdaySMSTriggerTime.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("AbsentSMSTime").ToString.Trim).ToString("HH:mm")
           
            TextAbsentSMSContent1.Text = ds.Tables(0).Rows(0).Item("AbsentSMS1").ToString.Trim
            TextAbsentSMSContent2.Text = ds.Tables(0).Rows(0).Item("AbsentSMS2").ToString.Trim

            If ds.Tables(0).Rows(0).Item("AbsentName").ToString.Trim = "Y" Then
                CheckName.Checked = True
            Else
                CheckName.Checked = False
            End If

            If ds.Tables(0).Rows(0).Item("AbsentDate").ToString.Trim = "Y" Then
                CheckDate.Checked = True
            Else
                CheckDate.Checked = False
            End If

            If ds.Tables(0).Rows(0).Item("IsLate").ToString.Trim = "Y" Then
                ToggleLateSMS.IsOn = True
            Else
                ToggleLateSMS.IsOn = False
            End If
           
            TextLateSMSContent1.Text = ds.Tables(0).Rows(0).Item("LateSMS1").ToString.Trim
            TextLateSMSContent2.Text = ds.Tables(0).Rows(0).Item("LateSMS2").ToString.Trim

            If ds.Tables(0).Rows(0).Item("IsIn").ToString.Trim = "Y" Then
                ToggleINSMS.IsOn = True
            Else
                ToggleINSMS.IsOn = False
            End If
           
            TextInPunchSMSContent1.Text = ds.Tables(0).Rows(0).Item("InSMS1").ToString.Trim
            TextInPunchSMSContent2.Text = ds.Tables(0).Rows(0).Item("InSMS2").ToString.Trim

            If ds.Tables(0).Rows(0).Item("IsOut").ToString.Trim = "Y" Then
                ToggleOUTSMS.IsOn = True
            Else
                ToggleOUTSMS.IsOn = False
            End If
           
            TextOutPunchSMSContent1.Text = ds.Tables(0).Rows(0).Item("OutSMS1").ToString.Trim
            TextOutPunchSMSContent2.Text = ds.Tables(0).Rows(0).Item("OutSMS2").ToString.Trim

            TextOutSMSAfter.Text = ds.Tables(0).Rows(0).Item("OutSMSAfter").ToString.Trim

            If ds.Tables(0).Rows(0).Item("IsAll").ToString.Trim = "Y" Then
                ToggleSwitchAllPunch.IsOn = True
            Else
                ToggleSwitchAllPunch.IsOn = False
            End If
            TextAllSms1.Text = ds.Tables(0).Rows(0).Item("AllSMS1").ToString.Trim
            TextAllSms2.Text = ds.Tables(0).Rows(0).Item("AllSMS2").ToString.Trim

            If ds.Tables(0).Rows(0).Item("DeviceWiseInOut").ToString.Trim = "Y" Then
                ToggleMachineWise.IsOn = True
                TextAllSms1.Enabled = False
                TextAllSms2.Enabled = False
            Else
                ToggleMachineWise.IsOn = False
                TextAllSms1.Enabled = True
                TextAllSms2.Enabled = True
            End If
        End If
        If CheckCurrentDate.Checked = True Then
            LabelControl3.Text = "SMS Triger After Shift Start Time"
            TextEditSMSTrigerAfterShiftStartTime.Visible = True
            TextEditPreviousdaySMSTriggerTime.Visible = False
        Else
            LabelControl3.Text = "Previous day SMS Trigger Time"
            TextEditSMSTrigerAfterShiftStartTime.Visible = False
            TextEditPreviousdaySMSTriggerTime.Visible = True
        End If


    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextKey.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Link.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextKey.Select()
            Exit Sub
        End If
        'If TextSenderId.Text.Trim = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Please Enter SenderID.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    TextSenderId.Select()
        '    Exit Sub
        'End If
        Dim SMSKey As String = TextKey.Text.Trim
        Dim SenderID As String = TextSenderId.Text.Trim
        Dim IsAbsent As String
        If ToggleAbsentSMS.IsOn = True Then
            IsAbsent = "Y"
            If CheckCurrentDate.Checked = True Then
                If TextEditSMSTrigerAfterShiftStartTime.Text.Trim = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Please Enter SMS Trigger after Shift Start Time.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEditSMSTrigerAfterShiftStartTime.Select()
                    Exit Sub
                End If
            Else
                If TextEditPreviousdaySMSTriggerTime.Text.Trim = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Previous day SMS Trigger Time.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEditPreviousdaySMSTriggerTime.Select()
                    Exit Sub
                End If
            End If
        Else
            IsAbsent = "N"
        End If
        Dim AbsentSMSFor As String
        If CheckCurrentDate.Checked = True Then
            AbsentSMSFor = "C"
        Else
            AbsentSMSFor = "P"
        End If
        Dim AbsentSMSAfter As String
        If TextEditSMSTrigerAfterShiftStartTime.Text = "" Then
            AbsentSMSAfter = "0"
        Else
            AbsentSMSAfter = TextEditSMSTrigerAfterShiftStartTime.Text
        End If
        Dim AbsentSMSTime As DateTime '= Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditPreviousdaySMSTriggerTime.Text & ":00")
        If TextEditPreviousdaySMSTriggerTime.Text = "" Then
            AbsentSMSTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & "00:00:00")
        Else
            AbsentSMSTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditPreviousdaySMSTriggerTime.Text & ":00")
        End If
        Dim AbsentSMS1 As String = TextAbsentSMSContent1.Text.Trim
        Dim AbsentSMS2 As String = TextAbsentSMSContent2.Text.Trim

        Dim AbsentName As String
        If CheckName.Checked = True Then
            AbsentName = "Y"
        Else
            AbsentName = "N"
        End If

        Dim AbsentDate As String
        If CheckDate.Checked = True Then
            AbsentDate = "Y"
        Else
            AbsentDate = "N"
        End If

        Dim IsLate As String
        If ToggleLateSMS.IsOn = True Then
            IsLate = "Y"
        Else
            IsLate = "N"
        End If
        Dim LateSMS1 As String = TextLateSMSContent1.Text.Trim
        Dim LateSMS2 As String = TextLateSMSContent2.Text.Trim
        Dim IsIn As String
        If ToggleINSMS.IsOn = True Then
            IsIn = "Y"
        Else
            IsIn = "N"
        End If
        Dim InSMS1 As String = TextInPunchSMSContent1.Text.Trim
        Dim InSMS2 As String = TextInPunchSMSContent2.Text.Trim
        Dim IsOut As String
        If ToggleOUTSMS.IsOn = True Then
            IsOut = "Y"
            If TextOutSMSAfter.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>SMS Trigger after Shift End Time</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextOutSMSAfter.Select()
                Exit Sub
            End If
        Else
            IsOut = "N"
        End If
        Dim OutSMS1 As String = TextOutPunchSMSContent1.Text.Trim
        Dim OutSMS2 As String = TextOutPunchSMSContent2.Text.Trim
        Dim OutSMSAfter As String
        If TextOutSMSAfter.Text.Trim = "" Then
            OutSMSAfter = "0"
        Else
            OutSMSAfter = TextOutSMSAfter.Text.Trim
        End If

        Dim IsAll As String
        If ToggleSwitchAllPunch.IsOn = True Then
            IsAll = "Y"
        Else
            IsAll = "N"
            ToggleMachineWise.IsOn = False
        End If
        Dim AllSMS1 As String = TextAllSms1.Text.Trim
        Dim AllSMS2 As String = TextAllSms2.Text.Trim

        Dim isCalled As String = "Y"

        Dim DeviceWiseInOut As String
        If ToggleMachineWise.IsOn = True Then
            DeviceWiseInOut = "Y"
            If InSMS1 = "" And InSMS2 = "" Then
                XtraMessageBox.Show(ulf, "<size=10>In SMS Content cannot be Empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextInPunchSMSContent1.Select()
                Exit Sub
            End If
            If OutSMS1 = "" And OutSMS2 = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Out SMS Content cannot be Empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextOutPunchSMSContent1.Select()
                Exit Sub
            End If
        Else
            DeviceWiseInOut = "N"
        End If
        'Dim dbType As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from tblSMS"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            'update
            sSql = "update tblSMS  set SMSKey  = '" & SMSKey & "',  SenderID = '" & SenderID & "',  IsAbsent  = '" & IsAbsent & "',  AbsentSMSFor = '" & AbsentSMSFor & "',  AbsentSMSAfter = " & AbsentSMSAfter & ",  AbsentSMSTime = '" & AbsentSMSTime.ToString("yyyy-MM-dd HH:mm:ss") & "',  AbsentSMS1 = '" & AbsentSMS1 & "', AbsentSMS2 = '" & AbsentSMS2 & "',  AbsentName = '" & AbsentName & "',  AbsentDate = '" & AbsentDate & "', IsLate = '" & IsLate & "',  LateSMS1 = '" & LateSMS1 & "',  LateSMS2 = '" & LateSMS2 & "',  IsIn = '" & IsIn & "',  InSMS1 = '" & InSMS1 & "',  InSMS2 = '" & InSMS2 & "', IsOut = '" & IsOut & "',  OutSMS1 = '" & OutSMS1 & "',  OutSMS2 = '" & OutSMS2 & "',  OutSMSAfter = " & OutSMSAfter & ", isCalled = '" & isCalled & "', IsAll='" & IsAll & "', AllSMS1='" & AllSMS1 & "', AllSMS2='" & AllSMS2 & "', DeviceWiseInOut='" & DeviceWiseInOut & "'"
        Else
            'insert()
            sSql = " INSERT into tblSMS (SMSKey,  SenderID,  IsAbsent,  AbsentSMSFor,  AbsentSMSAfter,  AbsentSMSTime,  AbsentSMS1, AbsentSMS2,  AbsentName,  AbsentDate, IsLate,  LateSMS1,  LateSMS2,  IsIn,  InSMS1,  InSMS2, IsOut,  OutSMS1,  OutSMS2,  OutSMSAfter, isCalled,IsAll, AllSMS1, AllSMS2, DeviceWiseInOut) VALUES" & _
                "('" & SMSKey & "','" & SenderID & "','" & IsAbsent & "','" & AbsentSMSFor & "','" & AbsentSMSAfter & "','" & AbsentSMSTime.ToString("yyyy-MM-dd HH:mm:ss") & "','" & AbsentSMS1 & "','" & AbsentSMS2 & "','" & AbsentName & "','" & AbsentDate & "','" & IsLate & "','" & LateSMS1 & "','" & LateSMS2 & "','" & IsIn & "','" & InSMS1 & "','" & InSMS2 & "','" & IsOut & "','" & OutSMS1 & "','" & OutSMS2 & "','" & OutSMSAfter & "','" & isCalled & "','" & IsAll & "','" & AllSMS1 & "','" & AllSMS2 & "', '" & DeviceWiseInOut & "')"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.LogPost("SMS Setting Save")
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
        Common.Load_SMS_Policy()
        setDefault()
    End Sub

    Private Sub ToggleSwitchAllPunch_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchAllPunch.Toggled
        If ToggleSwitchAllPunch.IsOn = True Then
            ToggleAbsentSMS.IsOn = False
            ToggleINSMS.IsOn = False
            ToggleLateSMS.IsOn = False
            ToggleOUTSMS.IsOn = False

            ToggleMachineWise.Visible = True
            LabelControl18.Visible = True
        Else
            ToggleMachineWise.Visible = False
            LabelControl18.Visible = False
        End If
    End Sub

    Private Sub ToggleAbsentSMS_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleAbsentSMS.Toggled
        If ToggleAbsentSMS.IsOn = True Then
            ToggleSwitchAllPunch.IsOn = False
        End If
    End Sub

    Private Sub ToggleLateSMS_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleLateSMS.Toggled
        If ToggleLateSMS.IsOn = True Then
            ToggleSwitchAllPunch.IsOn = False
        End If
    End Sub

    Private Sub ToggleINSMS_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleINSMS.Toggled
        If ToggleINSMS.IsOn = True Then
            ToggleSwitchAllPunch.IsOn = False
        End If
    End Sub

    Private Sub ToggleOUTSMS_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleOUTSMS.Toggled
        If ToggleOUTSMS.IsOn = True Then
            ToggleSwitchAllPunch.IsOn = False
        End If
    End Sub

    Private Sub ToggleMachineWise_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleMachineWise.Toggled
        If ToggleMachineWise.IsOn = True Then
            TextAllSms1.Enabled = False
            TextAllSms2.Enabled = False
        Else
            TextAllSms1.Enabled = True
            TextAllSms2.Enabled = True
        End If
    End Sub
End Class
