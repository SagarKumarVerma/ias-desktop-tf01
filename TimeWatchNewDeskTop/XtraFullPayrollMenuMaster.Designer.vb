﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraFullPayrollMenuMaster
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarSubItemSetup = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemEmpSetup = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPaySetup = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemFormulaSetup = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLoan = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemProcessing = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemPayProcess = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemMaint = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemArrerAndInc = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemArrer = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemInc = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemTax = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemIncTax = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemProTax = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemForm16 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemTools = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemReImb = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLeaveEncash = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemGratuity = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemFFE = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemPiece = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemPieceM = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPieceEntry = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemReport = New DevExpress.XtraBars.BarSubItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemReports = New DevExpress.XtraBars.BarSubItem()
        Me.SidePanelTitle = New DevExpress.XtraEditors.SidePanel()
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.SidePanelMainFormShow = New DevExpress.XtraEditors.SidePanel()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelTitle.SuspendLayout()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItemSetup, Me.BarButtonItemEmpSetup, Me.BarButtonItemPaySetup, Me.BarButtonItemFormulaSetup, Me.BarSubItem1, Me.BarSubItem2, Me.BarButtonItem1, Me.BarSubItemProcessing, Me.BarButtonItemPayProcess, Me.BarButtonItemMaint, Me.BarSubItemArrerAndInc, Me.BarButtonItemArrer, Me.BarButtonItemInc, Me.BarButtonItemLoan, Me.BarSubItemTax, Me.BarSubItemTools, Me.BarButtonItemIncTax, Me.BarButtonItemProTax, Me.BarButtonItemForm16, Me.BarButtonItemReImb, Me.BarButtonItemLeaveEncash, Me.BarButtonItemGratuity, Me.BarButtonItemFFE, Me.BarButtonItem2, Me.BarSubItemReports, Me.BarSubItemPiece, Me.BarSubItemReport, Me.BarButtonItemPieceM, Me.BarButtonItemPieceEntry})
        Me.BarManager1.MaxItemId = 31
        '
        'Bar2
        '
        Me.Bar2.BarAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 17.0!)
        Me.Bar2.BarAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarAppearance.Hovered.Options.UseFont = True
        Me.Bar2.BarAppearance.Hovered.Options.UseForeColor = True
        Me.Bar2.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar2.BarAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarAppearance.Normal.Options.UseFont = True
        Me.Bar2.BarAppearance.Normal.Options.UseForeColor = True
        Me.Bar2.BarAppearance.Pressed.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar2.BarAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarAppearance.Pressed.Options.UseFont = True
        Me.Bar2.BarAppearance.Pressed.Options.UseForeColor = True
        Me.Bar2.BarName = "Main menu"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemSetup), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemProcessing), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemArrerAndInc), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemTax), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemTools), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemPiece), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemReport)})
        Me.Bar2.OptionsBar.AllowQuickCustomization = False
        Me.Bar2.OptionsBar.DrawDragBorder = False
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Main menu"
        '
        'BarSubItemSetup
        '
        Me.BarSubItemSetup.Caption = "Setup"
        Me.BarSubItemSetup.Id = 1
        Me.BarSubItemSetup.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemEmpSetup), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPaySetup), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemFormulaSetup), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLoan)})
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemSetup.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemSetup.Name = "BarSubItemSetup"
        Me.BarSubItemSetup.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemEmpSetup
        '
        Me.BarButtonItemEmpSetup.Caption = "Employee Setup"
        Me.BarButtonItemEmpSetup.Id = 2
        Me.BarButtonItemEmpSetup.Name = "BarButtonItemEmpSetup"
        '
        'BarButtonItemPaySetup
        '
        Me.BarButtonItemPaySetup.Caption = "Payroll Setup"
        Me.BarButtonItemPaySetup.Id = 3
        Me.BarButtonItemPaySetup.Name = "BarButtonItemPaySetup"
        '
        'BarButtonItemFormulaSetup
        '
        Me.BarButtonItemFormulaSetup.Caption = "Formula Setup"
        Me.BarButtonItemFormulaSetup.Id = 4
        Me.BarButtonItemFormulaSetup.Name = "BarButtonItemFormulaSetup"
        '
        'BarButtonItemLoan
        '
        Me.BarButtonItemLoan.Caption = "Loan and Advance"
        Me.BarButtonItemLoan.Id = 14
        Me.BarButtonItemLoan.Name = "BarButtonItemLoan"
        '
        'BarSubItemProcessing
        '
        Me.BarSubItemProcessing.Caption = "Processing"
        Me.BarSubItemProcessing.Id = 8
        Me.BarSubItemProcessing.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPayProcess), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemMaint)})
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemProcessing.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemProcessing.Name = "BarSubItemProcessing"
        '
        'BarButtonItemPayProcess
        '
        Me.BarButtonItemPayProcess.Caption = "Pay Process"
        Me.BarButtonItemPayProcess.Id = 9
        Me.BarButtonItemPayProcess.Name = "BarButtonItemPayProcess"
        '
        'BarButtonItemMaint
        '
        Me.BarButtonItemMaint.Caption = "Maintenance"
        Me.BarButtonItemMaint.Id = 10
        Me.BarButtonItemMaint.Name = "BarButtonItemMaint"
        '
        'BarSubItemArrerAndInc
        '
        Me.BarSubItemArrerAndInc.Caption = "Arrear and Inc"
        Me.BarSubItemArrerAndInc.Id = 11
        Me.BarSubItemArrerAndInc.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemArrer), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemInc)})
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemArrerAndInc.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemArrerAndInc.Name = "BarSubItemArrerAndInc"
        '
        'BarButtonItemArrer
        '
        Me.BarButtonItemArrer.Caption = "Arrear Entry"
        Me.BarButtonItemArrer.Id = 12
        Me.BarButtonItemArrer.Name = "BarButtonItemArrer"
        '
        'BarButtonItemInc
        '
        Me.BarButtonItemInc.Caption = "Increment Entry"
        Me.BarButtonItemInc.Id = 13
        Me.BarButtonItemInc.Name = "BarButtonItemInc"
        '
        'BarSubItemTax
        '
        Me.BarSubItemTax.Caption = "Tax Setting"
        Me.BarSubItemTax.Id = 15
        Me.BarSubItemTax.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemIncTax), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemProTax), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemForm16)})
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemTax.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemTax.Name = "BarSubItemTax"
        '
        'BarButtonItemIncTax
        '
        Me.BarButtonItemIncTax.Caption = "IncomeTax Slab"
        Me.BarButtonItemIncTax.Id = 17
        Me.BarButtonItemIncTax.Name = "BarButtonItemIncTax"
        '
        'BarButtonItemProTax
        '
        Me.BarButtonItemProTax.Caption = "Professional Tax Slab"
        Me.BarButtonItemProTax.Id = 18
        Me.BarButtonItemProTax.Name = "BarButtonItemProTax"
        '
        'BarButtonItemForm16
        '
        Me.BarButtonItemForm16.Caption = "Form 16"
        Me.BarButtonItemForm16.Id = 19
        Me.BarButtonItemForm16.Name = "BarButtonItemForm16"
        Me.BarButtonItemForm16.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarSubItemTools
        '
        Me.BarSubItemTools.Caption = "Tools"
        Me.BarSubItemTools.Id = 16
        Me.BarSubItemTools.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemReImb), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLeaveEncash), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemGratuity), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemFFE)})
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemTools.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemTools.Name = "BarSubItemTools"
        '
        'BarButtonItemReImb
        '
        Me.BarButtonItemReImb.Caption = "Reimbursement"
        Me.BarButtonItemReImb.Id = 20
        Me.BarButtonItemReImb.Name = "BarButtonItemReImb"
        '
        'BarButtonItemLeaveEncash
        '
        Me.BarButtonItemLeaveEncash.Caption = "Leave Encashment"
        Me.BarButtonItemLeaveEncash.Id = 21
        Me.BarButtonItemLeaveEncash.Name = "BarButtonItemLeaveEncash"
        '
        'BarButtonItemGratuity
        '
        Me.BarButtonItemGratuity.Caption = "Gratuity Processing"
        Me.BarButtonItemGratuity.Id = 22
        Me.BarButtonItemGratuity.Name = "BarButtonItemGratuity"
        '
        'BarButtonItemFFE
        '
        Me.BarButtonItemFFE.Caption = "Full and Final Entry"
        Me.BarButtonItemFFE.Id = 23
        Me.BarButtonItemFFE.Name = "BarButtonItemFFE"
        '
        'BarSubItemPiece
        '
        Me.BarSubItemPiece.Caption = "Piece Steup"
        Me.BarSubItemPiece.Id = 27
        Me.BarSubItemPiece.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPieceM), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPieceEntry)})
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemPiece.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemPiece.Name = "BarSubItemPiece"
        '
        'BarButtonItemPieceM
        '
        Me.BarButtonItemPieceM.Caption = "Piece Master"
        Me.BarButtonItemPieceM.Id = 29
        Me.BarButtonItemPieceM.Name = "BarButtonItemPieceM"
        '
        'BarButtonItemPieceEntry
        '
        Me.BarButtonItemPieceEntry.Caption = "Piece Entry"
        Me.BarButtonItemPieceEntry.Id = 30
        Me.BarButtonItemPieceEntry.Name = "BarButtonItemPieceEntry"
        '
        'BarSubItemReport
        '
        Me.BarSubItemReport.Caption = "Reports"
        Me.BarSubItemReport.Id = 28
        Me.BarSubItemReport.Name = "BarSubItemReport"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1059, 42)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 476)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1059, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 42)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 434)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1059, 42)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 434)
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "BarSubItem1"
        Me.BarSubItem1.Id = 5
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'BarSubItem2
        '
        Me.BarSubItem2.Caption = "BarSubItem2"
        Me.BarSubItem2.Id = 6
        Me.BarSubItem2.Name = "BarSubItem2"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Processing"
        Me.BarButtonItem1.Id = 7
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "BarButtonItem2"
        Me.BarButtonItem2.Id = 24
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarSubItemReports
        '
        Me.BarSubItemReports.Caption = "Reports"
        Me.BarSubItemReports.Id = 25
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemReports.Name = "BarSubItemReports"
        '
        'SidePanelTitle
        '
        Me.SidePanelTitle.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelTitle.Appearance.Options.UseBackColor = True
        Me.SidePanelTitle.Controls.Add(Me.LabelTitle)
        Me.SidePanelTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelTitle.Location = New System.Drawing.Point(0, 42)
        Me.SidePanelTitle.Name = "SidePanelTitle"
        Me.SidePanelTitle.Size = New System.Drawing.Size(1059, 41)
        Me.SidePanelTitle.TabIndex = 9
        Me.SidePanelTitle.Text = "SidePanel2"
        '
        'LabelTitle
        '
        Me.LabelTitle.AutoSize = True
        Me.LabelTitle.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.LabelTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.LabelTitle.Location = New System.Drawing.Point(4, 7)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(94, 24)
        Me.LabelTitle.TabIndex = 0
        Me.LabelTitle.Text = "              "
        '
        'SidePanelMainFormShow
        '
        Me.SidePanelMainFormShow.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelMainFormShow.Location = New System.Drawing.Point(0, 83)
        Me.SidePanelMainFormShow.Name = "SidePanelMainFormShow"
        Me.SidePanelMainFormShow.Size = New System.Drawing.Size(1059, 393)
        Me.SidePanelMainFormShow.TabIndex = 1
        Me.SidePanelMainFormShow.Text = "SidePanel1"
        '
        'XtraFullPayrollMenuMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SidePanelMainFormShow)
        Me.Controls.Add(Me.SidePanelTitle)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraFullPayrollMenuMaster"
        Me.Size = New System.Drawing.Size(1059, 476)
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelTitle.ResumeLayout(False)
        Me.SidePanelTitle.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarSubItemSetup As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemEmpSetup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPaySetup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemFormulaSetup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SidePanelMainFormShow As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelTitle As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItemProcessing As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemPayProcess As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemMaint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemArrerAndInc As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemArrer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemInc As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLoan As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemTax As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItemTools As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemIncTax As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemProTax As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemForm16 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemReImb As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLeaveEncash As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemGratuity As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemFFE As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemReports As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemPiece As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItemReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemPieceM As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPieceEntry As DevExpress.XtraBars.BarButtonItem

End Class
