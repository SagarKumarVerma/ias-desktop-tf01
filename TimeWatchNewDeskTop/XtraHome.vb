﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Resources
Imports System.Globalization

Public Class XtraHome
    Private Sub XtraHome_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.Bounds.Width
        'Me.Height = My.Computer.Screen.Bounds.Height
        'MsgBox("home width " & Me.Parent.Width & vbCrLf & "height " & Me.Parent.Height)
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height

        'TileControl3.Width = TileControl3.Parent.Width
        'TileControl3.Height = TileControl3.Parent.Height

        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")


        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Emp")
        dt.Columns.Add("Present")
        dt.Columns.Add("Absent")
        dt.Columns.Add("WO")
        dt.Columns.Add("Hld")
        dt.Columns.Add("Leave")


        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim ssql As String = "select COUNT(PAYCODE) from TblEmployee"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlEmp.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        ds = New DataSet
        If Common.servername = "Access" Then
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='P'"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='P'"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlPresent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        ds = New DataSet
        If Common.servername = "Access" Then
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='A'"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='A'"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlAbsent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        ds = New DataSet
        If Common.servername = "Access" Then
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='WO'"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='WO'"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlWO.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        ds = New DataSet
        If Common.servername = "Access" Then
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='HLD'"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS='HLD'"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlHld.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        ds = New DataSet
        If Common.servername = "Access" Then
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS not in ('HLD','WO','P','A')"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and STATUS not in ('HLD','WO','P','A')"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlLeave.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        ds = New DataSet
        If Common.servername = "Access" Then
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and LATEARRIVAL > 0"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and LATEARRIVAL > 0"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlLate.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

        dt.Rows.Add(LabelControlEmp.Text, LabelControlPresent.Text, LabelControlAbsent.Text, LabelControlWO.Text, LabelControlHld.Text, LabelControlLeave.Text)
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        TileItem9.Text = "800"
    End Sub
    Private Sub TileItem9_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItem9.ItemClick
        XtraMaster.OfficeNavigationBar1.SelectedItem = XtraMaster.OfficeNavigationBar1.Items.Item(5)
    End Sub
    Private Sub TileItem12_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItem12.ItemClick
        XtraMaster.OfficeNavigationBar1.SelectedItem = XtraMaster.OfficeNavigationBar1.Items.Item(2)
    End Sub
    Private Sub TileItem11_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItem11.ItemClick
        XtraMaster.OfficeNavigationBar1.SelectedItem = XtraMaster.OfficeNavigationBar1.Items.Item(3)
    End Sub
    Private Sub TileItem7_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItem7.ItemClick
        XtraMaster.OfficeNavigationBar1.SelectedItem = XtraMaster.OfficeNavigationBar1.Items.Item(1)
    End Sub
    Private Sub TileItem8_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItem8.ItemClick
        XtraMaster.OfficeNavigationBar1.SelectedItem = XtraMaster.OfficeNavigationBar1.Items.Item(1)
    End Sub
    Private Sub TileItem10_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItem10.ItemClick
        XtraMaster.OfficeNavigationBar1.SelectedItem = XtraMaster.OfficeNavigationBar1.Items.Item(8)
    End Sub
End Class

