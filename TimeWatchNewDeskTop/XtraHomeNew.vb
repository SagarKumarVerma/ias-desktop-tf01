﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Sockets
Imports System.Threading
Imports System.Net
Imports System.Text
Imports ZKPushDemoNitinCSharp

Public Class XtraHomeNew
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds As DataSet = New DataSet
    Dim Paycode As String
    Dim Name As String
    Dim VerifyMode As String
    Dim PunchDate As String
    Dim PunchTime As String
    Dim Cn As Common = New Common

    Dim laodDeviceListThread As Thread
    Dim laodDeptAttenThread As Thread
    Structure DashBoardEmp
        Dim IN1 As String
        Dim WO_Value As Double
        Dim Leavevalue As Double
        Dim LATEARRIVAL As Double
    End Structure
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 12))
    End Sub
    Private Sub XtraHomeNew_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'Me.Width = My.Computer.Screen.Bounds.Width
        'Me.Height = My.Computer.Screen.Bounds.Height
        'MsgBox("home width " & Me.Parent.Width & vbCrLf & "height " & Me.Parent.Height)

        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height

        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")

        If TileControl1.Height < TileControl1.Parent.Height / 2 Then
            TileControl1.Height = TileControl1.Parent.Height / 2
        End If

        If Common.USERTYPE = "A" Then
            GridControl1.Visible = True
        Else
            GridControl1.Visible = False
        End If

        Application.DoEvents()
        GridControl1.Width = My.Computer.Screen.Bounds.Width / 2
        Application.DoEvents()

        If Common.HomeLoad = True Then
            laodDeptAtten()
            Application.DoEvents()
            LoadDashBoard()
            Application.DoEvents()
            TimerRefreshDashBoard.Enabled = True
        End If
      
        'sysIp = GetIP()
        'TextEditPortBio.Text = Common.BioPort
        'TextEditPortZK.Text = Common.ZKPort
        Common.HomeLoad = True
    End Sub
    Private Sub TimerRefreshDashBoard_Tick(sender As System.Object, e As System.EventArgs) Handles TimerRefreshDashBoard.Tick
        Try
            TimerRefreshDashBoard.Enabled = False
            LoadDashBoard()
            laodDeptAtten()
            TimerRefreshDashBoard.Enabled = True
            'Me.laodDeptAttenThread = New Thread(New ThreadStart(AddressOf Me.laodDeptAtten))
            'Me.laodDeptAttenThread.Start()
        Catch ex As Exception
            TimerRefreshDashBoard.Enabled = True
        End Try
    End Sub
    Public Delegate Sub laodDeptAttenCallback()
    'Private Sub laodDeptAtten() 'new
    '    'If Me.GridControl1.InvokeRequired Then
    '    '    Dim d As laodDeptAttenCallback = New laodDeptAttenCallback(AddressOf Me.laodDeptAtten)
    '    '    Me.Invoke(d, New Object() {})
    '    'Else
    '    GridControl1.DataSource = Nothing
    '    Dim dt As DataTable = New DataTable
    '    Dim deptname As String, total As String, present As String, absent As String, WO As String, leave As String, late As String
    '    dt.Columns.Add("Department")
    '    dt.Columns.Add("Total Employees")
    '    dt.Columns.Add("Present")
    '    dt.Columns.Add("Absent")
    '    dt.Columns.Add("Week Off")
    '    dt.Columns.Add("Leave")
    '    dt.Columns.Add("Late Arrival")
    '    Dim datase As DataSet = New DataSet()
    '    datase.Tables.Add(dt)
    '    GridControl1.DataSource = dt
    '    GridView1.Columns(1).Width = 120
    '    'Application.DoEvents()
    '    Dim c As Common = New Common()
    '    Dim sSql As String = "select * from tblDepartment"
    '    Dim ds1 As DataSet = New DataSet
    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(sSql, Common.con1)
    '        adapA.Fill(ds1)
    '    Else
    '        adap = New SqlDataAdapter(sSql, Common.con)
    '        adap.Fill(ds1)
    '    End If
    '    If ds1.Tables(0).Rows.Count > 0 Then
    '        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
    '            deptname = ds1.Tables(0).Rows(i).Item("DEPARTMENTNAME").ToString.Trim

    '            ds = New DataSet
    '            sSql = "select COUNT(PAYCODE) from tblemployee WHERE departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  active='Y'"
    '            If Common.servername = "Access" Then
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(ds)
    '            Else
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(ds)
    '            End If
    '            total = ds.Tables(0).Rows(0).Item(0).ToString.Trim

    '            ds = New DataSet
    '            If Common.servername = "Access" Then
    '                'sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.PRESENTVALUE > 0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "'"
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.IN1 Is Not Null and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "'  and  e.ACTIVE='Y'"
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(ds)
    '            Else
    '                'sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.PRESENTVALUE > 0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "'"
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.IN1 <> '' and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(ds)
    '            End If
    '            present = ds.Tables(0).Rows(0).Item(0).ToString.Trim


    '            ds = New DataSet
    '            If Common.servername = "Access" Then
    '                'sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.absentvalue = 1 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "'"
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.IN1 Is Null and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(ds)
    '            Else
    '                'sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.absentvalue = 1 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "'"
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and (t.IN1='' or t.IN1 is NULL) and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(ds)
    '            End If
    '            absent = ds.Tables(0).Rows(0).Item(0).ToString.Trim


    '            ds = New DataSet
    '            If Common.servername = "Access" Then
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.WO_VALUE>0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(ds)
    '            Else
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.WO_VALUE>0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(ds)
    '            End If
    '            WO = ds.Tables(0).Rows(0).Item(0).ToString.Trim

    '            ds = New DataSet
    '            If Common.servername = "Access" Then
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.leavevalue > 0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(ds)
    '            Else
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.leavevalue > 0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(ds)
    '            End If
    '            leave = ds.Tables(0).Rows(0).Item(0).ToString.Trim

    '            ds = New DataSet
    '            If Common.servername = "Access" Then
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE FORMAT(t.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.LATEARRIVAL > 0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(ds)
    '            Else
    '                sSql = "select COUNT(t.PAYCODE) from tblemployee e, tbltimeregister t WHERE t.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and t.LATEARRIVAL > 0 and e.paycode = t.paycode and e.departmentcode ='" & ds1.Tables(0).Rows(i).Item("departmentcode").ToString.Trim & "' and  e.ACTIVE='Y'"
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(ds)
    '            End If
    '            late = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '            GridView1.AddNewRow()
    '            Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
    '            If GridView1.IsNewItemRow(rowHandle) Then
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), deptname)
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), total)
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), present)
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), absent)
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), WO)
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), leave)
    '                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), late)
    '            End If
    '            'lblTotal.Text = ("Total : " & GridView1.RowCount)
    '            'Application.DoEvents()
    '        Next
    '        'laodDeptAttenThread.Abort()
    '    End If
    '    'End If
    'End Sub
    Private Sub laodDeptAtten() 'struct       
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        Dim deptname As String ', total As String, present As String, absent As String, WO As String, leave As String, late As String
        dt.Columns.Add("Department")
        dt.Columns.Add("Total Employees")
        dt.Columns.Add("Present")
        dt.Columns.Add("Absent")
        dt.Columns.Add("Week Off")
        dt.Columns.Add("Leave")
        dt.Columns.Add("Late Arrival")
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
        GridView1.Columns(1).Width = 120
        'Application.DoEvents()
        Dim c As Common = New Common()
        Dim sSql As String = "select * from tblDepartment"
        Dim ds1 As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds1)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds1)
        End If
        If ds1.Tables(0).Rows.Count > 0 Then
            For j As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                deptname = ds1.Tables(0).Rows(j).Item("DEPARTMENTNAME").ToString.Trim
                Dim DashBoardEmpArr() As DashBoardEmp
                Dim DashBoardEmpLst As New List(Of DashBoardEmp)

                ds = New DataSet
                'sSql = "select  tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,tblemployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblemployee.departmentcode ='" & ds1.Tables(0).Rows(j).Item("departmentcode").ToString.Trim & "' and  tblemployee.active='Y'"
                If Common.servername = "Access" Then
                    sSql = "select  tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,tblemployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblemployee.departmentcode ='" & ds1.Tables(0).Rows(j).Item("departmentcode").ToString.Trim & "' and  tblemployee.active='Y'"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    sSql = "select  tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,tblemployee WHERE tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblemployee.departmentcode ='" & ds1.Tables(0).Rows(j).Item("departmentcode").ToString.Trim & "' and  tblemployee.active='Y'"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim x As DashBoardEmp = New DashBoardEmp
                    x.IN1 = ds.Tables(0).Rows(i).Item("IN1").ToString.Trim
                    x.WO_Value = ds.Tables(0).Rows(i).Item("WO_Value").ToString.Trim
                    If ds.Tables(0).Rows(i).Item("Leavevalue").ToString.Trim = "" Then
                        x.Leavevalue = 0
                    Else
                        x.Leavevalue = ds.Tables(0).Rows(i).Item("Leavevalue").ToString.Trim
                    End If
                    If ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "" Then
                        x.LATEARRIVAL = 0
                    Else
                        x.LATEARRIVAL = ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim
                    End If

                    DashBoardEmpLst.Add(x)
                Next
                DashBoardEmpArr = DashBoardEmpLst.ToArray

                Dim PresenCount As Double = 0
                Dim AbsentCount As Double = 0
                Dim WOCount As Double = 0
                Dim LeaveCount As Double = 0
                Dim LateCount As Double = 0
                Dim total As Double = 0
                For i As Integer = 0 To DashBoardEmpArr.Count - 1
                    If DashBoardEmpArr(i).IN1 <> "" Then
                        PresenCount = PresenCount + 1
                    End If
                    If DashBoardEmpArr(i).IN1 = "" Then
                        AbsentCount = AbsentCount + 1
                    End If
                    If DashBoardEmpArr(i).WO_Value > 0 Then
                        WOCount = WOCount + 1
                    End If
                    If DashBoardEmpArr(i).Leavevalue > 0 Then
                        LeaveCount = LeaveCount + 1
                    End If
                    If DashBoardEmpArr(i).LATEARRIVAL > 0 Then
                        LateCount = LateCount + 1
                    End If
                Next
                total = DashBoardEmpArr.Count

                GridView1.AddNewRow()
                Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
                If GridView1.IsNewItemRow(rowHandle) Then
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), deptname)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), total)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), PresenCount)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), AbsentCount)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), WOCount)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), LeaveCount)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), LateCount)
                End If
            Next
        End If
    End Sub
    'Private Sub LoadDashBoard()    'old
    '    Dim adapA As OleDbDataAdapter
    '    Dim adap As SqlDataAdapter
    '    Dim ds As DataSet
    '    ds = New DataSet
    '    Dim ssql As String = "" '"select COUNT(PAYCODE) from TblEmployee where active='Y'"

    '    If Common.USERTYPE = "A" Then
    '        ssql = "select COUNT(PAYCODE) from TblEmployee where active='Y'"
    '    Else
    '        Dim emp() As String = Common.Auth_Branch.Split(",")
    '        Dim ls As New List(Of String)()
    '        For x As Integer = 0 To emp.Length - 1
    '            ls.Add(emp(x).Trim)
    '        Next
    '        ssql = "select COUNT(PAYCODE) from TblEmployee where active='Y' and BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
    '    End If


    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemTotal.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()
    '    If Common.USERTYPE = "A" Then
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            'ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and PRESENTVALUE > 0 "
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and IN1 Is Not Null "
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            'ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and PRESENTVALUE > 0 and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and IN1 <> '' and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemPresent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            'ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and absentvalue = 1 "
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and IN1 Is Null"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            'ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and absentvalue = 1 and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and IN1 = '' and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemAbsent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and WO_VALUE>0 "
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and WO_VALUE>0 and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemWO.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        'ds = New DataSet
    '        'If Common.servername = "Access" Then
    '        '    ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and HOLIDAY_VALUE>0"
    '        '    adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        '    adapA.Fill(ds)
    '        'Else
    '        '    ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and HOLIDAY_VALUE>0"
    '        '    adap = New SqlDataAdapter(ssql, Common.con)
    '        '    adap.Fill(ds)
    '        'End If
    '        'LabelControlHld.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and leavevalue > 0"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and leavevalue > 0 and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemLeave.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and LATEARRIVAL > 0 "
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            ssql = "select COUNT(PAYCODE) from tblTimeRegister WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and LATEARRIVAL > 0 and PAYCODE in (select paycode from TblEmployee with (nolock) where ACTIVE='Y')"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemLate.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '    Else

    '        Dim emp() As String = Common.Auth_Branch.Split(",")
    '        Dim ls As New List(Of String)()
    '        For x As Integer = 0 To emp.Length - 1
    '            ls.Add(emp(x).Trim)
    '        Next

    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.PRESENTVALUE > 0 and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and  TblEmployee.ACTIVE='Y'"
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 Is Not Null and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and  TblEmployee.ACTIVE='Y'"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.PRESENTVALUE > 0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 <> ''  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemPresent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.absentvalue = 1  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 Is Null  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.absentvalue = 1  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 = ''  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemAbsent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and  FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.WO_VALUE>0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.WO_VALUE>0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemWO.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        'If Common.servername = "Access" Then
    '        '    ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.HOLIDAY_VALUE>0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
    '        '    adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        '    adapA.Fill(ds)
    '        'Else
    '        '    ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.HOLIDAY_VALUE>0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
    '        '    adap = New SqlDataAdapter(ssql, Common.con)
    '        '    adap.Fill(ds)
    '        'End If
    '        'LabelControlHld.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.leavevalue > 0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.leavevalue > 0  and  BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemLeave.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()
    '        ds = New DataSet
    '        If Common.servername = "Access" Then
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.LATEARRIVAL > 0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adapA = New OleDbDataAdapter(ssql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.LATEARRIVAL > 0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '            adap = New SqlDataAdapter(ssql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        TileItemLate.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '        Application.DoEvents()

    '    End If

    '    Dim Present As Integer = TileItemPresent.Text
    '    Dim Absent As Integer = TileItemAbsent.Text
    '    Dim WeekOff As Integer = TileItemWO.Text
    '    Dim leave As Integer = TileItemLeave.Text
    '    Dim late As Integer = TileItemLate.Text

    '    Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
    '    Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
    '    Dim SeriesPoint1 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Present", New Object() {CType(Present, Object)}, 0)
    '    Dim SeriesPoint2 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Absent", New Object() {CType(Absent, Object)}, 1)
    '    Dim SeriesPoint3 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Week Off", New Object() {CType(WeekOff, Object)}, 2)
    '    Dim SeriesPoint4 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Leave", New Object() {CType(leave, Object)}, 3)
    '    Dim SeriesPoint5 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Late", New Object() {CType(late, Object)}, 4)
    '    Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
    '    Series1.Label.TextPattern = "{A} {V}"
    '    'PieSeriesLabel1.TextPattern = "{A} {V}"
    '    'Series1.Label = PieSeriesLabel1
    '    'Series1.Name = "Series 1"
    '    SeriesPoint1.ColorSerializable = "#32CD33" '"#FF8080"
    '    SeriesPoint2.ColorSerializable = "Red" '"#FFFF80"
    '    SeriesPoint3.ColorSerializable = "#BF00C2" '"#80FF80"
    '    SeriesPoint4.ColorSerializable = "#F0E78C" '"#80FF80"
    '    SeriesPoint5.ColorSerializable = "#FFC080" '"#80FF80"
    '    'ChartControl1.Series.Add(Series1)
    '    Series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2, SeriesPoint3, SeriesPoint4, SeriesPoint5})
    '    Series1.View = PieSeriesView1
    '    Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
    'End Sub
    'Private Sub LoadDashBoard()   'new
    '    Dim adapA As OleDbDataAdapter
    '    Dim adap As SqlDataAdapter
    '    Dim ds As DataSet
    '    ds = New DataSet
    '    Dim ssql As String = "" '"select COUNT(PAYCODE) from TblEmployee where active='Y'"
    '    ssql = "select COUNT(PAYCODE) from TblEmployee where PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and active='Y'"
    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemTotal.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()
    '    ds = New DataSet
    '    If Common.servername = "Access" Then
    '        'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.PRESENTVALUE > 0 and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and  TblEmployee.ACTIVE='Y'"
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 Is Not Null and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')  and  TblEmployee.ACTIVE='Y'"
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.PRESENTVALUE > 0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 <> ''  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemPresent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()
    '    ds = New DataSet
    '    If Common.servername = "Access" Then
    '        'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.absentvalue = 1  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.IN1 Is Null  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.absentvalue = 1  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and (tblTimeRegister.IN1='' or tblTimeRegister.IN1 is NULL) and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemAbsent.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()
    '    ds = New DataSet
    '    If Common.servername = "Access" Then
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and  FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.WO_VALUE>0  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.WO_VALUE>0  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemWO.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()
    '    ds = New DataSet
    '    'If Common.servername = "Access" Then
    '    '    ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.HOLIDAY_VALUE>0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
    '    '    adapA = New OleDbDataAdapter(ssql, Common.con1)
    '    '    adapA.Fill(ds)
    '    'Else
    '    '    ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.HOLIDAY_VALUE>0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
    '    '    adap = New SqlDataAdapter(ssql, Common.con)
    '    '    adap.Fill(ds)
    '    'End If
    '    'LabelControlHld.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim

    '    ds = New DataSet
    '    If Common.servername = "Access" Then
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.leavevalue > 0  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.leavevalue > 0  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemLeave.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()
    '    ds = New DataSet
    '    If Common.servername = "Access" Then
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.LATEARRIVAL > 0  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adapA = New OleDbDataAdapter(ssql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.LATEARRIVAL > 0  and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
    '        adap = New SqlDataAdapter(ssql, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    TileItemLate.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
    '    Application.DoEvents()


    '    Dim Present As Integer = TileItemPresent.Text
    '    Dim Absent As Integer = TileItemAbsent.Text
    '    Dim WeekOff As Integer = TileItemWO.Text
    '    Dim leave As Integer = TileItemLeave.Text
    '    Dim late As Integer = TileItemLate.Text

    '    Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
    '    Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
    '    Dim SeriesPoint1 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Present", New Object() {CType(Present, Object)}, 0)
    '    Dim SeriesPoint2 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Absent", New Object() {CType(Absent, Object)}, 1)
    '    Dim SeriesPoint3 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Week Off", New Object() {CType(WeekOff, Object)}, 2)
    '    Dim SeriesPoint4 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Leave", New Object() {CType(leave, Object)}, 3)
    '    Dim SeriesPoint5 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Late", New Object() {CType(late, Object)}, 4)
    '    Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
    '    Series1.Label.TextPattern = "{A} {V}"
    '    'PieSeriesLabel1.TextPattern = "{A} {V}"
    '    'Series1.Label = PieSeriesLabel1
    '    'Series1.Name = "Series 1"
    '    SeriesPoint1.ColorSerializable = "#32CD33" '"#FF8080"
    '    SeriesPoint2.ColorSerializable = "Red" '"#FFFF80"
    '    SeriesPoint3.ColorSerializable = "#BF00C2" '"#80FF80"
    '    SeriesPoint4.ColorSerializable = "#F0E78C" '"#80FF80"
    '    SeriesPoint5.ColorSerializable = "#FFC080" '"#80FF80"
    '    'ChartControl1.Series.Add(Series1)
    '    Series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2, SeriesPoint3, SeriesPoint4, SeriesPoint5})
    '    Series1.View = PieSeriesView1
    '    Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
    'End Sub
    Private Sub LoadDashBoard()  'with struct
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim ssql As String = "" '"select COUNT(PAYCODE) from TblEmployee where active='Y'"       
        ds = New DataSet
        Dim DashBoardEmpArr() As DashBoardEmp
        Dim DashBoardEmpLst As New List(Of DashBoardEmp)
        If Common.servername = "Access" Then
            'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.PRESENTVALUE > 0 and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and  TblEmployee.ACTIVE='Y'"
            ssql = "select tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')  and  TblEmployee.ACTIVE='Y'"
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            'ssql = "select COUNT(tblTimeRegister.PAYCODE) from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.PRESENTVALUE > 0  and  TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and  TblEmployee.ACTIVE='Y'"
            ssql = "select tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim x As DashBoardEmp = New DashBoardEmp
            x.IN1 = ds.Tables(0).Rows(i).Item("IN1").ToString.Trim
            x.WO_Value = ds.Tables(0).Rows(i).Item("WO_Value").ToString.Trim            
            If ds.Tables(0).Rows(i).Item("Leavevalue").ToString.Trim = "" Then
                x.Leavevalue = 0
            Else
                x.Leavevalue = ds.Tables(0).Rows(i).Item("Leavevalue").ToString.Trim
            End If
            If ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "" Then
                x.LATEARRIVAL = 0
            Else
                x.LATEARRIVAL = ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim
            End If

            DashBoardEmpLst.Add(x)
        Next
        DashBoardEmpArr = DashBoardEmpLst.ToArray

        Dim PresenCount As Double = 0
        Dim AbsentCount As Double = 0
        Dim WOCount As Double = 0
        Dim LeaveCount As Double = 0
        Dim LateCount As Double = 0

        For i As Integer = 0 To DashBoardEmpArr.Count - 1
            If DashBoardEmpArr(i).IN1 <> "" Then
                PresenCount = PresenCount + 1
            End If
            If DashBoardEmpArr(i).IN1 = "" Then
                AbsentCount = AbsentCount + 1
            End If
            If DashBoardEmpArr(i).WO_Value > 0 Then
                WOCount = WOCount + 1
            End If
            If DashBoardEmpArr(i).Leavevalue > 0 Then
                LeaveCount = LeaveCount + 1
            End If
            If DashBoardEmpArr(i).LATEARRIVAL > 0 Then
                LateCount = LateCount + 1
            End If
        Next
        TileItemPresent.Text = PresenCount
        TileItemAbsent.Text = AbsentCount
        TileItemWO.Text = WOCount
        TileItemLeave.Text = LeaveCount
        TileItemLate.Text = LateCount
        TileItemTotal.Text = DashBoardEmpArr.Count



        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim SeriesPoint1 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Present", New Object() {CType(PresenCount, Object)}, 0)
        Dim SeriesPoint2 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Absent", New Object() {CType(AbsentCount, Object)}, 1)
        Dim SeriesPoint3 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Week Off", New Object() {CType(WOCount, Object)}, 2)
        Dim SeriesPoint4 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Leave", New Object() {CType(LeaveCount, Object)}, 3)
        Dim SeriesPoint5 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Late", New Object() {CType(LateCount, Object)}, 4)
        Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Series1.Label.TextPattern = "{A} {V}"
        'PieSeriesLabel1.TextPattern = "{A} {V}"
        'Series1.Label = PieSeriesLabel1
        'Series1.Name = "Series 1"
        SeriesPoint1.ColorSerializable = "#F39C11" '"#32CD33" '"#FF8080"
        SeriesPoint2.ColorSerializable = "#DE4B39" '"Red" '"#FFFF80"
        SeriesPoint3.ColorSerializable = "#18A966" '"#BF00C2" '"#80FF80"
        SeriesPoint4.ColorSerializable = "#605CA8" '"#F0E78C" '"#80FF80"
        SeriesPoint5.ColorSerializable = "#A9891A" '"#FFC080" '"#80FF80"
        'ChartControl1.Series.Add(Series1)
        Series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2, SeriesPoint3, SeriesPoint4, SeriesPoint5})
        Series1.View = PieSeriesView1
        Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
    End Sub
    Private Sub XtraHomeNew_Leave(sender As System.Object, e As System.EventArgs) Handles MyBase.Leave
        TimerRefreshDashBoard.Enabled = False
        ''MsgBox("leave")
        'Dim svrPort As Integer = 0
        'svrPort = Convert.ToInt32(TextEditPortBio.Text.Trim)
        'If (Me.mbOpenFlag = True) Then
        '    AxRealSvrOcxTcp1.CloseNetwork(svrPort)
        '    Me.mbOpenFlag = False
        '    SimpleButtonStart.Enabled = True
        '    'Me.EnableButtons(True)
        'End If
        'Try
        '    tcp.Stop()   'for zk
        'Catch ex As Exception
        'End Try
    End Sub
    ' Get Host IP
    Protected Function GetIP() As String
        Dim ipHost As IPHostEntry = Dns.Resolve(Dns.GetHostName)
        Dim ipAddr As IPAddress = ipHost.AddressList(0)
        Return ipAddr.ToString
    End Function
    Private Sub TileItemPresent_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItemPresent.ItemClick
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Present"
        Dim daily As XtraReportsDaily = New XtraReportsDaily
        daily.SpotXl_PresentGrid("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemAbsent_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItemAbsent.ItemClick
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Absent"
        Dim daily As XtraReportsDaily = New XtraReportsDaily
        daily.SpotXl_AbsenteeismGrid("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemWO_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItemWO.ItemClick
        If TileItemWO.Text = "0" Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "WO"
        XtraDashBoardClickGrid.ShowDialog()
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemLeave_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItemLeave.ItemClick
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Leave"
        Dim daily As XtraLeaveReport = New XtraLeaveReport
        daily.MonthlyXl_SanctionedLeave("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemLate_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItemLate.ItemClick
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Late"
        Dim daily As XtraReportsDaily = New XtraReportsDaily
        daily.SpotXl_LateArrivalGrid("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub XtraHomeNew_VisibleChanged(sender As System.Object, e As System.EventArgs) Handles MyBase.VisibleChanged
        If MyBase.Visible = False Then
            TimerRefreshDashBoard.Enabled = False
        End If
    End Sub

    Private Sub TileItemTotal_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles TileItemTotal.ItemClick
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Employee"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployee
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
End Class
