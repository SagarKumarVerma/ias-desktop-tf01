﻿Imports System.Runtime.InteropServices
Class mdlFunction_Atf686n
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_ConnectComm(ByVal anMachineNo As Integer, ByVal anComPort As Integer, ByVal anBaudRate As Integer, ByVal astrTelNumber As String, ByVal anWaitDialTime As Integer, ByVal anLicense As Integer, ByVal anComTimeOut As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_ConnectNet(ByVal anMachineNo As Integer, ByVal astrIpAddress As String, ByVal anNetPort As Integer, ByVal anTimeOut As Integer, ByVal anProtocolType As Integer, ByVal anNetPassword As Integer, ByVal anLicense As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_ConnectUSB(ByVal anMachineNo As Integer, ByVal anLicense As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Sub ST_DisConnect(ByVal anHandleIndex As Integer)
    End Sub
    '} Connection

    '{ Error processing
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetLastError(ByVal anHandleIndex As Integer) As Integer
    End Function
    '} Error processing

    '{ Device Setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_EnableDevice(ByVal anHandleIndex As Integer, ByVal anEnableFlag As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetDeviceTime(ByVal anHandleIndex As Integer, ByRef apnDateTime As DateTime) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetDeviceTime(ByVal anHandleIndex As Integer, ByVal anDateTime As DateTime) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetDeviceInfo(ByVal anHandleIndex As Integer, ByVal anInfoIndex As Integer, ByRef apnValue As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetDeviceInfo(ByVal anHandleIndex As Integer, ByVal anInfoIndex As Integer, ByVal anValue As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_PowerOffDevice(ByVal anHandleIndex As Integer) As Integer
    End Function

    '} Device Setting

    '{ Log Data


    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_LoadGeneralLogData(ByVal anHandleIndex As Integer, ByVal anReadMark As Integer) As Integer
    End Function



    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_EmptyGeneralLogData(ByVal anHandleIndex As Integer) As Integer
    End Function
    '} Log Data

    '{ Enroll Data, User Name, Message
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_EnableUser_SID(ByVal anHandleIndex As Integer, ByVal strEnrollNumber As String, ByVal anBackupNumber As Integer, ByVal anEnableFlag As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_BenumbAllManager(ByVal anHandleIndex As Integer) As Integer
    End Function



    ' nabi added

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetIsSupportStringID(ByVal anHandleIndex As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetEnrollMainData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apEnrollData() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetEnrollMainData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apEnrollData() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetOnlyUserName_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apEnrollData() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetOnlyUserName_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apEnrollData() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_UserEnroll_SID(ByVal anHandleIndex As Integer, ByVal apEnrollID As String, ByVal apEnrollName() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetEnrollSettingData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apEnrollSettingData() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetEnrollSettingData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apEnrollSettingData() As Byte) As Integer
    End Function


    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetUserName_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal astrUserName As String) As Integer
    End Function


    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetLogDataIsSupportStringID(ByVal anHandleIndex As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetGeneralLogData_SID(ByVal anHandleIndex As Integer, <MarshalAs(UnmanagedType.LPStr)> ByRef apnEnrollNumber As String, ByRef apnVerifyMode As Integer, ByRef apnInOutMode As Integer, ByRef apnDateTime As DateTime) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_DeleteEnrollData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal bLogDeleteWithUser As Boolean) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetUserName_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, <MarshalAs(UnmanagedType.LPStr)> ByRef apstrUserName As String) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetEnrollPhoto_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apPhotoImage() As Byte, ByRef apnPhotoLength As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetEnrollPhoto_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal apPhotoImage() As Byte, ByVal anPhotoLength As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_DeleteEnrollPhoto_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_EmptyEnrollData(ByVal anHandleIndex As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_ClearAllData(ByVal anHandleIndex As Integer) As Integer
    End Function



    '{ BELL setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetBellTime(ByVal anHandleIndex As Integer, ByVal apnBellInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetBellTime(ByVal anHandleIndex As Integer, ByVal apnBellInfo() As Byte) As Integer
    End Function
    '} BELL setting

    '{ Shift Time Setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetShiftInfo(ByVal anHandleIndex As Integer, ByVal apnShiftInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetShiftInfo(ByVal anHandleIndex As Integer, ByVal apnShiftInfo() As Byte) As Integer
    End Function
    '} Shift Time Setting


    '{ Post Setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetPostInfo(ByVal anHandleIndex As Integer, ByVal apnShiftInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetPostInfo(ByVal anHandleIndex As Integer, ByVal apnShiftInfo() As Byte) As Integer
    End Function
    '} Post Setting

    '{ ShiftCycle setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetShiftCycle(ByVal anHandleIndex As Integer, ByVal apnShiftCycleInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetShiftCycle(ByVal anHandleIndex As Integer, ByVal apnShiftCycleInfo() As Byte) As Integer
    End Function
    '} ShiftCycle setting

    '{ AccessGroup setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetAccessGroup(ByVal anHandleIndex As Integer, ByVal apnAccessGroupInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_AddAccessGroup(ByVal anHandleIndex As Integer, ByVal apnAccessGroupInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetAccessGroup(ByVal anHandleIndex As Integer, ByVal apnAccessGroupInfo() As Byte) As Integer
    End Function
    '} AccessGroup setting

    '{ Holiday setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetHoliday(ByVal anHandleIndex As Integer, ByVal apnHolidayInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_AddHoliday(ByVal anHandleIndex As Integer, ByVal apnHolidayInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetHoliday(ByVal anHandleIndex As Integer, ByVal apnHolidayInfo() As Byte) As Integer
    End Function
    '} Holiday setting


    '{ FKey Mapping setting
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetFKeyInfo(ByVal anHandleIndex As Integer, ByVal apnShiftCycleInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetFKeyInfo(ByVal anHandleIndex As Integer, ByVal apnShiftCycleInfo() As Byte) As Integer
    End Function
    '} FKey Mapping setting

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetPassTime(ByVal anHandleIndex As Integer, ByVal anPassTimeID As Integer, ByVal apnPassTime() As Byte, ByVal anPassTimeSize As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetPassTime(ByVal anHandleIndex As Integer, ByVal anPassTimeID As Integer, ByVal apnPassTime() As Byte, ByVal anPassTimeSize As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetPermissionsPerRole(ByVal anHandleIndex As Integer, ByVal anRoleIndex As Integer, ByVal apnPermissions() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetPermissionsPerRole(ByVal anHandleIndex As Integer, ByVal anRoleIndex As Integer, ByVal apnPermissions() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetDSTInfo(ByVal anHandleIndex As Integer, ByVal abytDSTInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetDSTInfo(ByVal anHandleIndex As Integer, ByVal abytDSTInfo() As Byte) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetLOGO(ByVal anHandleIndex As Integer, ByVal apPhotoImage() As Byte, ByVal anPhotoLength As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetLOGOBmp(ByVal anHandleIndex As Integer, ByVal apPhotoImage() As Byte, ByVal anPhotoLength As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_ReadAllUserID(ByVal anHandleIndex As Integer) As Integer
    End Function

    'public static extern int FK_GetAllUserID_StringID(int anHandleIndex, [MarshalAs(UnmanagedType.LPStr)] ref string apEnrollNumber, ref int apnBackupNumber, ref int apnMachinePrivilege, ref int apnEnableFlag); 
    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetAllUserID_SID(ByVal anHandleIndex As Integer, ByRef apEnrollNumber As String, ByRef apnBackupNumber As Integer, ByRef apnMachinePrivilege As Integer, ByRef apnEnableFlag As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_GetEnrollData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal anBackupNumber As Integer, ByRef apnMachinePrivilege As Integer, ByVal apEnrollData() As Byte, ByRef apnPassWord As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_IsSupportedEnrollData(ByVal nHandleIndex As Integer, ByVal anBackupNumber As Integer, ByRef apnSupportFlag As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SetEnrollData_SID(ByVal anHandleIndex As Integer, ByVal apEnrollNumber As String, ByVal anBackupNumber As Integer, ByVal anMachinePrivilege As Integer, ByVal apEnrollData() As Byte, ByVal anPassWord As Integer) As Integer
    End Function

    <DllImport("ComMain", CharSet:=CharSet.Ansi)>
    Public Shared Function ST_SaveEnrollData(ByVal anHandleIndex As Integer) As Integer
    End Function

    Private Const NETWORK_DEVICE As Integer = 1
    Private Const SERIAL_DEVICE As Integer = 2
    Private Const USB_DEVICE As Integer = 3
    Private Const DEF_LICENSE As Integer = 7881
    Public Function ConnectUsb() As Integer
        Dim vnMachineNumber As Integer
        Dim vnCommPort As Integer
        Dim vnCommBaudrate As Integer
        Dim vstrTelNumber As String = ""
        Dim vnWaitDialTime As Integer = 3000
        Dim vnLicense As Integer
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Integer = 5000
        Dim vnProtocolType As Integer
        Dim vnResultCode As Long = 0


        vnMachineNumber = 0

        vnLicense = DEF_LICENSE
        vnCommPort = Convert.ToInt32(("1").Trim())
        vnCommBaudrate = Convert.ToInt32(("9600").Trim())
        Return ST_ConnectComm(vnMachineNumber, vnCommPort, vnCommBaudrate, vstrTelNumber, vnWaitDialTime, vnLicense, vnTimeOut)

    End Function

    Public Function ConnectNet(vpszIPAddress As String) As Integer
        Dim vnMachineNumber As Integer = 1
        Dim vnCommPort As Integer
        Dim vnCommBaudrate As Integer
        Dim vstrTelNumber As String = ""
        Dim vnWaitDialTime As Integer = 3000
        Dim vnLicense As Integer = 7881
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Integer = 5000
        Dim vnProtocolType As Integer
        Dim vnResultCode As Long = 0
        'vpszIPAddress = (txtIPAddress.Text).Trim()
        vpszNetPort = 5005 'Convert.ToInt32(txtPortNo.Text)       
        vpszNetPassword = 0
       vnProtocolType = CInt(enumProtocolType.PROTOCOL_TCPIP)
        Return ST_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)

    End Function
End Class