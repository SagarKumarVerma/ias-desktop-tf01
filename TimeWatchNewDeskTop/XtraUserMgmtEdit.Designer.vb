﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraUserMgmtEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDes = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPwd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditUsr = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPrevileges = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchDevice = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchVisitor = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchCanteen = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchReports = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchAutoProcess = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchAdmin = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchPay = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchLeaveMgmt = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchDataPtrocess = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchTransaction = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchMain = New DevExpress.XtraEditors.ToggleSwitch()
        Me.XtraTabDevice = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditUserSetup = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditLogMgmt = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl10 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditDDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabMaster = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl9 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditBranchDel = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBranchEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBranchAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBranch = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMarlAll = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl8 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditEmpDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditEmpEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditEmpAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditEmp = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditShiftDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditShiftEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditShiftAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditShift = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl6 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditCatDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCatEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCatAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCat = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditGrdDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditGrdEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditGrdAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditGrade = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditSecDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSecEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSecAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSec = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditDeptDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDeptEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDeptAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDept = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditCompDelete = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCompEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCompAdd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditComp = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabTransaction = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditDataMaintenance = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditHoliday = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditShiftShange = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOverStayToOT = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPunchEntry = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabDataProcess = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditVerification = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBckDate = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAttResUpd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAttResCre = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabLeave = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditMleaveInc = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditLeaveApp = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditLeaveAcc = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditLeaveMaster = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabAdmin = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditParallel = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBackup = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditEmail = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBulkSMS = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSMS = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDBSetting = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditUserPre = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCommonSetting = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabReports = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditLeaveR = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDailyR = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMonthlyR = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPayReport = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditTimeReport = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabPayroll = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleCompl = New DevExpress.XtraEditors.ToggleSwitch()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPwd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUsr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPrevileges.SuspendLayout()
        CType(Me.ToggleSwitchDevice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchReports.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchAutoProcess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchAdmin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchLeaveMgmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchDataPtrocess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchTransaction.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchMain.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabDevice.SuspendLayout()
        CType(Me.CheckEditUserSetup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLogMgmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl10.SuspendLayout()
        CType(Me.CheckEditDDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabMaster.SuspendLayout()
        CType(Me.PanelControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl9.SuspendLayout()
        CType(Me.CheckEditBranchDel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBranchEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBranchAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBranch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditMarlAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl8.SuspendLayout()
        CType(Me.CheckEditEmpDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditEmpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditEmpAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.CheckEditShiftDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditShiftEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditShiftAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl6.SuspendLayout()
        CType(Me.CheckEditCatDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCatEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCatAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        CType(Me.CheckEditGrdDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditGrdEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditGrdAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.CheckEditSecDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditSecEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditSecAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditSec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.CheckEditDeptDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDeptEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDeptAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.CheckEditCompDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCompEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCompAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabTransaction.SuspendLayout()
        CType(Me.CheckEditDataMaintenance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditHoliday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditShiftShange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOverStayToOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPunchEntry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabDataProcess.SuspendLayout()
        CType(Me.CheckEditVerification.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBckDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAttResUpd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAttResCre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabLeave.SuspendLayout()
        CType(Me.CheckEditMleaveInc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLeaveApp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLeaveAcc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLeaveMaster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabAdmin.SuspendLayout()
        CType(Me.CheckEditParallel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBackup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBulkSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDBSetting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditUserPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCommonSetting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabReports.SuspendLayout()
        CType(Me.CheckEditLeaveR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDailyR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditMonthlyR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPayReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditTimeReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleCompl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.PopupContainerEditLocation)
        Me.PanelControl1.Controls.Add(Me.PopupContainerEditComp)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.ComboBoxEdit2)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.TextEditDes)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.TextEditPwd)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.TextEditUsr)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(723, 119)
        Me.PanelControl1.TabIndex = 0
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(338, 66)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl14.TabIndex = 20
        Me.LabelControl14.Text = "Select Location"
        Me.LabelControl14.Visible = False
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(435, 63)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditLocation.TabIndex = 19
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Employees"
        Me.PopupContainerEditLocation.Visible = False
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(17, 464)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 25
        '
        'GridControlBranch
        '
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit2})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(435, 37)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 11
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Employees"
        Me.PopupContainerEditComp.Visible = False
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(323, 461)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 105
        '
        'GridControlComp
        '
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(338, 39)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl13.TabIndex = 12
        Me.LabelControl13.Text = "Select Company"
        Me.LabelControl13.Visible = False
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.EditValue = "Administrator"
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(111, 63)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Properties.Items.AddRange(New Object() {"Administrator", "Head Of Department"})
        Me.ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(150, 20)
        Me.ComboBoxEdit2.TabIndex = 10
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(17, 66)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl12.TabIndex = 9
        Me.LabelControl12.Text = "User Description"
        '
        'TextEditDes
        '
        Me.TextEditDes.Location = New System.Drawing.Point(111, 37)
        Me.TextEditDes.Name = "TextEditDes"
        Me.TextEditDes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDes.Properties.Appearance.Options.UseFont = True
        Me.TextEditDes.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEditDes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditDes.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditDes.Properties.MaxLength = 50
        Me.TextEditDes.Size = New System.Drawing.Size(150, 20)
        Me.TextEditDes.TabIndex = 8
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(17, 40)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl3.TabIndex = 7
        Me.LabelControl3.Text = "User Description"
        '
        'TextEditPwd
        '
        Me.TextEditPwd.Location = New System.Drawing.Point(435, 11)
        Me.TextEditPwd.Name = "TextEditPwd"
        Me.TextEditPwd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPwd.Properties.Appearance.Options.UseFont = True
        Me.TextEditPwd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPwd.Properties.MaxLength = 20
        Me.TextEditPwd.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextEditPwd.Size = New System.Drawing.Size(150, 20)
        Me.TextEditPwd.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(369, 14)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Password "
        '
        'TextEditUsr
        '
        Me.TextEditUsr.Location = New System.Drawing.Point(111, 11)
        Me.TextEditUsr.Name = "TextEditUsr"
        Me.TextEditUsr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUsr.Properties.Appearance.Options.UseFont = True
        Me.TextEditUsr.Properties.Mask.EditMask = "[a-zA-Z0-9]*"
        Me.TextEditUsr.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditUsr.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditUsr.Properties.MaxLength = 20
        Me.TextEditUsr.Size = New System.Drawing.Size(150, 20)
        Me.TextEditUsr.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(17, 14)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "User Name * "
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.XtraTabControl1.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.Blue
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseForeColor = True
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = True
        Me.XtraTabControl1.Location = New System.Drawing.Point(12, 137)
        Me.XtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.[True]
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPrevileges
        Me.XtraTabControl1.Size = New System.Drawing.Size(810, 330)
        Me.XtraTabControl1.TabIndex = 1
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPrevileges, Me.XtraTabDevice, Me.XtraTabMaster, Me.XtraTabTransaction, Me.XtraTabDataProcess, Me.XtraTabLeave, Me.XtraTabAdmin, Me.XtraTabReports, Me.XtraTabPayroll})
        '
        'XtraTabPrevileges
        '
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl19)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleCompl)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl17)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchDevice)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl16)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchVisitor)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl15)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchCanteen)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl11)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchReports)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl10)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchAutoProcess)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl9)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchAdmin)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl8)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchPay)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl7)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchLeaveMgmt)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl6)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchDataPtrocess)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl5)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchTransaction)
        Me.XtraTabPrevileges.Controls.Add(Me.LabelControl4)
        Me.XtraTabPrevileges.Controls.Add(Me.ToggleSwitchMain)
        Me.XtraTabPrevileges.Name = "XtraTabPrevileges"
        Me.XtraTabPrevileges.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabPrevileges.Text = "Previleges"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(15, 175)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl17.TabIndex = 25
        Me.LabelControl17.Text = "Device"
        '
        'ToggleSwitchDevice
        '
        Me.ToggleSwitchDevice.Location = New System.Drawing.Point(152, 170)
        Me.ToggleSwitchDevice.Name = "ToggleSwitchDevice"
        Me.ToggleSwitchDevice.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchDevice.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchDevice.Properties.OffText = "No"
        Me.ToggleSwitchDevice.Properties.OnText = "Yes"
        Me.ToggleSwitchDevice.Properties.ValueOff = "N"
        Me.ToggleSwitchDevice.Properties.ValueOn = "Y"
        Me.ToggleSwitchDevice.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchDevice.TabIndex = 24
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(357, 144)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl16.TabIndex = 23
        Me.LabelControl16.Text = "Visitor"
        '
        'ToggleSwitchVisitor
        '
        Me.ToggleSwitchVisitor.Location = New System.Drawing.Point(494, 139)
        Me.ToggleSwitchVisitor.Name = "ToggleSwitchVisitor"
        Me.ToggleSwitchVisitor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchVisitor.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchVisitor.Properties.OffText = "No"
        Me.ToggleSwitchVisitor.Properties.OnText = "Yes"
        Me.ToggleSwitchVisitor.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchVisitor.TabIndex = 22
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(357, 113)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl15.TabIndex = 21
        Me.LabelControl15.Text = "Canteen"
        '
        'ToggleSwitchCanteen
        '
        Me.ToggleSwitchCanteen.Location = New System.Drawing.Point(494, 108)
        Me.ToggleSwitchCanteen.Name = "ToggleSwitchCanteen"
        Me.ToggleSwitchCanteen.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchCanteen.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchCanteen.Properties.OffText = "No"
        Me.ToggleSwitchCanteen.Properties.OnText = "Yes"
        Me.ToggleSwitchCanteen.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchCanteen.TabIndex = 20
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(357, 82)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl11.TabIndex = 19
        Me.LabelControl11.Text = "Reports"
        '
        'ToggleSwitchReports
        '
        Me.ToggleSwitchReports.Location = New System.Drawing.Point(494, 77)
        Me.ToggleSwitchReports.Name = "ToggleSwitchReports"
        Me.ToggleSwitchReports.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchReports.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchReports.Properties.OffText = "No"
        Me.ToggleSwitchReports.Properties.OnText = "Yes"
        Me.ToggleSwitchReports.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchReports.TabIndex = 18
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(357, 51)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(135, 14)
        Me.LabelControl10.TabIndex = 17
        Me.LabelControl10.Text = "Auto Process on Startup"
        '
        'ToggleSwitchAutoProcess
        '
        Me.ToggleSwitchAutoProcess.Location = New System.Drawing.Point(494, 46)
        Me.ToggleSwitchAutoProcess.Name = "ToggleSwitchAutoProcess"
        Me.ToggleSwitchAutoProcess.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchAutoProcess.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchAutoProcess.Properties.OffText = "No"
        Me.ToggleSwitchAutoProcess.Properties.OnText = "Yes"
        Me.ToggleSwitchAutoProcess.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchAutoProcess.TabIndex = 16
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(357, 20)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl9.TabIndex = 15
        Me.LabelControl9.Text = "Admin"
        '
        'ToggleSwitchAdmin
        '
        Me.ToggleSwitchAdmin.Location = New System.Drawing.Point(494, 15)
        Me.ToggleSwitchAdmin.Name = "ToggleSwitchAdmin"
        Me.ToggleSwitchAdmin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchAdmin.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchAdmin.Properties.OffText = "No"
        Me.ToggleSwitchAdmin.Properties.OnText = "Yes"
        Me.ToggleSwitchAdmin.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchAdmin.TabIndex = 14
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(15, 144)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(109, 14)
        Me.LabelControl8.TabIndex = 13
        Me.LabelControl8.Text = "Payroll Management"
        '
        'ToggleSwitchPay
        '
        Me.ToggleSwitchPay.Location = New System.Drawing.Point(152, 139)
        Me.ToggleSwitchPay.Name = "ToggleSwitchPay"
        Me.ToggleSwitchPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchPay.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchPay.Properties.OffText = "No"
        Me.ToggleSwitchPay.Properties.OnText = "Yes"
        Me.ToggleSwitchPay.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchPay.TabIndex = 12
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(15, 113)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl7.TabIndex = 11
        Me.LabelControl7.Text = "Leave Management"
        '
        'ToggleSwitchLeaveMgmt
        '
        Me.ToggleSwitchLeaveMgmt.Location = New System.Drawing.Point(152, 108)
        Me.ToggleSwitchLeaveMgmt.Name = "ToggleSwitchLeaveMgmt"
        Me.ToggleSwitchLeaveMgmt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchLeaveMgmt.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchLeaveMgmt.Properties.OffText = "No"
        Me.ToggleSwitchLeaveMgmt.Properties.OnText = "Yes"
        Me.ToggleSwitchLeaveMgmt.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchLeaveMgmt.TabIndex = 10
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(15, 82)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl6.TabIndex = 9
        Me.LabelControl6.Text = "Data Process"
        '
        'ToggleSwitchDataPtrocess
        '
        Me.ToggleSwitchDataPtrocess.Location = New System.Drawing.Point(152, 77)
        Me.ToggleSwitchDataPtrocess.Name = "ToggleSwitchDataPtrocess"
        Me.ToggleSwitchDataPtrocess.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchDataPtrocess.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchDataPtrocess.Properties.OffText = "No"
        Me.ToggleSwitchDataPtrocess.Properties.OnText = "Yes"
        Me.ToggleSwitchDataPtrocess.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchDataPtrocess.TabIndex = 8
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(15, 51)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl5.TabIndex = 7
        Me.LabelControl5.Text = "Transaction"
        '
        'ToggleSwitchTransaction
        '
        Me.ToggleSwitchTransaction.Location = New System.Drawing.Point(152, 46)
        Me.ToggleSwitchTransaction.Name = "ToggleSwitchTransaction"
        Me.ToggleSwitchTransaction.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchTransaction.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchTransaction.Properties.OffText = "No"
        Me.ToggleSwitchTransaction.Properties.OnText = "Yes"
        Me.ToggleSwitchTransaction.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchTransaction.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(15, 20)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "Master"
        '
        'ToggleSwitchMain
        '
        Me.ToggleSwitchMain.Location = New System.Drawing.Point(152, 15)
        Me.ToggleSwitchMain.Name = "ToggleSwitchMain"
        Me.ToggleSwitchMain.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchMain.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchMain.Properties.OffText = "No"
        Me.ToggleSwitchMain.Properties.OnText = "Yes"
        Me.ToggleSwitchMain.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchMain.TabIndex = 4
        '
        'XtraTabDevice
        '
        Me.XtraTabDevice.Controls.Add(Me.LabelControl18)
        Me.XtraTabDevice.Controls.Add(Me.CheckEditUserSetup)
        Me.XtraTabDevice.Controls.Add(Me.CheckEditLogMgmt)
        Me.XtraTabDevice.Controls.Add(Me.PanelControl10)
        Me.XtraTabDevice.Name = "XtraTabDevice"
        Me.XtraTabDevice.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabDevice.Text = "Device"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(30, 27)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl18.TabIndex = 26
        Me.LabelControl18.Text = "Device"
        '
        'CheckEditUserSetup
        '
        Me.CheckEditUserSetup.Location = New System.Drawing.Point(15, 135)
        Me.CheckEditUserSetup.Name = "CheckEditUserSetup"
        Me.CheckEditUserSetup.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditUserSetup.Properties.Appearance.Options.UseFont = True
        Me.CheckEditUserSetup.Properties.Caption = "User Setup"
        Me.CheckEditUserSetup.Properties.DisplayValueChecked = "Y"
        Me.CheckEditUserSetup.Properties.DisplayValueGrayed = "N"
        Me.CheckEditUserSetup.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditUserSetup.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditUserSetup.Properties.ValueChecked = "Y"
        Me.CheckEditUserSetup.Properties.ValueGrayed = "N"
        Me.CheckEditUserSetup.Properties.ValueUnchecked = "N"
        Me.CheckEditUserSetup.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditUserSetup.TabIndex = 25
        '
        'CheckEditLogMgmt
        '
        Me.CheckEditLogMgmt.Location = New System.Drawing.Point(15, 78)
        Me.CheckEditLogMgmt.Name = "CheckEditLogMgmt"
        Me.CheckEditLogMgmt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLogMgmt.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLogMgmt.Properties.Caption = "Log Management"
        Me.CheckEditLogMgmt.Properties.DisplayValueChecked = "Y"
        Me.CheckEditLogMgmt.Properties.DisplayValueGrayed = "N"
        Me.CheckEditLogMgmt.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditLogMgmt.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditLogMgmt.Properties.ValueChecked = "Y"
        Me.CheckEditLogMgmt.Properties.ValueGrayed = "N"
        Me.CheckEditLogMgmt.Properties.ValueUnchecked = "N"
        Me.CheckEditLogMgmt.Size = New System.Drawing.Size(183, 19)
        Me.CheckEditLogMgmt.TabIndex = 24
        '
        'PanelControl10
        '
        Me.PanelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl10.Controls.Add(Me.CheckEditDDelete)
        Me.PanelControl10.Controls.Add(Me.CheckEditDEdit)
        Me.PanelControl10.Controls.Add(Me.CheckEditDAdd)
        Me.PanelControl10.Location = New System.Drawing.Point(118, 20)
        Me.PanelControl10.Name = "PanelControl10"
        Me.PanelControl10.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl10.TabIndex = 23
        '
        'CheckEditDDelete
        '
        Me.CheckEditDDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditDDelete.Name = "CheckEditDDelete"
        Me.CheckEditDDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditDDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditDDelete.Properties.Caption = "Delete"
        Me.CheckEditDDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDDelete.Properties.ValueChecked = "Y"
        Me.CheckEditDDelete.Properties.ValueGrayed = "N"
        Me.CheckEditDDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditDDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditDDelete.TabIndex = 3
        '
        'CheckEditDEdit
        '
        Me.CheckEditDEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditDEdit.Name = "CheckEditDEdit"
        Me.CheckEditDEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditDEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditDEdit.Properties.Caption = "Edit"
        Me.CheckEditDEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDEdit.Properties.ValueChecked = "Y"
        Me.CheckEditDEdit.Properties.ValueGrayed = "N"
        Me.CheckEditDEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditDEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditDEdit.TabIndex = 1
        '
        'CheckEditDAdd
        '
        Me.CheckEditDAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditDAdd.Name = "CheckEditDAdd"
        Me.CheckEditDAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditDAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditDAdd.Properties.Caption = "Add"
        Me.CheckEditDAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDAdd.Properties.ValueChecked = "Y"
        Me.CheckEditDAdd.Properties.ValueGrayed = "N"
        Me.CheckEditDAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditDAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditDAdd.TabIndex = 2
        '
        'XtraTabMaster
        '
        Me.XtraTabMaster.Controls.Add(Me.PanelControl9)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditBranch)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditMarlAll)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl8)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditEmp)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl7)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditShift)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl6)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditCat)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl5)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditGrade)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl4)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditSec)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl3)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditDept)
        Me.XtraTabMaster.Controls.Add(Me.PanelControl2)
        Me.XtraTabMaster.Controls.Add(Me.CheckEditComp)
        Me.XtraTabMaster.Name = "XtraTabMaster"
        Me.XtraTabMaster.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabMaster.Text = "Master"
        '
        'PanelControl9
        '
        Me.PanelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl9.Controls.Add(Me.CheckEditBranchDel)
        Me.PanelControl9.Controls.Add(Me.CheckEditBranchEdit)
        Me.PanelControl9.Controls.Add(Me.CheckEditBranchAdd)
        Me.PanelControl9.Location = New System.Drawing.Point(118, 54)
        Me.PanelControl9.Name = "PanelControl9"
        Me.PanelControl9.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl9.TabIndex = 19
        '
        'CheckEditBranchDel
        '
        Me.CheckEditBranchDel.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditBranchDel.Name = "CheckEditBranchDel"
        Me.CheckEditBranchDel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBranchDel.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditBranchDel.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBranchDel.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditBranchDel.Properties.Caption = "Delete"
        Me.CheckEditBranchDel.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBranchDel.Properties.ValueChecked = "Y"
        Me.CheckEditBranchDel.Properties.ValueGrayed = "N"
        Me.CheckEditBranchDel.Properties.ValueUnchecked = "N"
        Me.CheckEditBranchDel.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditBranchDel.TabIndex = 3
        '
        'CheckEditBranchEdit
        '
        Me.CheckEditBranchEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditBranchEdit.Name = "CheckEditBranchEdit"
        Me.CheckEditBranchEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBranchEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditBranchEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBranchEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditBranchEdit.Properties.Caption = "Edit"
        Me.CheckEditBranchEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBranchEdit.Properties.ValueChecked = "Y"
        Me.CheckEditBranchEdit.Properties.ValueGrayed = "N"
        Me.CheckEditBranchEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditBranchEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditBranchEdit.TabIndex = 1
        '
        'CheckEditBranchAdd
        '
        Me.CheckEditBranchAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditBranchAdd.Name = "CheckEditBranchAdd"
        Me.CheckEditBranchAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBranchAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditBranchAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBranchAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditBranchAdd.Properties.Caption = "Add"
        Me.CheckEditBranchAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBranchAdd.Properties.ValueChecked = "Y"
        Me.CheckEditBranchAdd.Properties.ValueGrayed = "N"
        Me.CheckEditBranchAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditBranchAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditBranchAdd.TabIndex = 2
        '
        'CheckEditBranch
        '
        Me.CheckEditBranch.Location = New System.Drawing.Point(15, 59)
        Me.CheckEditBranch.Name = "CheckEditBranch"
        Me.CheckEditBranch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBranch.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBranch.Properties.Caption = "Location"
        Me.CheckEditBranch.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBranch.Properties.ValueChecked = "Y"
        Me.CheckEditBranch.Properties.ValueGrayed = "N"
        Me.CheckEditBranch.Properties.ValueUnchecked = "N"
        Me.CheckEditBranch.Size = New System.Drawing.Size(97, 19)
        Me.CheckEditBranch.TabIndex = 18
        '
        'CheckEditMarlAll
        '
        Me.CheckEditMarlAll.Location = New System.Drawing.Point(673, 25)
        Me.CheckEditMarlAll.Name = "CheckEditMarlAll"
        Me.CheckEditMarlAll.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditMarlAll.Properties.Appearance.Options.UseFont = True
        Me.CheckEditMarlAll.Properties.Caption = "Mark All"
        Me.CheckEditMarlAll.Properties.DisplayValueChecked = "Y"
        Me.CheckEditMarlAll.Properties.DisplayValueGrayed = "N"
        Me.CheckEditMarlAll.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditMarlAll.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditMarlAll.Properties.ValueChecked = "Y"
        Me.CheckEditMarlAll.Properties.ValueGrayed = "N"
        Me.CheckEditMarlAll.Properties.ValueUnchecked = "N"
        Me.CheckEditMarlAll.Size = New System.Drawing.Size(88, 19)
        Me.CheckEditMarlAll.TabIndex = 17
        '
        'PanelControl8
        '
        Me.PanelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl8.Controls.Add(Me.CheckEditEmpDelete)
        Me.PanelControl8.Controls.Add(Me.CheckEditEmpEdit)
        Me.PanelControl8.Controls.Add(Me.CheckEditEmpAdd)
        Me.PanelControl8.Location = New System.Drawing.Point(118, 258)
        Me.PanelControl8.Name = "PanelControl8"
        Me.PanelControl8.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl8.TabIndex = 16
        '
        'CheckEditEmpDelete
        '
        Me.CheckEditEmpDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditEmpDelete.Name = "CheckEditEmpDelete"
        Me.CheckEditEmpDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditEmpDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditEmpDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditEmpDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditEmpDelete.Properties.Caption = "Delete"
        Me.CheckEditEmpDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditEmpDelete.Properties.ValueChecked = "Y"
        Me.CheckEditEmpDelete.Properties.ValueGrayed = "N"
        Me.CheckEditEmpDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditEmpDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditEmpDelete.TabIndex = 3
        '
        'CheckEditEmpEdit
        '
        Me.CheckEditEmpEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditEmpEdit.Name = "CheckEditEmpEdit"
        Me.CheckEditEmpEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditEmpEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditEmpEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditEmpEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditEmpEdit.Properties.Caption = "Edit"
        Me.CheckEditEmpEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditEmpEdit.Properties.ValueChecked = "Y"
        Me.CheckEditEmpEdit.Properties.ValueGrayed = "N"
        Me.CheckEditEmpEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditEmpEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditEmpEdit.TabIndex = 1
        '
        'CheckEditEmpAdd
        '
        Me.CheckEditEmpAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditEmpAdd.Name = "CheckEditEmpAdd"
        Me.CheckEditEmpAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditEmpAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditEmpAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditEmpAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditEmpAdd.Properties.Caption = "Add"
        Me.CheckEditEmpAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditEmpAdd.Properties.ValueChecked = "Y"
        Me.CheckEditEmpAdd.Properties.ValueGrayed = "N"
        Me.CheckEditEmpAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditEmpAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditEmpAdd.TabIndex = 2
        '
        'CheckEditEmp
        '
        Me.CheckEditEmp.Location = New System.Drawing.Point(15, 263)
        Me.CheckEditEmp.Name = "CheckEditEmp"
        Me.CheckEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditEmp.Properties.Appearance.Options.UseFont = True
        Me.CheckEditEmp.Properties.Caption = "Employee"
        Me.CheckEditEmp.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditEmp.Properties.ValueChecked = "Y"
        Me.CheckEditEmp.Properties.ValueGrayed = "N"
        Me.CheckEditEmp.Properties.ValueUnchecked = "N"
        Me.CheckEditEmp.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditEmp.TabIndex = 15
        '
        'PanelControl7
        '
        Me.PanelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl7.Controls.Add(Me.CheckEditShiftDelete)
        Me.PanelControl7.Controls.Add(Me.CheckEditShiftEdit)
        Me.PanelControl7.Controls.Add(Me.CheckEditShiftAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(118, 224)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl7.TabIndex = 14
        '
        'CheckEditShiftDelete
        '
        Me.CheckEditShiftDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditShiftDelete.Name = "CheckEditShiftDelete"
        Me.CheckEditShiftDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditShiftDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditShiftDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditShiftDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditShiftDelete.Properties.Caption = "Delete"
        Me.CheckEditShiftDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditShiftDelete.Properties.ValueChecked = "Y"
        Me.CheckEditShiftDelete.Properties.ValueGrayed = "N"
        Me.CheckEditShiftDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditShiftDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditShiftDelete.TabIndex = 3
        '
        'CheckEditShiftEdit
        '
        Me.CheckEditShiftEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditShiftEdit.Name = "CheckEditShiftEdit"
        Me.CheckEditShiftEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditShiftEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditShiftEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditShiftEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditShiftEdit.Properties.Caption = "Edit"
        Me.CheckEditShiftEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditShiftEdit.Properties.ValueChecked = "Y"
        Me.CheckEditShiftEdit.Properties.ValueGrayed = "N"
        Me.CheckEditShiftEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditShiftEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditShiftEdit.TabIndex = 1
        '
        'CheckEditShiftAdd
        '
        Me.CheckEditShiftAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditShiftAdd.Name = "CheckEditShiftAdd"
        Me.CheckEditShiftAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditShiftAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditShiftAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditShiftAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditShiftAdd.Properties.Caption = "Add"
        Me.CheckEditShiftAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditShiftAdd.Properties.ValueChecked = "Y"
        Me.CheckEditShiftAdd.Properties.ValueGrayed = "N"
        Me.CheckEditShiftAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditShiftAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditShiftAdd.TabIndex = 2
        '
        'CheckEditShift
        '
        Me.CheckEditShift.Location = New System.Drawing.Point(15, 229)
        Me.CheckEditShift.Name = "CheckEditShift"
        Me.CheckEditShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditShift.Properties.Appearance.Options.UseFont = True
        Me.CheckEditShift.Properties.Caption = "Shift"
        Me.CheckEditShift.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditShift.Properties.ValueChecked = "Y"
        Me.CheckEditShift.Properties.ValueGrayed = "N"
        Me.CheckEditShift.Properties.ValueUnchecked = "N"
        Me.CheckEditShift.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditShift.TabIndex = 13
        '
        'PanelControl6
        '
        Me.PanelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl6.Controls.Add(Me.CheckEditCatDelete)
        Me.PanelControl6.Controls.Add(Me.CheckEditCatEdit)
        Me.PanelControl6.Controls.Add(Me.CheckEditCatAdd)
        Me.PanelControl6.Location = New System.Drawing.Point(118, 190)
        Me.PanelControl6.Name = "PanelControl6"
        Me.PanelControl6.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl6.TabIndex = 12
        '
        'CheckEditCatDelete
        '
        Me.CheckEditCatDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditCatDelete.Name = "CheckEditCatDelete"
        Me.CheckEditCatDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCatDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditCatDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCatDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditCatDelete.Properties.Caption = "Delete"
        Me.CheckEditCatDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCatDelete.Properties.ValueChecked = "Y"
        Me.CheckEditCatDelete.Properties.ValueGrayed = "N"
        Me.CheckEditCatDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditCatDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditCatDelete.TabIndex = 3
        '
        'CheckEditCatEdit
        '
        Me.CheckEditCatEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditCatEdit.Name = "CheckEditCatEdit"
        Me.CheckEditCatEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCatEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditCatEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCatEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditCatEdit.Properties.Caption = "Edit"
        Me.CheckEditCatEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCatEdit.Properties.ValueChecked = "Y"
        Me.CheckEditCatEdit.Properties.ValueGrayed = "N"
        Me.CheckEditCatEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditCatEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditCatEdit.TabIndex = 1
        '
        'CheckEditCatAdd
        '
        Me.CheckEditCatAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditCatAdd.Name = "CheckEditCatAdd"
        Me.CheckEditCatAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCatAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditCatAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCatAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditCatAdd.Properties.Caption = "Add"
        Me.CheckEditCatAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCatAdd.Properties.ValueChecked = "Y"
        Me.CheckEditCatAdd.Properties.ValueGrayed = "N"
        Me.CheckEditCatAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditCatAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditCatAdd.TabIndex = 2
        '
        'CheckEditCat
        '
        Me.CheckEditCat.Location = New System.Drawing.Point(15, 195)
        Me.CheckEditCat.Name = "CheckEditCat"
        Me.CheckEditCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCat.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCat.Properties.Caption = "Category"
        Me.CheckEditCat.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCat.Properties.ValueChecked = "Y"
        Me.CheckEditCat.Properties.ValueGrayed = "N"
        Me.CheckEditCat.Properties.ValueUnchecked = "N"
        Me.CheckEditCat.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditCat.TabIndex = 11
        '
        'PanelControl5
        '
        Me.PanelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl5.Controls.Add(Me.CheckEditGrdDelete)
        Me.PanelControl5.Controls.Add(Me.CheckEditGrdEdit)
        Me.PanelControl5.Controls.Add(Me.CheckEditGrdAdd)
        Me.PanelControl5.Location = New System.Drawing.Point(118, 156)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl5.TabIndex = 10
        '
        'CheckEditGrdDelete
        '
        Me.CheckEditGrdDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditGrdDelete.Name = "CheckEditGrdDelete"
        Me.CheckEditGrdDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditGrdDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditGrdDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditGrdDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditGrdDelete.Properties.Caption = "Delete"
        Me.CheckEditGrdDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditGrdDelete.Properties.ValueChecked = "Y"
        Me.CheckEditGrdDelete.Properties.ValueGrayed = "N"
        Me.CheckEditGrdDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditGrdDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditGrdDelete.TabIndex = 3
        '
        'CheckEditGrdEdit
        '
        Me.CheckEditGrdEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditGrdEdit.Name = "CheckEditGrdEdit"
        Me.CheckEditGrdEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditGrdEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditGrdEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditGrdEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditGrdEdit.Properties.Caption = "Edit"
        Me.CheckEditGrdEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditGrdEdit.Properties.ValueChecked = "Y"
        Me.CheckEditGrdEdit.Properties.ValueGrayed = "N"
        Me.CheckEditGrdEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditGrdEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditGrdEdit.TabIndex = 1
        '
        'CheckEditGrdAdd
        '
        Me.CheckEditGrdAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditGrdAdd.Name = "CheckEditGrdAdd"
        Me.CheckEditGrdAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditGrdAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditGrdAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditGrdAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditGrdAdd.Properties.Caption = "Add"
        Me.CheckEditGrdAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditGrdAdd.Properties.ValueChecked = "Y"
        Me.CheckEditGrdAdd.Properties.ValueGrayed = "N"
        Me.CheckEditGrdAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditGrdAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditGrdAdd.TabIndex = 2
        '
        'CheckEditGrade
        '
        Me.CheckEditGrade.Location = New System.Drawing.Point(15, 161)
        Me.CheckEditGrade.Name = "CheckEditGrade"
        Me.CheckEditGrade.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditGrade.Properties.Appearance.Options.UseFont = True
        Me.CheckEditGrade.Properties.Caption = "Grade"
        Me.CheckEditGrade.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditGrade.Properties.ValueChecked = "Y"
        Me.CheckEditGrade.Properties.ValueGrayed = "N"
        Me.CheckEditGrade.Properties.ValueUnchecked = "N"
        Me.CheckEditGrade.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditGrade.TabIndex = 9
        '
        'PanelControl4
        '
        Me.PanelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl4.Controls.Add(Me.CheckEditSecDelete)
        Me.PanelControl4.Controls.Add(Me.CheckEditSecEdit)
        Me.PanelControl4.Controls.Add(Me.CheckEditSecAdd)
        Me.PanelControl4.Location = New System.Drawing.Point(118, 122)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl4.TabIndex = 8
        '
        'CheckEditSecDelete
        '
        Me.CheckEditSecDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditSecDelete.Name = "CheckEditSecDelete"
        Me.CheckEditSecDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSecDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditSecDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSecDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditSecDelete.Properties.Caption = "Delete"
        Me.CheckEditSecDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditSecDelete.Properties.ValueChecked = "Y"
        Me.CheckEditSecDelete.Properties.ValueGrayed = "N"
        Me.CheckEditSecDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditSecDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditSecDelete.TabIndex = 3
        '
        'CheckEditSecEdit
        '
        Me.CheckEditSecEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditSecEdit.Name = "CheckEditSecEdit"
        Me.CheckEditSecEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSecEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditSecEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSecEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditSecEdit.Properties.Caption = "Edit"
        Me.CheckEditSecEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditSecEdit.Properties.ValueChecked = "Y"
        Me.CheckEditSecEdit.Properties.ValueGrayed = "N"
        Me.CheckEditSecEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditSecEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditSecEdit.TabIndex = 1
        '
        'CheckEditSecAdd
        '
        Me.CheckEditSecAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditSecAdd.Name = "CheckEditSecAdd"
        Me.CheckEditSecAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSecAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditSecAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSecAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditSecAdd.Properties.Caption = "Add"
        Me.CheckEditSecAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditSecAdd.Properties.ValueChecked = "Y"
        Me.CheckEditSecAdd.Properties.ValueGrayed = "N"
        Me.CheckEditSecAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditSecAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditSecAdd.TabIndex = 2
        '
        'CheckEditSec
        '
        Me.CheckEditSec.Location = New System.Drawing.Point(15, 127)
        Me.CheckEditSec.Name = "CheckEditSec"
        Me.CheckEditSec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSec.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSec.Properties.Caption = "Emp Group"
        Me.CheckEditSec.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditSec.Properties.ValueChecked = "Y"
        Me.CheckEditSec.Properties.ValueGrayed = "N"
        Me.CheckEditSec.Properties.ValueUnchecked = "N"
        Me.CheckEditSec.Size = New System.Drawing.Size(88, 19)
        Me.CheckEditSec.TabIndex = 7
        '
        'PanelControl3
        '
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl3.Controls.Add(Me.CheckEditDeptDelete)
        Me.PanelControl3.Controls.Add(Me.CheckEditDeptEdit)
        Me.PanelControl3.Controls.Add(Me.CheckEditDeptAdd)
        Me.PanelControl3.Location = New System.Drawing.Point(118, 88)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl3.TabIndex = 6
        '
        'CheckEditDeptDelete
        '
        Me.CheckEditDeptDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditDeptDelete.Name = "CheckEditDeptDelete"
        Me.CheckEditDeptDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDeptDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditDeptDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDeptDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditDeptDelete.Properties.Caption = "Delete"
        Me.CheckEditDeptDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDeptDelete.Properties.ValueChecked = "Y"
        Me.CheckEditDeptDelete.Properties.ValueGrayed = "N"
        Me.CheckEditDeptDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditDeptDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditDeptDelete.TabIndex = 3
        '
        'CheckEditDeptEdit
        '
        Me.CheckEditDeptEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditDeptEdit.Name = "CheckEditDeptEdit"
        Me.CheckEditDeptEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDeptEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditDeptEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDeptEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditDeptEdit.Properties.Caption = "Edit"
        Me.CheckEditDeptEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDeptEdit.Properties.ValueChecked = "Y"
        Me.CheckEditDeptEdit.Properties.ValueGrayed = "N"
        Me.CheckEditDeptEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditDeptEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditDeptEdit.TabIndex = 1
        '
        'CheckEditDeptAdd
        '
        Me.CheckEditDeptAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditDeptAdd.Name = "CheckEditDeptAdd"
        Me.CheckEditDeptAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDeptAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditDeptAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDeptAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditDeptAdd.Properties.Caption = "Add"
        Me.CheckEditDeptAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDeptAdd.Properties.ValueChecked = "Y"
        Me.CheckEditDeptAdd.Properties.ValueGrayed = "N"
        Me.CheckEditDeptAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditDeptAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditDeptAdd.TabIndex = 2
        '
        'CheckEditDept
        '
        Me.CheckEditDept.Location = New System.Drawing.Point(15, 93)
        Me.CheckEditDept.Name = "CheckEditDept"
        Me.CheckEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDept.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDept.Properties.Caption = "Department"
        Me.CheckEditDept.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDept.Properties.ValueChecked = "Y"
        Me.CheckEditDept.Properties.ValueGrayed = "N"
        Me.CheckEditDept.Properties.ValueUnchecked = "N"
        Me.CheckEditDept.Size = New System.Drawing.Size(97, 19)
        Me.CheckEditDept.TabIndex = 5
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl2.Controls.Add(Me.CheckEditCompDelete)
        Me.PanelControl2.Controls.Add(Me.CheckEditCompEdit)
        Me.PanelControl2.Controls.Add(Me.CheckEditCompAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(118, 20)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(283, 28)
        Me.PanelControl2.TabIndex = 4
        '
        'CheckEditCompDelete
        '
        Me.CheckEditCompDelete.Location = New System.Drawing.Point(190, 5)
        Me.CheckEditCompDelete.Name = "CheckEditCompDelete"
        Me.CheckEditCompDelete.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCompDelete.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditCompDelete.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCompDelete.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditCompDelete.Properties.Caption = "Delete"
        Me.CheckEditCompDelete.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCompDelete.Properties.ValueChecked = "Y"
        Me.CheckEditCompDelete.Properties.ValueGrayed = "N"
        Me.CheckEditCompDelete.Properties.ValueUnchecked = "N"
        Me.CheckEditCompDelete.Size = New System.Drawing.Size(66, 19)
        Me.CheckEditCompDelete.TabIndex = 3
        '
        'CheckEditCompEdit
        '
        Me.CheckEditCompEdit.Location = New System.Drawing.Point(100, 5)
        Me.CheckEditCompEdit.Name = "CheckEditCompEdit"
        Me.CheckEditCompEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCompEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditCompEdit.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCompEdit.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditCompEdit.Properties.Caption = "Edit"
        Me.CheckEditCompEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCompEdit.Properties.ValueChecked = "Y"
        Me.CheckEditCompEdit.Properties.ValueGrayed = "N"
        Me.CheckEditCompEdit.Properties.ValueUnchecked = "N"
        Me.CheckEditCompEdit.Size = New System.Drawing.Size(55, 19)
        Me.CheckEditCompEdit.TabIndex = 1
        '
        'CheckEditCompAdd
        '
        Me.CheckEditCompAdd.Location = New System.Drawing.Point(18, 5)
        Me.CheckEditCompAdd.Name = "CheckEditCompAdd"
        Me.CheckEditCompAdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCompAdd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditCompAdd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCompAdd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditCompAdd.Properties.Caption = "Add"
        Me.CheckEditCompAdd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCompAdd.Properties.ValueChecked = "Y"
        Me.CheckEditCompAdd.Properties.ValueGrayed = "N"
        Me.CheckEditCompAdd.Properties.ValueUnchecked = "N"
        Me.CheckEditCompAdd.Size = New System.Drawing.Size(62, 19)
        Me.CheckEditCompAdd.TabIndex = 2
        '
        'CheckEditComp
        '
        Me.CheckEditComp.Location = New System.Drawing.Point(15, 25)
        Me.CheckEditComp.Name = "CheckEditComp"
        Me.CheckEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditComp.Properties.Appearance.Options.UseFont = True
        Me.CheckEditComp.Properties.Caption = "Company"
        Me.CheckEditComp.Properties.DisplayValueChecked = "Y"
        Me.CheckEditComp.Properties.DisplayValueGrayed = "N"
        Me.CheckEditComp.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditComp.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditComp.Properties.ValueChecked = "Y"
        Me.CheckEditComp.Properties.ValueGrayed = "N"
        Me.CheckEditComp.Properties.ValueUnchecked = "N"
        Me.CheckEditComp.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditComp.TabIndex = 0
        '
        'XtraTabTransaction
        '
        Me.XtraTabTransaction.Controls.Add(Me.CheckEditDataMaintenance)
        Me.XtraTabTransaction.Controls.Add(Me.CheckEditHoliday)
        Me.XtraTabTransaction.Controls.Add(Me.CheckEditShiftShange)
        Me.XtraTabTransaction.Controls.Add(Me.CheckEditOverStayToOT)
        Me.XtraTabTransaction.Controls.Add(Me.CheckEditPunchEntry)
        Me.XtraTabTransaction.Name = "XtraTabTransaction"
        Me.XtraTabTransaction.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabTransaction.Text = "Transaction"
        '
        'CheckEditDataMaintenance
        '
        Me.CheckEditDataMaintenance.Location = New System.Drawing.Point(336, 30)
        Me.CheckEditDataMaintenance.Name = "CheckEditDataMaintenance"
        Me.CheckEditDataMaintenance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDataMaintenance.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDataMaintenance.Properties.Caption = "Data Maintenance"
        Me.CheckEditDataMaintenance.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDataMaintenance.Properties.ValueChecked = "Y"
        Me.CheckEditDataMaintenance.Properties.ValueGrayed = "N"
        Me.CheckEditDataMaintenance.Properties.ValueUnchecked = "N"
        Me.CheckEditDataMaintenance.Size = New System.Drawing.Size(157, 19)
        Me.CheckEditDataMaintenance.TabIndex = 5
        '
        'CheckEditHoliday
        '
        Me.CheckEditHoliday.Location = New System.Drawing.Point(28, 156)
        Me.CheckEditHoliday.Name = "CheckEditHoliday"
        Me.CheckEditHoliday.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditHoliday.Properties.Appearance.Options.UseFont = True
        Me.CheckEditHoliday.Properties.Caption = "Holiday"
        Me.CheckEditHoliday.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditHoliday.Properties.ValueChecked = "Y"
        Me.CheckEditHoliday.Properties.ValueGrayed = "N"
        Me.CheckEditHoliday.Properties.ValueUnchecked = "N"
        Me.CheckEditHoliday.Size = New System.Drawing.Size(157, 19)
        Me.CheckEditHoliday.TabIndex = 4
        '
        'CheckEditShiftShange
        '
        Me.CheckEditShiftShange.Location = New System.Drawing.Point(28, 110)
        Me.CheckEditShiftShange.Name = "CheckEditShiftShange"
        Me.CheckEditShiftShange.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditShiftShange.Properties.Appearance.Options.UseFont = True
        Me.CheckEditShiftShange.Properties.Caption = "Shift Change"
        Me.CheckEditShiftShange.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditShiftShange.Properties.ValueChecked = "Y"
        Me.CheckEditShiftShange.Properties.ValueGrayed = "N"
        Me.CheckEditShiftShange.Properties.ValueUnchecked = "N"
        Me.CheckEditShiftShange.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditShiftShange.TabIndex = 3
        '
        'CheckEditOverStayToOT
        '
        Me.CheckEditOverStayToOT.Location = New System.Drawing.Point(28, 68)
        Me.CheckEditOverStayToOT.Name = "CheckEditOverStayToOT"
        Me.CheckEditOverStayToOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOverStayToOT.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOverStayToOT.Properties.Caption = "Over Stay to Over Time"
        Me.CheckEditOverStayToOT.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditOverStayToOT.Properties.ValueChecked = "Y"
        Me.CheckEditOverStayToOT.Properties.ValueGrayed = "N"
        Me.CheckEditOverStayToOT.Properties.ValueUnchecked = "N"
        Me.CheckEditOverStayToOT.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditOverStayToOT.TabIndex = 2
        '
        'CheckEditPunchEntry
        '
        Me.CheckEditPunchEntry.Location = New System.Drawing.Point(28, 30)
        Me.CheckEditPunchEntry.Name = "CheckEditPunchEntry"
        Me.CheckEditPunchEntry.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPunchEntry.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPunchEntry.Properties.Caption = "Manual Entry"
        Me.CheckEditPunchEntry.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditPunchEntry.Properties.ValueChecked = "Y"
        Me.CheckEditPunchEntry.Properties.ValueGrayed = "N"
        Me.CheckEditPunchEntry.Properties.ValueUnchecked = "N"
        Me.CheckEditPunchEntry.Size = New System.Drawing.Size(216, 19)
        Me.CheckEditPunchEntry.TabIndex = 1
        '
        'XtraTabDataProcess
        '
        Me.XtraTabDataProcess.Controls.Add(Me.CheckEditVerification)
        Me.XtraTabDataProcess.Controls.Add(Me.CheckEditBckDate)
        Me.XtraTabDataProcess.Controls.Add(Me.CheckEditAttResUpd)
        Me.XtraTabDataProcess.Controls.Add(Me.CheckEditAttResCre)
        Me.XtraTabDataProcess.Name = "XtraTabDataProcess"
        Me.XtraTabDataProcess.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabDataProcess.Text = "Data Process"
        '
        'CheckEditVerification
        '
        Me.CheckEditVerification.Location = New System.Drawing.Point(37, 181)
        Me.CheckEditVerification.Name = "CheckEditVerification"
        Me.CheckEditVerification.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditVerification.Properties.Appearance.Options.UseFont = True
        Me.CheckEditVerification.Properties.Caption = "Verification"
        Me.CheckEditVerification.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditVerification.Properties.ValueChecked = "Y"
        Me.CheckEditVerification.Properties.ValueGrayed = "N"
        Me.CheckEditVerification.Properties.ValueUnchecked = "N"
        Me.CheckEditVerification.Size = New System.Drawing.Size(159, 19)
        Me.CheckEditVerification.TabIndex = 4
        '
        'CheckEditBckDate
        '
        Me.CheckEditBckDate.Location = New System.Drawing.Point(37, 130)
        Me.CheckEditBckDate.Name = "CheckEditBckDate"
        Me.CheckEditBckDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBckDate.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBckDate.Properties.Caption = "Back Date Processing"
        Me.CheckEditBckDate.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBckDate.Properties.ValueChecked = "Y"
        Me.CheckEditBckDate.Properties.ValueGrayed = "N"
        Me.CheckEditBckDate.Properties.ValueUnchecked = "N"
        Me.CheckEditBckDate.Size = New System.Drawing.Size(182, 19)
        Me.CheckEditBckDate.TabIndex = 3
        '
        'CheckEditAttResUpd
        '
        Me.CheckEditAttResUpd.Location = New System.Drawing.Point(37, 78)
        Me.CheckEditAttResUpd.Name = "CheckEditAttResUpd"
        Me.CheckEditAttResUpd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAttResUpd.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAttResUpd.Properties.Caption = "Attendance Register Updation"
        Me.CheckEditAttResUpd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditAttResUpd.Properties.ValueChecked = "Y"
        Me.CheckEditAttResUpd.Properties.ValueGrayed = "N"
        Me.CheckEditAttResUpd.Properties.ValueUnchecked = "N"
        Me.CheckEditAttResUpd.Size = New System.Drawing.Size(193, 19)
        Me.CheckEditAttResUpd.TabIndex = 2
        '
        'CheckEditAttResCre
        '
        Me.CheckEditAttResCre.Location = New System.Drawing.Point(37, 28)
        Me.CheckEditAttResCre.Name = "CheckEditAttResCre"
        Me.CheckEditAttResCre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAttResCre.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAttResCre.Properties.Caption = "Attendance Register Creation"
        Me.CheckEditAttResCre.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditAttResCre.Properties.ValueChecked = "Y"
        Me.CheckEditAttResCre.Properties.ValueGrayed = "N"
        Me.CheckEditAttResCre.Properties.ValueUnchecked = "N"
        Me.CheckEditAttResCre.Size = New System.Drawing.Size(222, 19)
        Me.CheckEditAttResCre.TabIndex = 1
        '
        'XtraTabLeave
        '
        Me.XtraTabLeave.Controls.Add(Me.CheckEditMleaveInc)
        Me.XtraTabLeave.Controls.Add(Me.CheckEditLeaveApp)
        Me.XtraTabLeave.Controls.Add(Me.CheckEditLeaveAcc)
        Me.XtraTabLeave.Controls.Add(Me.CheckEditLeaveMaster)
        Me.XtraTabLeave.Name = "XtraTabLeave"
        Me.XtraTabLeave.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabLeave.Text = "Leave"
        '
        'CheckEditMleaveInc
        '
        Me.CheckEditMleaveInc.Location = New System.Drawing.Point(28, 179)
        Me.CheckEditMleaveInc.Name = "CheckEditMleaveInc"
        Me.CheckEditMleaveInc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditMleaveInc.Properties.Appearance.Options.UseFont = True
        Me.CheckEditMleaveInc.Properties.Caption = "Monthly Leave Icrement"
        Me.CheckEditMleaveInc.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditMleaveInc.Properties.ValueChecked = "Y"
        Me.CheckEditMleaveInc.Properties.ValueGrayed = "N"
        Me.CheckEditMleaveInc.Properties.ValueUnchecked = "N"
        Me.CheckEditMleaveInc.Size = New System.Drawing.Size(214, 19)
        Me.CheckEditMleaveInc.TabIndex = 4
        '
        'CheckEditLeaveApp
        '
        Me.CheckEditLeaveApp.Location = New System.Drawing.Point(28, 79)
        Me.CheckEditLeaveApp.Name = "CheckEditLeaveApp"
        Me.CheckEditLeaveApp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLeaveApp.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLeaveApp.Properties.Caption = "Leave Application"
        Me.CheckEditLeaveApp.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditLeaveApp.Properties.ValueChecked = "Y"
        Me.CheckEditLeaveApp.Properties.ValueGrayed = "N"
        Me.CheckEditLeaveApp.Properties.ValueUnchecked = "N"
        Me.CheckEditLeaveApp.Size = New System.Drawing.Size(138, 19)
        Me.CheckEditLeaveApp.TabIndex = 3
        '
        'CheckEditLeaveAcc
        '
        Me.CheckEditLeaveAcc.Location = New System.Drawing.Point(28, 127)
        Me.CheckEditLeaveAcc.Name = "CheckEditLeaveAcc"
        Me.CheckEditLeaveAcc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLeaveAcc.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLeaveAcc.Properties.Caption = "Leave Accrual"
        Me.CheckEditLeaveAcc.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditLeaveAcc.Properties.ValueChecked = "Y"
        Me.CheckEditLeaveAcc.Properties.ValueGrayed = "N"
        Me.CheckEditLeaveAcc.Properties.ValueUnchecked = "N"
        Me.CheckEditLeaveAcc.Size = New System.Drawing.Size(138, 19)
        Me.CheckEditLeaveAcc.TabIndex = 2
        '
        'CheckEditLeaveMaster
        '
        Me.CheckEditLeaveMaster.Location = New System.Drawing.Point(28, 34)
        Me.CheckEditLeaveMaster.Name = "CheckEditLeaveMaster"
        Me.CheckEditLeaveMaster.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLeaveMaster.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLeaveMaster.Properties.Caption = "Leave Master"
        Me.CheckEditLeaveMaster.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditLeaveMaster.Properties.ValueChecked = "Y"
        Me.CheckEditLeaveMaster.Properties.ValueGrayed = "N"
        Me.CheckEditLeaveMaster.Properties.ValueUnchecked = "N"
        Me.CheckEditLeaveMaster.Size = New System.Drawing.Size(204, 19)
        Me.CheckEditLeaveMaster.TabIndex = 1
        '
        'XtraTabAdmin
        '
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditParallel)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditBackup)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditEmail)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditBulkSMS)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditSMS)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditDBSetting)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditUserPre)
        Me.XtraTabAdmin.Controls.Add(Me.CheckEditCommonSetting)
        Me.XtraTabAdmin.Name = "XtraTabAdmin"
        Me.XtraTabAdmin.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabAdmin.Text = "Admin"
        '
        'CheckEditParallel
        '
        Me.CheckEditParallel.Location = New System.Drawing.Point(292, 170)
        Me.CheckEditParallel.Name = "CheckEditParallel"
        Me.CheckEditParallel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditParallel.Properties.Appearance.Options.UseFont = True
        Me.CheckEditParallel.Properties.Caption = "Parallel Data"
        Me.CheckEditParallel.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditParallel.Properties.ValueChecked = "Y"
        Me.CheckEditParallel.Properties.ValueGrayed = "N"
        Me.CheckEditParallel.Properties.ValueUnchecked = "N"
        Me.CheckEditParallel.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditParallel.TabIndex = 8
        '
        'CheckEditBackup
        '
        Me.CheckEditBackup.Location = New System.Drawing.Point(292, 120)
        Me.CheckEditBackup.Name = "CheckEditBackup"
        Me.CheckEditBackup.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBackup.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBackup.Properties.Caption = "Backup Setup"
        Me.CheckEditBackup.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBackup.Properties.ValueChecked = "Y"
        Me.CheckEditBackup.Properties.ValueGrayed = "N"
        Me.CheckEditBackup.Properties.ValueUnchecked = "N"
        Me.CheckEditBackup.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditBackup.TabIndex = 7
        '
        'CheckEditEmail
        '
        Me.CheckEditEmail.Location = New System.Drawing.Point(292, 28)
        Me.CheckEditEmail.Name = "CheckEditEmail"
        Me.CheckEditEmail.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditEmail.Properties.Appearance.Options.UseFont = True
        Me.CheckEditEmail.Properties.Caption = "Email Setting"
        Me.CheckEditEmail.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditEmail.Properties.ValueChecked = "Y"
        Me.CheckEditEmail.Properties.ValueGrayed = "N"
        Me.CheckEditEmail.Properties.ValueUnchecked = "N"
        Me.CheckEditEmail.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditEmail.TabIndex = 5
        '
        'CheckEditBulkSMS
        '
        Me.CheckEditBulkSMS.Location = New System.Drawing.Point(28, 170)
        Me.CheckEditBulkSMS.Name = "CheckEditBulkSMS"
        Me.CheckEditBulkSMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBulkSMS.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBulkSMS.Properties.Caption = "Bulk SMS"
        Me.CheckEditBulkSMS.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBulkSMS.Properties.ValueChecked = "Y"
        Me.CheckEditBulkSMS.Properties.ValueGrayed = "N"
        Me.CheckEditBulkSMS.Properties.ValueUnchecked = "N"
        Me.CheckEditBulkSMS.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditBulkSMS.TabIndex = 4
        '
        'CheckEditSMS
        '
        Me.CheckEditSMS.Location = New System.Drawing.Point(28, 120)
        Me.CheckEditSMS.Name = "CheckEditSMS"
        Me.CheckEditSMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSMS.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSMS.Properties.Caption = "SMS Setting"
        Me.CheckEditSMS.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditSMS.Properties.ValueChecked = "Y"
        Me.CheckEditSMS.Properties.ValueGrayed = "N"
        Me.CheckEditSMS.Properties.ValueUnchecked = "N"
        Me.CheckEditSMS.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditSMS.TabIndex = 3
        '
        'CheckEditDBSetting
        '
        Me.CheckEditDBSetting.Location = New System.Drawing.Point(28, 73)
        Me.CheckEditDBSetting.Name = "CheckEditDBSetting"
        Me.CheckEditDBSetting.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDBSetting.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDBSetting.Properties.Caption = "Database Setting"
        Me.CheckEditDBSetting.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDBSetting.Properties.ValueChecked = "Y"
        Me.CheckEditDBSetting.Properties.ValueGrayed = "N"
        Me.CheckEditDBSetting.Properties.ValueUnchecked = "N"
        Me.CheckEditDBSetting.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditDBSetting.TabIndex = 2
        '
        'CheckEditUserPre
        '
        Me.CheckEditUserPre.Location = New System.Drawing.Point(292, 73)
        Me.CheckEditUserPre.Name = "CheckEditUserPre"
        Me.CheckEditUserPre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditUserPre.Properties.Appearance.Options.UseFont = True
        Me.CheckEditUserPre.Properties.Caption = "User Previlige"
        Me.CheckEditUserPre.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditUserPre.Properties.ValueChecked = "Y"
        Me.CheckEditUserPre.Properties.ValueGrayed = "N"
        Me.CheckEditUserPre.Properties.ValueUnchecked = "N"
        Me.CheckEditUserPre.Size = New System.Drawing.Size(130, 19)
        Me.CheckEditUserPre.TabIndex = 6
        '
        'CheckEditCommonSetting
        '
        Me.CheckEditCommonSetting.Location = New System.Drawing.Point(28, 28)
        Me.CheckEditCommonSetting.Name = "CheckEditCommonSetting"
        Me.CheckEditCommonSetting.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCommonSetting.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCommonSetting.Properties.Caption = "Common Setting"
        Me.CheckEditCommonSetting.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditCommonSetting.Properties.ValueChecked = "Y"
        Me.CheckEditCommonSetting.Properties.ValueGrayed = "N"
        Me.CheckEditCommonSetting.Properties.ValueUnchecked = "N"
        Me.CheckEditCommonSetting.Size = New System.Drawing.Size(175, 19)
        Me.CheckEditCommonSetting.TabIndex = 1
        '
        'XtraTabReports
        '
        Me.XtraTabReports.Controls.Add(Me.CheckEditLeaveR)
        Me.XtraTabReports.Controls.Add(Me.CheckEditDailyR)
        Me.XtraTabReports.Controls.Add(Me.CheckEditMonthlyR)
        Me.XtraTabReports.Controls.Add(Me.CheckEditPayReport)
        Me.XtraTabReports.Controls.Add(Me.CheckEditTimeReport)
        Me.XtraTabReports.Name = "XtraTabReports"
        Me.XtraTabReports.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabReports.Text = "Reports"
        '
        'CheckEditLeaveR
        '
        Me.CheckEditLeaveR.Location = New System.Drawing.Point(28, 144)
        Me.CheckEditLeaveR.Name = "CheckEditLeaveR"
        Me.CheckEditLeaveR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLeaveR.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLeaveR.Properties.Caption = "Leave Reports"
        Me.CheckEditLeaveR.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditLeaveR.Properties.ValueChecked = "Y"
        Me.CheckEditLeaveR.Properties.ValueGrayed = "N"
        Me.CheckEditLeaveR.Properties.ValueUnchecked = "N"
        Me.CheckEditLeaveR.Size = New System.Drawing.Size(145, 19)
        Me.CheckEditLeaveR.TabIndex = 5
        '
        'CheckEditDailyR
        '
        Me.CheckEditDailyR.Location = New System.Drawing.Point(28, 88)
        Me.CheckEditDailyR.Name = "CheckEditDailyR"
        Me.CheckEditDailyR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDailyR.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDailyR.Properties.Caption = "Daily Reports"
        Me.CheckEditDailyR.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditDailyR.Properties.ValueChecked = "Y"
        Me.CheckEditDailyR.Properties.ValueGrayed = "N"
        Me.CheckEditDailyR.Properties.ValueUnchecked = "N"
        Me.CheckEditDailyR.Size = New System.Drawing.Size(145, 19)
        Me.CheckEditDailyR.TabIndex = 4
        '
        'CheckEditMonthlyR
        '
        Me.CheckEditMonthlyR.Location = New System.Drawing.Point(28, 32)
        Me.CheckEditMonthlyR.Name = "CheckEditMonthlyR"
        Me.CheckEditMonthlyR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditMonthlyR.Properties.Appearance.Options.UseFont = True
        Me.CheckEditMonthlyR.Properties.Caption = "Monthly Reports"
        Me.CheckEditMonthlyR.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditMonthlyR.Properties.ValueChecked = "Y"
        Me.CheckEditMonthlyR.Properties.ValueGrayed = "N"
        Me.CheckEditMonthlyR.Properties.ValueUnchecked = "N"
        Me.CheckEditMonthlyR.Size = New System.Drawing.Size(145, 19)
        Me.CheckEditMonthlyR.TabIndex = 3
        '
        'CheckEditPayReport
        '
        Me.CheckEditPayReport.Location = New System.Drawing.Point(433, 32)
        Me.CheckEditPayReport.Name = "CheckEditPayReport"
        Me.CheckEditPayReport.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPayReport.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPayReport.Properties.Caption = "Payroll Reports"
        Me.CheckEditPayReport.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditPayReport.Properties.ValueChecked = "Y"
        Me.CheckEditPayReport.Properties.ValueGrayed = "N"
        Me.CheckEditPayReport.Properties.ValueUnchecked = "N"
        Me.CheckEditPayReport.Size = New System.Drawing.Size(165, 19)
        Me.CheckEditPayReport.TabIndex = 2
        Me.CheckEditPayReport.Visible = False
        '
        'CheckEditTimeReport
        '
        Me.CheckEditTimeReport.Location = New System.Drawing.Point(433, 57)
        Me.CheckEditTimeReport.Name = "CheckEditTimeReport"
        Me.CheckEditTimeReport.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditTimeReport.Properties.Appearance.Options.UseFont = True
        Me.CheckEditTimeReport.Properties.Caption = "Time Office Reports"
        Me.CheckEditTimeReport.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditTimeReport.Properties.ValueChecked = "Y"
        Me.CheckEditTimeReport.Properties.ValueGrayed = "N"
        Me.CheckEditTimeReport.Properties.ValueUnchecked = "N"
        Me.CheckEditTimeReport.Size = New System.Drawing.Size(216, 19)
        Me.CheckEditTimeReport.TabIndex = 1
        Me.CheckEditTimeReport.Visible = False
        '
        'XtraTabPayroll
        '
        Me.XtraTabPayroll.Name = "XtraTabPayroll"
        Me.XtraTabPayroll.PageVisible = False
        Me.XtraTabPayroll.Size = New System.Drawing.Size(802, 298)
        Me.XtraTabPayroll.Text = "Payroll"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(741, 471)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 104
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(660, 471)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 103
        Me.SimpleButton1.Text = "Save"
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(357, 175)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(41, 14)
        Me.LabelControl19.TabIndex = 27
        Me.LabelControl19.Text = "Process"
        '
        'ToggleCompl
        '
        Me.ToggleCompl.Location = New System.Drawing.Point(494, 170)
        Me.ToggleCompl.Name = "ToggleCompl"
        Me.ToggleCompl.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleCompl.Properties.Appearance.Options.UseFont = True
        Me.ToggleCompl.Properties.OffText = "No"
        Me.ToggleCompl.Properties.OnText = "Yes"
        Me.ToggleCompl.Size = New System.Drawing.Size(95, 25)
        Me.ToggleCompl.TabIndex = 26
        '
        'XtraUserMgmtEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 499)
        Me.ControlBox = False
        Me.Controls.Add(Me.PopupContainerControlComp)
        Me.Controls.Add(Me.PopupContainerControlBranch)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraUserMgmtEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Management"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPwd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUsr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPrevileges.ResumeLayout(False)
        Me.XtraTabPrevileges.PerformLayout()
        CType(Me.ToggleSwitchDevice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchReports.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchAutoProcess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchAdmin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchLeaveMgmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchDataPtrocess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchTransaction.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchMain.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabDevice.ResumeLayout(False)
        Me.XtraTabDevice.PerformLayout()
        CType(Me.CheckEditUserSetup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLogMgmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl10.ResumeLayout(False)
        CType(Me.CheckEditDDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabMaster.ResumeLayout(False)
        CType(Me.PanelControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl9.ResumeLayout(False)
        CType(Me.CheckEditBranchDel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBranchEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBranchAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBranch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditMarlAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl8.ResumeLayout(False)
        CType(Me.CheckEditEmpDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditEmpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditEmpAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        CType(Me.CheckEditShiftDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditShiftEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditShiftAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl6.ResumeLayout(False)
        CType(Me.CheckEditCatDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCatEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCatAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        CType(Me.CheckEditGrdDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditGrdEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditGrdAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.CheckEditSecDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditSecEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditSecAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditSec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.CheckEditDeptDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDeptEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDeptAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.CheckEditCompDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCompEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCompAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabTransaction.ResumeLayout(False)
        CType(Me.CheckEditDataMaintenance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditHoliday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditShiftShange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOverStayToOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPunchEntry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabDataProcess.ResumeLayout(False)
        CType(Me.CheckEditVerification.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBckDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAttResUpd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAttResCre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabLeave.ResumeLayout(False)
        CType(Me.CheckEditMleaveInc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLeaveApp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLeaveAcc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLeaveMaster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabAdmin.ResumeLayout(False)
        CType(Me.CheckEditParallel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBackup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBulkSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDBSetting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditUserPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCommonSetting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabReports.ResumeLayout(False)
        CType(Me.CheckEditLeaveR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDailyR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditMonthlyR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPayReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditTimeReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleCompl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditUsr As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditPwd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPrevileges As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabMaster As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabTransaction As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabLeave As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabAdmin As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPayroll As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabReports As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabDataProcess As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ToggleSwitchMain As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchDataPtrocess As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchTransaction As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchLeaveMgmt As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchPay As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchAdmin As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchAutoProcess As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchReports As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEditCompDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCompAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCompEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditComp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditDeptDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDeptEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDeptAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDept As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditSecDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSecEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSecAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSec As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditGrdDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditGrdEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditGrdAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditGrade As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl6 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditCatDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCatEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCatAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCat As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl8 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditEmpDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditEmpEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditEmpAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditEmp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditShiftDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditShiftEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditShiftAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditShift As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPunchEntry As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOverStayToOT As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditHoliday As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditShiftShange As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLeaveApp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLeaveAcc As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLeaveMaster As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditUserPre As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCommonSetting As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPayReport As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditTimeReport As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBckDate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAttResUpd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAttResCre As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchVisitor As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchCanteen As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents CheckEditDataMaintenance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMarlAll As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditVerification As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl9 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditBranchDel As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBranchEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBranchAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBranch As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLeaveR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDailyR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMonthlyR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBackup As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditEmail As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBulkSMS As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSMS As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDBSetting As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditParallel As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents XtraTabDevice As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl10 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditDDelete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDAdd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditUserSetup As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLogMgmt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchDevice As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditMleaveInc As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleCompl As DevExpress.XtraEditors.ToggleSwitch
End Class
