﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLoanAdvance
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraLoanAdvance))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ComboNepaliYearT = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthT = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliYearL = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthL = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditInstAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditNoInst = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditIntRate = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControlTotal = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlTranFrmMon = New DevExpress.XtraEditors.LabelControl()
        Me.DateEditT = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControlLonMonth = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditLoan = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAdvance = New DevExpress.XtraEditors.CheckEdit()
        Me.DateEditL = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.lblGrade = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lblEmpGrp = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.lblCat = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.lblComp = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.lblDesi = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.lblVoucher = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_L = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMON_YEAR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEntry_Date = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTran_Month = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADV_AMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINST_AMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINSTNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINTREST_RATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TBLADVANCEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TBLADVANCETableAdapter = New iAS.SSSDBDataSetTableAdapters.TBLADVANCETableAdapter()
        Me.TbladvancE1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TBLADVANCE1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ComboNepaliYearT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditInstAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditNoInst.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditIntRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditT.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLoan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAdvance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditL.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TBLADVANCEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.LookAndFeel.SkinName = "iMaginary"
        Me.SplitContainerControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 5
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl2.Controls.Add(Me.ComboNepaliYearT)
        Me.PanelControl2.Controls.Add(Me.ComboNEpaliMonthT)
        Me.PanelControl2.Controls.Add(Me.ComboNepaliYearL)
        Me.PanelControl2.Controls.Add(Me.ComboNEpaliMonthL)
        Me.PanelControl2.Controls.Add(Me.LabelControl14)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonSave)
        Me.PanelControl2.Controls.Add(Me.TextEditInstAmt)
        Me.PanelControl2.Controls.Add(Me.LabelControl13)
        Me.PanelControl2.Controls.Add(Me.TextEditNoInst)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.TextEditIntRate)
        Me.PanelControl2.Controls.Add(Me.TextEditTotal)
        Me.PanelControl2.Controls.Add(Me.LabelControlTotal)
        Me.PanelControl2.Controls.Add(Me.LabelControlTranFrmMon)
        Me.PanelControl2.Controls.Add(Me.DateEditT)
        Me.PanelControl2.Controls.Add(Me.LabelControlLonMonth)
        Me.PanelControl2.Controls.Add(Me.CheckEditLoan)
        Me.PanelControl2.Controls.Add(Me.CheckEditAdvance)
        Me.PanelControl2.Controls.Add(Me.DateEditL)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(350, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(686, 328)
        Me.PanelControl2.TabIndex = 34
        '
        'ComboNepaliYearT
        '
        Me.ComboNepaliYearT.Location = New System.Drawing.Point(227, 83)
        Me.ComboNepaliYearT.Name = "ComboNepaliYearT"
        Me.ComboNepaliYearT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearT.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearT.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearT.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearT.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearT.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearT.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearT.TabIndex = 5
        '
        'ComboNEpaliMonthT
        '
        Me.ComboNEpaliMonthT.Location = New System.Drawing.Point(155, 83)
        Me.ComboNEpaliMonthT.Name = "ComboNEpaliMonthT"
        Me.ComboNEpaliMonthT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthT.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthT.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthT.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthT.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthT.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthT.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthT.TabIndex = 4
        '
        'ComboNepaliYearL
        '
        Me.ComboNepaliYearL.Location = New System.Drawing.Point(227, 57)
        Me.ComboNepaliYearL.Name = "ComboNepaliYearL"
        Me.ComboNepaliYearL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearL.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearL.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearL.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearL.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearL.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearL.TabIndex = 3
        '
        'ComboNEpaliMonthL
        '
        Me.ComboNEpaliMonthL.Location = New System.Drawing.Point(155, 57)
        Me.ComboNEpaliMonthL.Name = "ComboNEpaliMonthL"
        Me.ComboNEpaliMonthL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthL.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthL.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthL.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthL.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthL.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthL.TabIndex = 2
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(474, 127)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(12, 14)
        Me.LabelControl14.TabIndex = 22
        Me.LabelControl14.Text = "%"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(17, 273)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 10
        Me.SimpleButtonSave.Text = "Save"
        '
        'TextEditInstAmt
        '
        Me.TextEditInstAmt.Location = New System.Drawing.Point(155, 177)
        Me.TextEditInstAmt.Name = "TextEditInstAmt"
        Me.TextEditInstAmt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditInstAmt.Properties.Appearance.Options.UseFont = True
        Me.TextEditInstAmt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditInstAmt.Properties.ReadOnly = True
        Me.TextEditInstAmt.Size = New System.Drawing.Size(100, 20)
        Me.TextEditInstAmt.TabIndex = 9
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(13, 180)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(108, 14)
        Me.LabelControl13.TabIndex = 20
        Me.LabelControl13.Text = "Installment Amount"
        '
        'TextEditNoInst
        '
        Me.TextEditNoInst.Location = New System.Drawing.Point(155, 151)
        Me.TextEditNoInst.Name = "TextEditNoInst"
        Me.TextEditNoInst.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditNoInst.Properties.Appearance.Options.UseFont = True
        Me.TextEditNoInst.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditNoInst.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditNoInst.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditNoInst.Properties.MaxLength = 3
        Me.TextEditNoInst.Size = New System.Drawing.Size(100, 20)
        Me.TextEditNoInst.TabIndex = 8
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 153)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(124, 14)
        Me.LabelControl3.TabIndex = 18
        Me.LabelControl3.Text = "Number Of Installment"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(308, 128)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl2.TabIndex = 16
        Me.LabelControl2.Text = "Intrest Rate"
        '
        'TextEditIntRate
        '
        Me.TextEditIntRate.Location = New System.Drawing.Point(385, 125)
        Me.TextEditIntRate.Name = "TextEditIntRate"
        Me.TextEditIntRate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditIntRate.Properties.Appearance.Options.UseFont = True
        Me.TextEditIntRate.Properties.Mask.EditMask = "###.##"
        Me.TextEditIntRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditIntRate.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditIntRate.Properties.MaxLength = 5
        Me.TextEditIntRate.Size = New System.Drawing.Size(83, 20)
        Me.TextEditIntRate.TabIndex = 7
        '
        'TextEditTotal
        '
        Me.TextEditTotal.Location = New System.Drawing.Point(155, 125)
        Me.TextEditTotal.Name = "TextEditTotal"
        Me.TextEditTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTotal.Properties.Appearance.Options.UseFont = True
        Me.TextEditTotal.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTotal.Properties.MaxLength = 10
        Me.TextEditTotal.Size = New System.Drawing.Size(100, 20)
        Me.TextEditTotal.TabIndex = 6
        '
        'LabelControlTotal
        '
        Me.LabelControlTotal.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlTotal.Appearance.Options.UseFont = True
        Me.LabelControlTotal.Location = New System.Drawing.Point(12, 127)
        Me.LabelControlTotal.Name = "LabelControlTotal"
        Me.LabelControlTotal.Size = New System.Drawing.Size(79, 14)
        Me.LabelControlTotal.TabIndex = 13
        Me.LabelControlTotal.Text = "Total Advance"
        '
        'LabelControlTranFrmMon
        '
        Me.LabelControlTranFrmMon.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlTranFrmMon.Appearance.Options.UseFont = True
        Me.LabelControlTranFrmMon.Location = New System.Drawing.Point(10, 86)
        Me.LabelControlTranFrmMon.Name = "LabelControlTranFrmMon"
        Me.LabelControlTranFrmMon.Size = New System.Drawing.Size(133, 14)
        Me.LabelControlTranFrmMon.TabIndex = 12
        Me.LabelControlTranFrmMon.Text = "Transaction From Month"
        '
        'DateEditT
        '
        Me.DateEditT.EditValue = Nothing
        Me.DateEditT.Location = New System.Drawing.Point(155, 83)
        Me.DateEditT.Name = "DateEditT"
        Me.DateEditT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditT.Properties.Appearance.Options.UseFont = True
        Me.DateEditT.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditT.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditT.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditT.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditT.Size = New System.Drawing.Size(83, 20)
        Me.DateEditT.TabIndex = 4
        '
        'LabelControlLonMonth
        '
        Me.LabelControlLonMonth.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlLonMonth.Appearance.Options.UseFont = True
        Me.LabelControlLonMonth.Location = New System.Drawing.Point(13, 60)
        Me.LabelControlLonMonth.Name = "LabelControlLonMonth"
        Me.LabelControlLonMonth.Size = New System.Drawing.Size(65, 14)
        Me.LabelControlLonMonth.TabIndex = 10
        Me.LabelControlLonMonth.Text = "Loan Month"
        '
        'CheckEditLoan
        '
        Me.CheckEditLoan.Location = New System.Drawing.Point(148, 17)
        Me.CheckEditLoan.Name = "CheckEditLoan"
        Me.CheckEditLoan.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLoan.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLoan.Properties.Caption = "Loan"
        Me.CheckEditLoan.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditLoan.Properties.RadioGroupIndex = 0
        Me.CheckEditLoan.Size = New System.Drawing.Size(77, 19)
        Me.CheckEditLoan.TabIndex = 1
        Me.CheckEditLoan.TabStop = False
        '
        'CheckEditAdvance
        '
        Me.CheckEditAdvance.EditValue = True
        Me.CheckEditAdvance.Location = New System.Drawing.Point(26, 17)
        Me.CheckEditAdvance.Name = "CheckEditAdvance"
        Me.CheckEditAdvance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAdvance.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAdvance.Properties.Caption = "Advance"
        Me.CheckEditAdvance.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditAdvance.Properties.RadioGroupIndex = 0
        Me.CheckEditAdvance.Size = New System.Drawing.Size(77, 19)
        Me.CheckEditAdvance.TabIndex = 0
        '
        'DateEditL
        '
        Me.DateEditL.EditValue = Nothing
        Me.DateEditL.Location = New System.Drawing.Point(155, 57)
        Me.DateEditL.Name = "DateEditL"
        Me.DateEditL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditL.Properties.Appearance.Options.UseFont = True
        Me.DateEditL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditL.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditL.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditL.Size = New System.Drawing.Size(83, 20)
        Me.DateEditL.TabIndex = 2
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.LookUpEdit1)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.lblGrade)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.lblEmpGrp)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.lblCat)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.lblDept)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.lblComp)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.lblDesi)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.lblCardNum)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.lblName)
        Me.PanelControl1.Controls.Add(Me.lblVoucher)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(350, 328)
        Me.PanelControl1.TabIndex = 33
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(109, 14)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(22, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Paycode"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(22, 60)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl4.TabIndex = 9
        Me.LabelControl4.Text = "Id No."
        Me.LabelControl4.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(22, 85)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Name"
        '
        'lblGrade
        '
        Me.lblGrade.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblGrade.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblGrade.Appearance.Options.UseFont = True
        Me.lblGrade.Appearance.Options.UseForeColor = True
        Me.lblGrade.Location = New System.Drawing.Point(134, 282)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(20, 14)
        Me.lblGrade.TabIndex = 26
        Me.lblGrade.Text = "     "
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(22, 111)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl6.TabIndex = 11
        Me.LabelControl6.Text = "Card No."
        '
        'lblEmpGrp
        '
        Me.lblEmpGrp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblEmpGrp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEmpGrp.Appearance.Options.UseFont = True
        Me.lblEmpGrp.Appearance.Options.UseForeColor = True
        Me.lblEmpGrp.Location = New System.Drawing.Point(134, 253)
        Me.lblEmpGrp.Name = "lblEmpGrp"
        Me.lblEmpGrp.Size = New System.Drawing.Size(20, 14)
        Me.lblEmpGrp.TabIndex = 25
        Me.lblEmpGrp.Text = "     "
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(22, 138)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Designation"
        '
        'lblCat
        '
        Me.lblCat.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCat.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCat.Appearance.Options.UseFont = True
        Me.lblCat.Appearance.Options.UseForeColor = True
        Me.lblCat.Location = New System.Drawing.Point(134, 224)
        Me.lblCat.Name = "lblCat"
        Me.lblCat.Size = New System.Drawing.Size(20, 14)
        Me.lblCat.TabIndex = 24
        Me.lblCat.Text = "     "
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(22, 167)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl8.TabIndex = 13
        Me.LabelControl8.Text = "Company"
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(134, 195)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 23
        Me.lblDept.Text = "      "
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(22, 195)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 14
        Me.LabelControl9.Text = "Department"
        '
        'lblComp
        '
        Me.lblComp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblComp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblComp.Appearance.Options.UseFont = True
        Me.lblComp.Appearance.Options.UseForeColor = True
        Me.lblComp.Location = New System.Drawing.Point(134, 167)
        Me.lblComp.Name = "lblComp"
        Me.lblComp.Size = New System.Drawing.Size(20, 14)
        Me.lblComp.TabIndex = 22
        Me.lblComp.Text = "     "
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(22, 224)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl10.TabIndex = 15
        Me.LabelControl10.Text = "Catagory"
        '
        'lblDesi
        '
        Me.lblDesi.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDesi.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDesi.Appearance.Options.UseFont = True
        Me.lblDesi.Appearance.Options.UseForeColor = True
        Me.lblDesi.Location = New System.Drawing.Point(134, 138)
        Me.lblDesi.Name = "lblDesi"
        Me.lblDesi.Size = New System.Drawing.Size(20, 14)
        Me.lblDesi.TabIndex = 21
        Me.lblDesi.Text = "     "
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(22, 253)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl11.TabIndex = 16
        Me.LabelControl11.Text = "Employee Group"
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(134, 111)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 20
        Me.lblCardNum.Text = "      "
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(22, 282)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl12.TabIndex = 17
        Me.LabelControl12.Text = "Grade"
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(134, 85)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 19
        Me.lblName.Text = "     "
        '
        'lblVoucher
        '
        Me.lblVoucher.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblVoucher.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblVoucher.Appearance.Options.UseFont = True
        Me.lblVoucher.Appearance.Options.UseForeColor = True
        Me.lblVoucher.Location = New System.Drawing.Point(134, 60)
        Me.lblVoucher.Name = "lblVoucher"
        Me.lblVoucher.Size = New System.Drawing.Size(20, 14)
        Me.lblVoucher.TabIndex = 18
        Me.lblVoucher.Text = "     "
        Me.lblVoucher.Visible = False
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.GridControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 328)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 240)
        Me.SidePanel1.TabIndex = 32
        Me.SidePanel1.Text = "SidePanel1"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 1)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1036, 239)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colIDNO, Me.colA_L, Me.colMON_YEAR, Me.colEntry_Date, Me.colTran_Month, Me.colADV_AMT, Me.colINST_AMT, Me.colINSTNO, Me.colINTREST_RATE})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        '
        'colPAYCODE
        '
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        '
        'colIDNO
        '
        Me.colIDNO.Caption = "Id"
        Me.colIDNO.FieldName = "IDNO"
        Me.colIDNO.Name = "colIDNO"
        Me.colIDNO.Visible = True
        Me.colIDNO.VisibleIndex = 0
        '
        'colA_L
        '
        Me.colA_L.Caption = "Type"
        Me.colA_L.FieldName = "A_L"
        Me.colA_L.Name = "colA_L"
        Me.colA_L.Visible = True
        Me.colA_L.VisibleIndex = 1
        '
        'colMON_YEAR
        '
        Me.colMON_YEAR.Caption = "Date"
        Me.colMON_YEAR.FieldName = "MON_YEAR"
        Me.colMON_YEAR.Name = "colMON_YEAR"
        Me.colMON_YEAR.Visible = True
        Me.colMON_YEAR.VisibleIndex = 2
        '
        'colEntry_Date
        '
        Me.colEntry_Date.Caption = "Entry Date"
        Me.colEntry_Date.FieldName = "Entry_Date"
        Me.colEntry_Date.Name = "colEntry_Date"
        Me.colEntry_Date.Visible = True
        Me.colEntry_Date.VisibleIndex = 3
        '
        'colTran_Month
        '
        Me.colTran_Month.Caption = "Transaction Date"
        Me.colTran_Month.FieldName = "Tran_Month"
        Me.colTran_Month.Name = "colTran_Month"
        Me.colTran_Month.Visible = True
        Me.colTran_Month.VisibleIndex = 4
        '
        'colADV_AMT
        '
        Me.colADV_AMT.Caption = "Advance Amt"
        Me.colADV_AMT.FieldName = "ADV_AMT"
        Me.colADV_AMT.Name = "colADV_AMT"
        Me.colADV_AMT.Visible = True
        Me.colADV_AMT.VisibleIndex = 5
        '
        'colINST_AMT
        '
        Me.colINST_AMT.Caption = "Installment Amt"
        Me.colINST_AMT.FieldName = "INST_AMT"
        Me.colINST_AMT.Name = "colINST_AMT"
        Me.colINST_AMT.Visible = True
        Me.colINST_AMT.VisibleIndex = 6
        '
        'colINSTNO
        '
        Me.colINSTNO.Caption = "No Of Installments"
        Me.colINSTNO.FieldName = "INSTNO"
        Me.colINSTNO.Name = "colINSTNO"
        Me.colINSTNO.Visible = True
        Me.colINSTNO.VisibleIndex = 7
        '
        'colINTREST_RATE
        '
        Me.colINTREST_RATE.Caption = "Interest Rate"
        Me.colINTREST_RATE.FieldName = "INTREST_RATE"
        Me.colINTREST_RATE.Name = "colINTREST_RATE"
        Me.colINTREST_RATE.Visible = True
        Me.colINTREST_RATE.VisibleIndex = 8
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        Me.MemoEdit1.TabStop = False
        '
        'TBLADVANCEBindingSource
        '
        Me.TBLADVANCEBindingSource.DataMember = "TBLADVANCE"
        Me.TBLADVANCEBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TBLADVANCETableAdapter
        '
        Me.TBLADVANCETableAdapter.ClearBeforeFill = True
        '
        'TbladvancE1TableAdapter1
        '
        Me.TbladvancE1TableAdapter1.ClearBeforeFill = True
        '
        'XtraLoanAdvance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraLoanAdvance"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ComboNepaliYearT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditInstAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditNoInst.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditIntRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditT.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLoan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAdvance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditL.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TBLADVANCEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lblGrade As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEmpGrp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCat As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblComp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDesi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblVoucher As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditL As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditLoan As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAdvance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControlLonMonth As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlTranFrmMon As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditT As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControlTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditIntRate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditNoInst As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditInstAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TBLADVANCEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_L As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMON_YEAR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEntry_Date As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTran_Month As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADV_AMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINST_AMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINSTNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINTREST_RATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TBLADVANCETableAdapter As iAS.SSSDBDataSetTableAdapters.TBLADVANCETableAdapter
    Friend WithEvents TbladvancE1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TBLADVANCE1TableAdapter
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboNepaliYearT As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthT As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearL As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthL As DevExpress.XtraEditors.ComboBoxEdit

End Class
