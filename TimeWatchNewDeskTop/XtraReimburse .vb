﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Text

Public Class XtraReimburse
    Dim ulf As UserLookAndFeel
    Public Shared EmpId As String
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    'Public Shared EmpQuickList As New List(Of String)()

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraEmployee_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        txtDatefrom.EditValue = Now '.Month & "/" & Now.Year
        txtDateTo.EditValue = Now '.Month & "/" & Now.Year ' txtDatefrom.DateTime.AddDays(System.DateTime.DaysInMonth(Now.Year, Now.Month)) '.AddDays(-1)

        loadEmp()
    End Sub
    Private Sub loadEmp()
        Dim Emp As DataSet = New DataSet
        Dim sSql As String = "select tblemployee.Paycode, tblemployee.EmpName from tblemployee, Pay_Master where tblemployee.paycode = Pay_Master.paycode and tblemployee.paycode in ('" & String.Join("', '", Common.EmpArr) & "')"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(sSql, Common.con1)
            dataAdapter.Fill(Emp)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(sSql, Common.con)
            dataAdapter.Fill(Emp)
        End If
        GridControlEmp.DataSource = Emp.Tables(0)
        GridViewEmp.ClearSelection()
    End Sub
    Private Sub txtDateTo_Leave(sender As System.Object, e As System.EventArgs) Handles txtDateTo.Leave
        DateLeave()
    End Sub
    Private Sub DateLeave()
        Dim startdate As DateTime = Convert.ToDateTime(txtDatefrom.DateTime.Year & "-" & txtDatefrom.DateTime.Month & "-01 00:00:00")
        'Dim enddate As DateTime = txtDatefrom.DateTime.AddDays(System.DateTime.DaysInMonth(txtDatefrom.DateTime.Year, txtDatefrom.DateTime.Month)) '.AddDays(-1)
        Dim enddate As DateTime = Convert.ToDateTime(txtDateTo.DateTime.Year & "-" & txtDateTo.DateTime.Month & "-" & System.DateTime.DaysInMonth(txtDateTo.DateTime.Year, txtDateTo.DateTime.Month))

        Dim selectedRows As Integer() = GridViewEmp.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim EmpQuickList As New List(Of String)()
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                EmpQuickList.Add(GridViewEmp.GetRowCellValue(rowHandle, "Paycode").ToString.Trim)
            End If
        Next
        If EmpQuickList.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Employees Selected</size>", "<size=9>iAS</size>")
            XtraMasterTest.LabelControlStatus.Text = ""
            Exit Sub
        End If
        Dim Emp As DataSet = New DataSet
        Dim sSql As String = "select a.Paycode,b.EmpName,a.mon_year as Month,a.vmed_amt+a.vconv_amt+a.vi_1_amt+a.vi_2_amt+a.vi_3_amt+a.vi_4_amt+a.vi_5_amt+a.vi_6_amt+a.vi_7_amt+a.vi_8_amt+a.vi_9_amt+a.vi_10_amt as TotalAmt,a.Paid from PAY_REIMURSH a, tblemployee b where a.paycode=b.paycode and a.paycode in ('" & String.Join("', '", EmpQuickList.ToArray()) & "') and a.mon_year between '" & startdate.ToString("yyyy-MM-dd") & "' and '" & enddate.ToString("yyyy-MM-dd") & "' " & _
            " and (a.vmed_amt+a.vconv_amt+a.vi_1_amt+a.vi_2_amt+a.vi_3_amt+a.vi_4_amt+a.vi_5_amt+a.vi_6_amt+a.vi_7_amt+a.vi_8_amt+a.vi_9_amt+a.vi_10_amt) > 0 " & _
             "order by a.paycode,a.mon_year"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(sSql, Common.con1)
            dataAdapter.Fill(Emp)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(sSql, Common.con)
            dataAdapter.Fill(Emp)
        End If
        GridControl1.DataSource = Emp.Tables(0)
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("Paycode"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub SimpleButtonPaid_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonPaid.Click
        If GridView1.SelectedRowsCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Entries Selected</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        Dim startdate As DateTime = Convert.ToDateTime(txtDatefrom.DateTime.Year & "-" & txtDatefrom.DateTime.Month & "-01 00:00:00")
        Dim enddate As DateTime = txtDatefrom.DateTime.AddDays(System.DateTime.DaysInMonth(txtDatefrom.DateTime.Year, txtDatefrom.DateTime.Month)) '.AddDays(-1)

        'Dim EmpQuickList As New List(Of String)()
        Dim sSql As String = ""
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                Dim paycode As String = (GridView1.GetRowCellValue(rowHandle, "Paycode").ToString.Trim)
                Dim mon_year As DateTime = Convert.ToDateTime(GridView1.GetRowCellValue(rowHandle, "Month").ToString.Trim)
                sSql = "update PAY_REIMURSH set paid='Y'  where paycode='" & paycode & "' and mon_year = '" & mon_year.ToString("yyyy-MM-dd") & "'"
                If Common.servername = "Access" Then
                    Try
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Catch ex As Exception
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    End Try
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                DateLeave()
            End If
        Next
    End Sub
    Private Sub SimpleButtonUnpaid_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonUnpaid.Click
        If GridView1.SelectedRowsCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Entries Selected</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        Dim startdate As DateTime = Convert.ToDateTime(txtDatefrom.DateTime.Year & "-" & txtDatefrom.DateTime.Month & "-01 00:00:00")
        Dim enddate As DateTime = txtDatefrom.DateTime.AddDays(System.DateTime.DaysInMonth(txtDatefrom.DateTime.Year, txtDatefrom.DateTime.Month)) '.AddDays(-1)

        'Dim EmpQuickList As New List(Of String)()
        Dim sSql As String = ""
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                Dim paycode As String = (GridView1.GetRowCellValue(rowHandle, "Paycode").ToString.Trim)
                Dim mon_year As DateTime = Convert.ToDateTime(GridView1.GetRowCellValue(rowHandle, "Month").ToString.Trim)
                sSql = "update PAY_REIMURSH set paid='N'  where paycode='" & paycode & "' and mon_year = '" & mon_year.ToString("yyyy-MM-dd") & "'"
                If Common.servername = "Access" Then
                    Try
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Catch ex As Exception
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    End Try
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                DateLeave()
            End If
        Next
    End Sub
End Class
