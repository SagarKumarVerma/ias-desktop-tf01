﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraTimeZoneEditZK
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnTZCreate = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditTZCre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSunEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSunStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.TextEditTZCre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSunEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSunStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnTZCreate
        '
        Me.btnTZCreate.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnTZCreate.Appearance.Options.UseFont = True
        Me.btnTZCreate.Location = New System.Drawing.Point(141, 220)
        Me.btnTZCreate.Name = "btnTZCreate"
        Me.btnTZCreate.Size = New System.Drawing.Size(104, 23)
        Me.btnTZCreate.TabIndex = 16
        Me.btnTZCreate.Text = "Set TimeZone"
        '
        'TextEditTZCre
        '
        Me.TextEditTZCre.Location = New System.Drawing.Point(101, 12)
        Me.TextEditTZCre.Name = "TextEditTZCre"
        Me.TextEditTZCre.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTZCre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTZCre.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTZCre.Properties.MaxLength = 3
        Me.TextEditTZCre.Properties.ReadOnly = True
        Me.TextEditTZCre.Size = New System.Drawing.Size(53, 20)
        Me.TextEditTZCre.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(10, 15)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl7.TabIndex = 72
        Me.LabelControl7.Text = "Time Zone ID 1"
        '
        'TxtSunEndTime
        '
        Me.TxtSunEndTime.EditValue = "23:59"
        Me.TxtSunEndTime.Location = New System.Drawing.Point(247, 38)
        Me.TxtSunEndTime.Name = "TxtSunEndTime"
        Me.TxtSunEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSunEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunEndTime.Properties.MaxLength = 5
        Me.TxtSunEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunEndTime.TabIndex = 3
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(219, 39)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl6.TabIndex = 70
        Me.LabelControl6.Text = "To"
        '
        'TxtSunStrtTime
        '
        Me.TxtSunStrtTime.EditValue = "00:00"
        Me.TxtSunStrtTime.Location = New System.Drawing.Point(101, 38)
        Me.TxtSunStrtTime.Name = "TxtSunStrtTime"
        Me.TxtSunStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSunStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunStrtTime.Properties.MaxLength = 5
        Me.TxtSunStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunStrtTime.TabIndex = 2
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(9, 41)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl5.TabIndex = 68
        Me.LabelControl5.Text = "SUN Start Time"
        '
        'TxtMonEndTime
        '
        Me.TxtMonEndTime.EditValue = "23:59"
        Me.TxtMonEndTime.Location = New System.Drawing.Point(247, 64)
        Me.TxtMonEndTime.Name = "TxtMonEndTime"
        Me.TxtMonEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtMonEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonEndTime.Properties.MaxLength = 5
        Me.TxtMonEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonEndTime.TabIndex = 5
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(219, 65)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl1.TabIndex = 77
        Me.LabelControl1.Text = "To"
        '
        'TxtMonStrtTime
        '
        Me.TxtMonStrtTime.EditValue = "00:00"
        Me.TxtMonStrtTime.Location = New System.Drawing.Point(101, 64)
        Me.TxtMonStrtTime.Name = "TxtMonStrtTime"
        Me.TxtMonStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtMonStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonStrtTime.Properties.MaxLength = 5
        Me.TxtMonStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonStrtTime.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(9, 67)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl2.TabIndex = 75
        Me.LabelControl2.Text = "MON Start Time"
        '
        'TxtTueEndTime
        '
        Me.TxtTueEndTime.EditValue = "23:59"
        Me.TxtTueEndTime.Location = New System.Drawing.Point(247, 90)
        Me.TxtTueEndTime.Name = "TxtTueEndTime"
        Me.TxtTueEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtTueEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueEndTime.Properties.MaxLength = 5
        Me.TxtTueEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueEndTime.TabIndex = 7
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(219, 91)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl3.TabIndex = 81
        Me.LabelControl3.Text = "To"
        '
        'TxtTueStrtTime
        '
        Me.TxtTueStrtTime.EditValue = "00:00"
        Me.TxtTueStrtTime.Location = New System.Drawing.Point(101, 90)
        Me.TxtTueStrtTime.Name = "TxtTueStrtTime"
        Me.TxtTueStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtTueStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueStrtTime.Properties.MaxLength = 5
        Me.TxtTueStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueStrtTime.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(9, 93)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl4.TabIndex = 79
        Me.LabelControl4.Text = "TUE Start Time"
        '
        'TxtWedEndTime
        '
        Me.TxtWedEndTime.EditValue = "23:59"
        Me.TxtWedEndTime.Location = New System.Drawing.Point(247, 116)
        Me.TxtWedEndTime.Name = "TxtWedEndTime"
        Me.TxtWedEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtWedEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedEndTime.Properties.MaxLength = 5
        Me.TxtWedEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedEndTime.TabIndex = 9
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(219, 117)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl8.TabIndex = 85
        Me.LabelControl8.Text = "To"
        '
        'TxtWedStrtTime
        '
        Me.TxtWedStrtTime.EditValue = "00:00"
        Me.TxtWedStrtTime.Location = New System.Drawing.Point(101, 116)
        Me.TxtWedStrtTime.Name = "TxtWedStrtTime"
        Me.TxtWedStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtWedStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedStrtTime.Properties.MaxLength = 5
        Me.TxtWedStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedStrtTime.TabIndex = 8
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(9, 119)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl9.TabIndex = 83
        Me.LabelControl9.Text = "WED Start Time"
        '
        'TxtThuEndTime
        '
        Me.TxtThuEndTime.EditValue = "23:59"
        Me.TxtThuEndTime.Location = New System.Drawing.Point(247, 142)
        Me.TxtThuEndTime.Name = "TxtThuEndTime"
        Me.TxtThuEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtThuEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuEndTime.Properties.MaxLength = 5
        Me.TxtThuEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuEndTime.TabIndex = 11
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(219, 143)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl10.TabIndex = 89
        Me.LabelControl10.Text = "To"
        '
        'TxtThuStrtTime
        '
        Me.TxtThuStrtTime.EditValue = "00:00"
        Me.TxtThuStrtTime.Location = New System.Drawing.Point(101, 142)
        Me.TxtThuStrtTime.Name = "TxtThuStrtTime"
        Me.TxtThuStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtThuStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuStrtTime.Properties.MaxLength = 5
        Me.TxtThuStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuStrtTime.TabIndex = 10
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(9, 145)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl11.TabIndex = 87
        Me.LabelControl11.Text = "THU Start Time"
        '
        'TxtFriEndTime
        '
        Me.TxtFriEndTime.EditValue = "23:59"
        Me.TxtFriEndTime.Location = New System.Drawing.Point(247, 168)
        Me.TxtFriEndTime.Name = "TxtFriEndTime"
        Me.TxtFriEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtFriEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriEndTime.Properties.MaxLength = 5
        Me.TxtFriEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriEndTime.TabIndex = 13
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(219, 169)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl12.TabIndex = 93
        Me.LabelControl12.Text = "To"
        '
        'TxtFriStrtTime
        '
        Me.TxtFriStrtTime.EditValue = "00:00"
        Me.TxtFriStrtTime.Location = New System.Drawing.Point(101, 168)
        Me.TxtFriStrtTime.Name = "TxtFriStrtTime"
        Me.TxtFriStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtFriStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriStrtTime.Properties.MaxLength = 5
        Me.TxtFriStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriStrtTime.TabIndex = 12
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(9, 171)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl13.TabIndex = 91
        Me.LabelControl13.Text = "FRI Start Time"
        '
        'TxtSatEndTime
        '
        Me.TxtSatEndTime.EditValue = "23:59"
        Me.TxtSatEndTime.Location = New System.Drawing.Point(247, 194)
        Me.TxtSatEndTime.Name = "TxtSatEndTime"
        Me.TxtSatEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSatEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatEndTime.Properties.MaxLength = 5
        Me.TxtSatEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatEndTime.TabIndex = 15
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(219, 195)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl14.TabIndex = 97
        Me.LabelControl14.Text = "To"
        '
        'TxtSatStrtTime
        '
        Me.TxtSatStrtTime.EditValue = "00:00"
        Me.TxtSatStrtTime.Location = New System.Drawing.Point(101, 194)
        Me.TxtSatStrtTime.Name = "TxtSatStrtTime"
        Me.TxtSatStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSatStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatStrtTime.Properties.MaxLength = 5
        Me.TxtSatStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatStrtTime.TabIndex = 14
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(9, 197)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl15.TabIndex = 95
        Me.LabelControl15.Text = "SAT Start Time"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(251, 220)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 17
        Me.SimpleButton1.Text = "Close"
        '
        'XtraTimeZoneEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(350, 257)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.TxtSatEndTime)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.TxtSatStrtTime)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.TxtFriEndTime)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.TxtFriStrtTime)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.TxtThuEndTime)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.TxtThuStrtTime)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.TxtWedEndTime)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.TxtWedStrtTime)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.TxtTueEndTime)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TxtTueStrtTime)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.TxtMonEndTime)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TxtMonStrtTime)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.btnTZCreate)
        Me.Controls.Add(Me.TextEditTZCre)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.TxtSunEndTime)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.TxtSunStrtTime)
        Me.Controls.Add(Me.LabelControl5)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraTimeZoneEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.TextEditTZCre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSunEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSunStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnTZCreate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditTZCre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSunEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSunStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
