﻿Imports System
Imports System.Runtime.InteropServices
Imports System.IO
'entire filr is for finger print of visitor management ZK
Namespace Sample
    Public Class BitmapFormat
        Public Structure BITMAPFILEHEADER
            Public bfType As System.UInt16
            Public bfSize As Integer
            Public bfReserved1 As System.UInt16
            Public bfReserved2 As System.UInt16
            Public bfOffBits As Integer
        End Structure
        Public Structure MASK
            Public redmask As Byte
            Public greenmask As Byte
            Public bluemask As Byte
            Public rgbReserved As Byte
        End Structure

        Public Structure BITMAPINFOHEADER
            Public biSize As Integer
            Public biWidth As Integer
            Public biHeight As Integer
            Public biPlanes As System.UInt16
            Public biBitCount As System.UInt16
            Public biCompression As Integer
            Public biSizeImage As Integer
            Public biXPelsPerMeter As Integer
            Public biYPelsPerMeter As Integer
            Public biClrUsed As Integer
            Public biClrImportant As Integer
        End Structure

        Public Shared Sub RotatePic(ByVal BmpBuf() As Byte, ByVal width As Integer, ByVal height As Integer, ByRef ResBuf() As Byte)
            Dim RowLoop As Integer = 0
            Dim ColLoop As Integer = 0
            Dim BmpBuflen As Integer = (width * height)
            Try
                For RowLoop = 0 To BmpBuflen - 1 'width '   Do While (RowLoop < BmpBuflen)
                    'Do While (ColLoop < width)
                    For ColLoop = 0 To width - 1
                        ResBuf(RowLoop + ColLoop) = BmpBuf(BmpBuflen - RowLoop - width + ColLoop)
                        'ResBuf((RowLoop + ColLoop)) = BmpBuf(((BmpBuflen _
                        '            - (RowLoop - width)) _
                        '            + ColLoop))
                    Next
                    'ColLoop = (ColLoop + 1)
                    'Loop
                    RowLoop = (RowLoop + width - 1)
                Next RowLoop 'Loop
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Function StructToBytes(ByVal StructObj As Object, ByVal Size As Integer) As Byte()
            Dim StructSize As Integer = Marshal.SizeOf(StructObj)
            Dim GetBytes() As Byte = New Byte((StructSize) - 1) {}
            Try
                Dim StructPtr As IntPtr = Marshal.AllocHGlobal(StructSize)
                Marshal.StructureToPtr(StructObj, StructPtr, False)
                Marshal.Copy(StructPtr, GetBytes, 0, StructSize)
                Marshal.FreeHGlobal(StructPtr)
                If (Size = 14) Then
                    Dim NewBytes() As Byte = New Byte((Size) - 1) {}
                    Dim Count As Integer = 0
                    Dim LoopX As Integer = 0
                    For LoopX = 0 To StructSize - 1 ' Do While (LoopX < StructSize)
                        If ((LoopX <> 2) And (LoopX <> 3)) Then
                            NewBytes(Count) = GetBytes(LoopX)
                            Count = (Count + 1)
                        End If
                        'LoopX = (LoopX + 1)
                    Next '  Loop
                Return NewBytes
                Else
                    Return GetBytes
                End If
            Catch ex As Exception
                Return GetBytes
            End Try
        End Function
        Public Shared Sub GetBitmap(ByVal buffer() As Byte, ByVal nWidth As Integer, ByVal nHeight As Integer, ByRef ms As System.IO.MemoryStream)
            Dim ColorIndex As Integer = 0
            Dim m_nBitCount As System.UInt16 = 8
            Dim m_nColorTableEntries As Integer = 256
            Dim ResBuf() As Byte = New Byte((nWidth * nHeight * 2) - 1) {}
            Try
                Dim BmpHeader As BITMAPFILEHEADER = New BITMAPFILEHEADER
                Dim BmpInfoHeader As BITMAPINFOHEADER = New BITMAPINFOHEADER
                Dim ColorMask() As MASK = New MASK((m_nColorTableEntries) - 1) {}
                Dim w As Integer = (Math.Truncate((nWidth + 3) / 4) * 4) '(((nWidth + 3) / 4) * 4)
                'Dim w As Integer = (((nWidth + 3) / 4) * 4)
                'ͼƬͷ��Ϣ5
                BmpInfoHeader.biSize = Marshal.SizeOf(BmpInfoHeader)
                BmpInfoHeader.biWidth = nWidth
                BmpInfoHeader.biHeight = nHeight
                BmpInfoHeader.biPlanes = 1
                BmpInfoHeader.biBitCount = m_nBitCount
                BmpInfoHeader.biCompression = 0
                BmpInfoHeader.biSizeImage = 0
                BmpInfoHeader.biXPelsPerMeter = 0
                BmpInfoHeader.biYPelsPerMeter = 0
                BmpInfoHeader.biClrUsed = m_nColorTableEntries
                BmpInfoHeader.biClrImportant = m_nColorTableEntries
                '�ļ�ͷ��Ϣ
                BmpHeader.bfType = 19778
                'BmpHeader.bfOffBits = 14 + Marshal.SizeOf(BmpInfoHeader) + BmpInfoHeader.biClrUsed * 4
                'BmpHeader.bfSize = BmpHeader.bfOffBits + ((((w * BmpInfoHeader.biBitCount + 31) / 32) * 4) * BmpInfoHeader.biHeight)
                'BmpHeader.bfReserved1 = 0
                'BmpHeader.bfReserved2 = 0
                BmpHeader.bfOffBits = 14 + Marshal.SizeOf(BmpInfoHeader) + (BmpInfoHeader.biClrUsed * 4)
                BmpHeader.bfSize = BmpHeader.bfOffBits + ((Math.Truncate(((w * BmpInfoHeader.biBitCount) + 31) / 32) * 4) * BmpInfoHeader.biHeight)
                BmpHeader.bfReserved1 = 0
                BmpHeader.bfReserved2 = 0

                ms.Write(StructToBytes(BmpHeader, 14), 0, 14)
                ms.Write(StructToBytes(BmpInfoHeader, Marshal.SizeOf(BmpInfoHeader)), 0, Marshal.SizeOf(BmpInfoHeader))
                'ms.Write(BitmapFormat.StructToBytes(BmpHeader, 14), 0, 14)
                'ms.Write(BitmapFormat.StructToBytes(BmpInfoHeader, Marshal.SizeOf(BmpInfoHeader)), 0, Marshal.SizeOf(BmpInfoHeader))
                '���԰���Ϣ
                ColorIndex = 0
                'Do While (ColorIndex < m_nColorTableEntries)
                For ColorIndex = 0 To m_nColorTableEntries - 1
                    ColorMask(ColorIndex).redmask = CType(ColorIndex, Byte)
                    ColorMask(ColorIndex).greenmask = CType(ColorIndex, Byte)
                    ColorMask(ColorIndex).bluemask = CType(ColorIndex, Byte)
                    ColorMask(ColorIndex).rgbReserved = 0
                    ms.Write(StructToBytes(ColorMask(ColorIndex), Marshal.SizeOf(ColorMask(ColorIndex))), 0, Marshal.SizeOf(ColorMask(ColorIndex)))
                    'ms.Write(BitmapFormat.StructToBytes(ColorMask(ColorIndex), Marshal.SizeOf(ColorMask(ColorIndex))), 0, Marshal.SizeOf(ColorMask(ColorIndex)))
                    'ColorIndex = (ColorIndex + 1)
                Next
                'Loop

                'ͼƬ��ת�����ָ��ͼƬ����������
                BitmapFormat.RotatePic(buffer, nWidth, nHeight, ResBuf)
                Dim filter() As Byte = Nothing
                If ((w - nWidth) > 0) Then
                    'filter = New Byte(((w - nWidth)) - 1) {}
                    filter = New Byte(w - nWidth - 1) {}
                End If
                Dim i As Integer = 0
                For i = 0 To nHeight - 1 'Do While (i < nHeight)
                    ms.Write(ResBuf, (i * nWidth), nWidth)
                    If ((w - nWidth) > 0) Then
                        ms.Write(ResBuf, 0, (w - nWidth))
                    End If
                    'i = (i + 1)
                Next '  Loop
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Sub WriteBitmap(ByVal buffer() As Byte, ByVal nWidth As Integer, ByVal nHeight As Integer)
            Dim ColorIndex As Integer = 0
            Dim m_nBitCount As System.UInt16 = 8
            Dim m_nColorTableEntries As Integer = 256
            Dim ResBuf() As Byte = New Byte(((nWidth * nHeight)) - 1) {}
            Try
                Dim BmpHeader As BITMAPFILEHEADER = New BITMAPFILEHEADER
                Dim BmpInfoHeader As BITMAPINFOHEADER = New BITMAPINFOHEADER
                Dim ColorMask() As MASK = New MASK((m_nColorTableEntries) - 1) {}
                Dim w As Integer = (((nWidth + 3)/ 4) * 4)
                'ͼƬͷ��Ϣ
                BmpInfoHeader.biSize = Marshal.SizeOf(BmpInfoHeader)
                BmpInfoHeader.biWidth = nWidth
                BmpInfoHeader.biHeight = nHeight
                BmpInfoHeader.biPlanes = 1
                BmpInfoHeader.biBitCount = m_nBitCount
                BmpInfoHeader.biCompression = 0
                BmpInfoHeader.biSizeImage = 0
                BmpInfoHeader.biXPelsPerMeter = 0
                BmpInfoHeader.biYPelsPerMeter = 0
                BmpInfoHeader.biClrUsed = m_nColorTableEntries
                BmpInfoHeader.biClrImportant = m_nColorTableEntries
                '�ļ�ͷ��Ϣ
                BmpHeader.bfType = 19778
                BmpHeader.bfOffBits = (14 _
                            + (Marshal.SizeOf(BmpInfoHeader) _
                            + (BmpInfoHeader.biClrUsed * 4)))
                BmpHeader.bfSize = (BmpHeader.bfOffBits _
                            + (((((w * BmpInfoHeader.biBitCount) _
                            + 31) _
                            / 32) _
                            * 4) _
                            * BmpInfoHeader.biHeight))
                BmpHeader.bfReserved1 = 0
                BmpHeader.bfReserved2 = 0
                Dim FileStream As Stream = File.Open("finger.bmp", FileMode.Create, FileAccess.Write)
                Dim TmpBinaryWriter As BinaryWriter = New BinaryWriter(FileStream)
                TmpBinaryWriter.Write(BitmapFormat.StructToBytes(BmpHeader, 14))
                TmpBinaryWriter.Write(BitmapFormat.StructToBytes(BmpInfoHeader, Marshal.SizeOf(BmpInfoHeader)))
                '���԰���Ϣ
                ColorIndex = 0
                Do While (ColorIndex < m_nColorTableEntries)
                    ColorMask(ColorIndex).redmask = CType(ColorIndex, Byte)
                    ColorMask(ColorIndex).greenmask = CType(ColorIndex, Byte)
                    ColorMask(ColorIndex).bluemask = CType(ColorIndex, Byte)
                    ColorMask(ColorIndex).rgbReserved = 0
                    TmpBinaryWriter.Write(BitmapFormat.StructToBytes(ColorMask(ColorIndex), Marshal.SizeOf(ColorMask(ColorIndex))))
                    ColorIndex = (ColorIndex + 1)
                Loop

                'ͼƬ��ת�����ָ��ͼƬ����������
                BitmapFormat.RotatePic(buffer, nWidth, nHeight, ResBuf)
                'дͼƬ
                'TmpBinaryWriter.Write(ResBuf);
                Dim filter() As Byte = Nothing
                If ((w - nWidth) _
                            > 0) Then
                    filter = New Byte(((w - nWidth)) - 1) {}
                End If

                Dim i As Integer = 0
                Do While (i < nHeight)
                    TmpBinaryWriter.Write(ResBuf, (i * nWidth), nWidth)
                    If ((w - nWidth) _
                                > 0) Then
                        TmpBinaryWriter.Write(ResBuf, 0, (w - nWidth))
                    End If

                    i = (i + 1)
                Loop

                FileStream.Close()
                TmpBinaryWriter.Close()
            Catch ex As Exception
                'ZKCE.SysException.ZKCELogger logger = new ZKCE.SysException.ZKCELogger(ex);
                'logger.Append();
            End Try

        End Sub
    End Class
End Namespace