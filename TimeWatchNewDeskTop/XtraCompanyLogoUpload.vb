﻿Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraCompanyLogoUpload
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub TextEdit1_Click(sender As System.Object, e As System.EventArgs) Handles TextEdit1.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.png;;"
        dlg.ShowDialog()
        TextEdit1.Text = dlg.FileName
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEdit1.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the file to upload</size>", "<size>Error</size>")
        Else
            Try
                Me.Cursor = Cursors.WaitCursor
                My.Computer.FileSystem.CopyFile(TextEdit1.Text.Trim, My.Application.Info.DirectoryPath & "\TimeWatch_Logo_pdf.png", True)
                Me.Cursor = Cursors.Default
                Me.Close()
            Catch ex As Exception
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Could not copy the file " & vbCrLf & "Please Close the File and Try Again</size>", "<size>Error</size>")
            End Try
           
        End If

    End Sub

    Private Sub XtraCompanyLogoUpload_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEdit1.Text = ""
    End Sub
End Class