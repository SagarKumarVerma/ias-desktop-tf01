﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraVisitorPrint
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraVisitorPrint))
        Me.PictureEditPhoto = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEditLogo = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlSrNo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlName = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlComp = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlToMeet = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlDept = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlDate = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlTime = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.PrintForm1 = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LabelEscort = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelAdhar = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PictureEditPhoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEditLogo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureEditPhoto
        '
        Me.PictureEditPhoto.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEditPhoto.Location = New System.Drawing.Point(269, 52)
        Me.PictureEditPhoto.Name = "PictureEditPhoto"
        Me.PictureEditPhoto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PictureEditPhoto.Properties.Appearance.Options.UseFont = True
        Me.PictureEditPhoto.Properties.LookAndFeel.SkinName = "iMaginary"
        Me.PictureEditPhoto.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PictureEditPhoto.Properties.NullText = " "
        Me.PictureEditPhoto.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.PictureEditPhoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.PictureEditPhoto.Size = New System.Drawing.Size(120, 110)
        Me.PictureEditPhoto.TabIndex = 37
        '
        'PictureEditLogo
        '
        Me.PictureEditLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEditLogo.Location = New System.Drawing.Point(12, 12)
        Me.PictureEditLogo.Name = "PictureEditLogo"
        Me.PictureEditLogo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PictureEditLogo.Properties.Appearance.Options.UseFont = True
        Me.PictureEditLogo.Properties.LookAndFeel.SkinName = "iMaginary"
        Me.PictureEditLogo.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PictureEditLogo.Properties.NullText = " "
        Me.PictureEditLogo.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.PictureEditLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.PictureEditLogo.Size = New System.Drawing.Size(107, 49)
        Me.PictureEditLogo.TabIndex = 38
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(163, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl1.TabIndex = 39
        Me.LabelControl1.Text = "VISITOR PASS"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(125, 32)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 40
        Me.LabelControl2.Text = "Sr. No.: "
        '
        'LabelControlSrNo
        '
        Me.LabelControlSrNo.Location = New System.Drawing.Point(172, 32)
        Me.LabelControlSrNo.Name = "LabelControlSrNo"
        Me.LabelControlSrNo.Size = New System.Drawing.Size(22, 13)
        Me.LabelControlSrNo.TabIndex = 41
        Me.LabelControlSrNo.Text = "Srno"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 67)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl3.TabIndex = 42
        Me.LabelControl3.Text = "Name : "
        '
        'LabelControlName
        '
        Me.LabelControlName.Location = New System.Drawing.Point(55, 67)
        Me.LabelControlName.Name = "LabelControlName"
        Me.LabelControlName.Size = New System.Drawing.Size(27, 13)
        Me.LabelControlName.TabIndex = 43
        Me.LabelControlName.Text = "Name"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(12, 86)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl4.TabIndex = 44
        Me.LabelControl4.Text = "Company : "
        '
        'LabelControlComp
        '
        Me.LabelControlComp.Location = New System.Drawing.Point(73, 86)
        Me.LabelControlComp.Name = "LabelControlComp"
        Me.LabelControlComp.Size = New System.Drawing.Size(21, 13)
        Me.LabelControlComp.TabIndex = 45
        Me.LabelControlComp.Text = "Com"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 105)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(49, 13)
        Me.LabelControl5.TabIndex = 46
        Me.LabelControl5.Text = "To Meet : "
        '
        'LabelControlToMeet
        '
        Me.LabelControlToMeet.Location = New System.Drawing.Point(67, 105)
        Me.LabelControlToMeet.Name = "LabelControlToMeet"
        Me.LabelControlToMeet.Size = New System.Drawing.Size(36, 13)
        Me.LabelControlToMeet.TabIndex = 47
        Me.LabelControlToMeet.Text = "Tomeet"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(12, 124)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl6.TabIndex = 48
        Me.LabelControl6.Text = "Department : "
        '
        'LabelControlDept
        '
        Me.LabelControlDept.Location = New System.Drawing.Point(85, 124)
        Me.LabelControlDept.Name = "LabelControlDept"
        Me.LabelControlDept.Size = New System.Drawing.Size(23, 13)
        Me.LabelControlDept.TabIndex = 49
        Me.LabelControlDept.Text = "Dept"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(12, 143)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl7.TabIndex = 50
        Me.LabelControl7.Text = "Date : "
        '
        'LabelControlDate
        '
        Me.LabelControlDate.Location = New System.Drawing.Point(51, 143)
        Me.LabelControlDate.Name = "LabelControlDate"
        Me.LabelControlDate.Size = New System.Drawing.Size(23, 13)
        Me.LabelControlDate.TabIndex = 51
        Me.LabelControlDate.Text = "Date"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(13, 162)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl8.TabIndex = 52
        Me.LabelControl8.Text = "Time : "
        '
        'LabelControlTime
        '
        Me.LabelControlTime.Location = New System.Drawing.Point(51, 162)
        Me.LabelControlTime.Name = "LabelControlTime"
        Me.LabelControlTime.Size = New System.Drawing.Size(22, 13)
        Me.LabelControlTime.TabIndex = 53
        Me.LabelControlTime.Text = "Time"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(13, 253)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl9.TabIndex = 54
        Me.LabelControl9.Text = "(Sign-Visitor)"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(125, 253)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl10.TabIndex = 55
        Me.LabelControl10.Text = "(Sign-Person Meet)"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(263, 253)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl11.TabIndex = 56
        Me.LabelControl11.Text = "(Security Officer)"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(13, 273)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(290, 13)
        Me.LabelControl12.TabIndex = 57
        Me.LabelControl12.Text = "Please return the Pass to the Security Office before leaving."
        '
        'PrintForm1
        '
        Me.PrintForm1.DocumentName = "document"
        Me.PrintForm1.Form = Me
        Me.PrintForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPrinter
        Me.PrintForm1.PrinterSettings = CType(resources.GetObject("PrintForm1.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.PrintForm1.PrintFileName = Nothing
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'LabelEscort
        '
        Me.LabelEscort.Location = New System.Drawing.Point(89, 200)
        Me.LabelEscort.Name = "LabelEscort"
        Me.LabelEscort.Size = New System.Drawing.Size(30, 13)
        Me.LabelEscort.TabIndex = 61
        Me.LabelEscort.Text = "Escort"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(14, 200)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl14.TabIndex = 60
        Me.LabelControl14.Text = "Escort Name : "
        '
        'LabelAdhar
        '
        Me.LabelAdhar.Location = New System.Drawing.Point(55, 181)
        Me.LabelAdhar.Name = "LabelAdhar"
        Me.LabelAdhar.Size = New System.Drawing.Size(29, 13)
        Me.LabelAdhar.TabIndex = 59
        Me.LabelAdhar.Text = "Adhar"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(13, 181)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl16.TabIndex = 58
        Me.LabelControl16.Text = "Adhar :"
        '
        'XtraVisitorPrint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 303)
        Me.Controls.Add(Me.LabelEscort)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.LabelAdhar)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControlTime)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControlDate)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControlDept)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControlToMeet)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControlComp)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControlName)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControlSrNo)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.PictureEditLogo)
        Me.Controls.Add(Me.PictureEditPhoto)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraVisitorPrint"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.PictureEditPhoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEditLogo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureEditPhoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEditLogo As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlSrNo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlComp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlToMeet As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlDate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlTime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PrintForm1 As Microsoft.VisualBasic.PowerPacks.Printing.PrintForm
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LabelEscort As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelAdhar As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
End Class
