﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Text.RegularExpressions

Public Class XtraConpanyInfo
    Dim ulf As UserLookAndFeel
    Private Sub XtraConpanyInfo_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If Common.checkOpenedFromLicense = "" Then
            Application.Exit()
        End If
    End Sub

    Private Sub XtraConpanyInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        GetDefault()
    End Sub
    Private Sub GetDefault()
        Dim adap As SqlDataAdapter
        Dim dsL As DataSet = New DataSet
        Dim sSql As String = "select * from InstallSystemInfo"
        If Common.checkOpenedFromLicense = "Admin" Then
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsL)
            If dsL.Tables(0).Rows.Count > 0 Then
                TextEditLicense.Text = dsL.Tables(0).Rows(0).Item("License").ToString.Trim
                TextEditCompanyName.Text = dsL.Tables(0).Rows(0).Item("ComName").ToString.Trim
                MemoEditAddress.Text = dsL.Tables(0).Rows(0).Item("ComAdd").ToString.Trim
                TextEditContact.Text = dsL.Tables(0).Rows(0).Item("ComContact").ToString.Trim
                TextEditEmail.Text = dsL.Tables(0).Rows(0).Item("ComEmail").ToString.Trim
                TextEditUserKey.Text = dsL.Tables(0).Rows(0).Item("UserKey").ToString.Trim
                SimpleButtonSave.Visible = False
                SimpleButtonVerify.Visible = False
            Else
                TextEditLicense.Text = ""
                TextEditCompanyName.Text = ""
                MemoEditAddress.Text = ""
                TextEditContact.Text = ""
                TextEditEmail.Text = ""
                TextEditUserKey.Text = ""
                SimpleButtonSave.Visible = True
                SimpleButtonVerify.Visible = True
            End If
        Else
            TextEditLicense.Text = ""
            TextEditCompanyName.Text = ""
            MemoEditAddress.Text = ""
            TextEditContact.Text = ""
            TextEditEmail.Text = ""
            TextEditUserKey.Text = ""
            SimpleButtonSave.Visible = True
            SimpleButtonVerify.Visible = True
        End If


    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim ConnectionString As String = "Data Source=137.159.201.167;Initial Catalog=TimeWatch_License;Integrated Security=True"
        Dim contmp As SqlConnection = New SqlConnection(ConnectionString)

        Dim MAC As String = getMacAddress()
        If TextEditCompanyName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Name cannot be empty</size>", "<size=9></size>")
            TextEditCompanyName.Select()
            Exit Sub
        End If

        If MemoEditAddress.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Address cannot be empty</size>", "<size=9></size>")
            MemoEditAddress.Select()
            Exit Sub
        End If

        If TextEditContact.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Contact cannot be empty</size>", "<size=9></size>")
            TextEditContact.Select()
            Exit Sub
        End If

        If TextEditEmail.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Email cannot be empty</size>", "<size=9></size>")
            TextEditEmail.Select()
            Exit Sub
        End If

        If TextEditUserKey.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>User Key cannot be empty</size>", "<size=9></size>")
            TextEditUserKey.Select()
            Exit Sub
        End If

        If emailaddresscheck(TextEditEmail.Text.Trim) = False Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Email </size>", "<size=9></size>")
            TextEditEmail.Select()
            Exit Sub
        End If

        Dim License As String = TextEditLicense.Text.Trim
        Dim ComName As String = TextEditCompanyName.Text.Trim
        Dim ComAdd As String = MemoEditAddress.Text.Trim
        Dim ComContact As String = TextEditContact.Text.Trim
        Dim ComEmail As String = TextEditEmail.Text.Trim
        Dim UserKey As String = TextEditUserKey.Text

        Dim cmd As New SqlCommand
        Dim sSql As String = "insert into InstallSystemInfo (MAC,ComName,ComAdd,ComContact,ComEmail,UserKey,License) values ('" & MAC & "','" & ComName & "','" & ComAdd & "','" & ComContact & "','" & ComEmail & "','" & UserKey & "','" & License & "')"
        If Common.con.State <> ConnectionState.Open Then
            Common.con.Open()
        End If
        cmd = New SqlCommand(sSql, Common.con)
        cmd.ExecuteNonQuery()
        If Common.con.State <> ConnectionState.Closed Then
            Common.con.Close()
        End If
        Application.Exit()
        Process.Start(Application.ExecutablePath)
    End Sub
    Function getMacAddress()
        Dim nics() As NetworkInterface = NetworkInterface.GetAllNetworkInterfaces()
        Return nics(1).GetPhysicalAddress.ToString
    End Function
    Private Sub SimpleButtonVerify_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonVerify.Click
        Me.Cursor = Cursors.WaitCursor
        Dim ConnectionString As String = "Data Source=137.59.201.167;Initial Catalog=TimeWatch_License;Integrated Security=True"
        ConnectionString = "Data Source=137.59.201.167,1433;Initial Catalog=TimeWatch_License;User Id=sa;Password=sss;"
        'ConnectionString = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'SSSDB';Integrated Security=True"
        Dim contmp As SqlConnection = New SqlConnection(ConnectionString)
        Dim MAC As String = getMacAddress()

        Dim cmd As New SqlCommand

        Dim sSql As String = "Select * from Infolicense where UserKey = '" & TextEditUserKey.Text.Trim & "'"
        Dim adap As SqlDataAdapter
        Dim ds As DataSet = New DataSet
        Try
            adap = New SqlDataAdapter(sSql, contmp)
            adap.Fill(ds)
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>Please make sure that you are connected to Internet</size>", "<size=9></size>")
            Me.Cursor = Cursors.Default
            Exit Sub
        End Try

        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid UserKey</size>", "<size=9></size>")
            TextEditUserKey.Select()
            Exit Sub
        Else

            If ds.Tables(0).Rows(0).Item("NoOfUsersAllowed").ToString.Trim <= ds.Tables(0).Rows(0).Item("UserUsed").ToString.Trim Then
                Dim macArr() As String = ds.Tables(0).Rows(0).Item("MAC").ToString.Trim.Split(",")
                For i As Integer = 0 To macArr.Length - 1
                    If MAC = macArr(i) Then
                        GoTo tmp
                    End If
                Next
                XtraMessageBox.Show(ulf, "<size=10>UserKey already used for maximum number of Users</size>", "<size=9></size>")
                TextEditUserKey.Select()
                Me.Cursor = Cursors.Default
                Exit Sub
            Else
                'Dim macArry() As String = ds.Tables(0).Rows(0).Item("NoOfUsersAllowed").ToString.Trim.Split(",")
tmp:            TextEditLicense.Text = ds.Tables(0).Rows(0).Item("License").ToString.Trim
                If ds.Tables(0).Rows(0).Item("NoOfUsersAllowed").ToString.Trim = 1 Then
                    sSql = "update Infolicense set MAC='" & MAC & "' , LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' , UserUsed = UserUsed + 1   where UserKey ='" & TextEditUserKey.Text.Trim & "'"
                Else
                    sSql = "update Infolicense set MAC=MAC + ,'" & MAC & "' , LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'  , UserUsed = UserUsed + 1  where UserKey ='" & TextEditUserKey.Text.Trim & "'"
                End If
                If contmp.State <> ConnectionState.Open Then
                    contmp.Open()
                End If
                cmd = New SqlCommand(sSql, contmp)
                cmd.ExecuteNonQuery()
                If contmp.State <> ConnectionState.Closed Then
                    contmp.Close()
                End If
                XtraMessageBox.Show(ulf, "<size=10>UserKey Activated Successfully</size>", "<size=9></size>")
                SimpleButtonVerify.Enabled = False
            End If
        End If
        Me.Cursor = Cursors.Default
        SimpleButtonSave.Enabled = True
    End Sub
    Private Function emailaddresscheck(ByVal emailaddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailaddress, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function
End Class