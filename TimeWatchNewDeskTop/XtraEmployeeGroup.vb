﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Text

Public Class XtraEmployeeGroup
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection
    'Dim ConnectionString As String
    Dim adap1, adap As SqlDataAdapter
    Dim ds As DataSet
    Dim ulf As UserLookAndFeel
    Public Shared GpId As String

    Public Sub New()
        InitializeComponent()

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True


        'Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        'Dim sr As StreamReader = New StreamReader(fs)
        'Dim str As String
        'Dim str1() As String
        'Do While sr.Peek <> -1
        '    str = sr.ReadLine
        '    str1 = str.Split(",")
        '    servername = str1(0)
        'Loop
        'sr.Close()
        'fs.Close()

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.EmployeeGroup1TableAdapter.Fill(Me.SSSDBDataSet.EmployeeGroup1)
            GridControl1.DataSource = SSSDBDataSet.EmployeeGroup1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            EmployeeGroupTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.EmployeeGroupTableAdapter.Fill(Me.SSSDBDataSet.EmployeeGroup)
            GridControl1.DataSource = SSSDBDataSet.EmployeeGroup
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub

    Private Sub XtraEmployeeGroup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraEmployeeGroup).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        If Common.SecDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        'Dim view As GridView = CType(sender, GridView)
        'view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        'view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")

    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.EmployeeGroupTableAdapter.Update(Me.SSSDBDataSet.EmployeeGroup)
        Me.EmployeeGroup1TableAdapter.Update(Me.SSSDBDataSet.EmployeeGroup1)
        'Me.TblGrade1TableAdapter1.Update(Me.SSSDBDataSet.tblGrade1)

        Common.SetEmpGrpId()
        Common.LoadGroupStruct()

        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.EmployeeGroupTableAdapter.Update(Me.SSSDBDataSet.EmployeeGroup)
        Me.EmployeeGroup1TableAdapter.Update(Me.SSSDBDataSet.EmployeeGroup1)
        'Me.TblGrade1TableAdapter1.Update(Me.SSSDBDataSet.tblGrade1)
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                    'button.Visible = False
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
    End Sub

    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim sSql As String = "select * from EmployeeGroup"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count = 1 Then
                    Me.Validate()
                    e.Handled = True
                    XtraMessageBox.Show(ulf, "<size=10>Minimum one record is necessary.</size>", "<size=9>Error</size>")
                End If


                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("GroupId").ToString.Trim

                'Dim adap As SqlDataAdapter
                'Dim adapA As OleDbDataAdapter
                'Dim ds As DataSet = New DataSet
                sSql = "select count(*) from TblEmployee where EmployeeGroupId = '" & CellId & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Employee Group already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Validate()
                    e.Handled = True
                    Exit Sub
                Else
                    Dim cmd1 As OleDbCommand
                    Dim cmd As SqlCommand
                    Dim deleteGrpLeaveLedger As String = "DELETE from EmployeeGroupLeaveLedger where GroupId = '" & CellId & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(deleteGrpLeaveLedger, Common.con1)
                        cmd1.ExecuteNonQuery()
                        Common.con1.Close()
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(deleteGrpLeaveLedger, Common.con)
                        cmd.ExecuteNonQuery()
                        Common.con.Close()
                    End If
                    Common.LogPost("Employee Group Delete; Group Id='" & CellId)
                End If

            End If
        End If
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.SecAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.SecModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        'Try
        '    Dim CellID As String = row("GroupId").ToString.Trim
        '    GridView1.OptionsEditForm.CustomEditFormLayout = New XtraEmpGroupEdit(CellID)
        'Catch ex As Exception
        '    GridView1.OptionsEditForm.CustomEditFormLayout = New XtraEmpGroupEdit("")
        'End Try
        Try
            GpId = row("GroupId").ToString.Trim
        Catch ex As Exception
            GpId = ""
        End Try
        e.Allow = False

        Dim adapA As OleDbDataAdapter
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter("select count(*) from tblShiftMaster", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Shift master is empty. Cannot add new Employee Group</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Else
            adap = New SqlDataAdapter("select count(*) from tblShiftMaster", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Shift master is empty. Cannot add new Employee Group</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If
        XtraEmpGrpEdit.ShowDialog()
        If Common.servername = "Access" Then   'to refresh the grid
            EmployeeGroup1TableAdapter.Fill(SSSDBDataSet.EmployeeGroup1)
        Else
            EmployeeGroupTableAdapter.Fill(SSSDBDataSet.EmployeeGroup)
        End If
    End Sub
End Class
