﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraMaster
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ButtonImageOptions1 As DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions = New DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraMaster))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanel8 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlCount = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel7 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlUser = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel6 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlStatus = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel5 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlTime = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel4 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.NavigationBarItem6 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem5 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem4 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem3 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem2 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem1 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.OfficeNavigationBar1 = New DevExpress.XtraBars.Navigation.OfficeNavigationBar()
        Me.NavigationBarItem9 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem8 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem7 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem10 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem11 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.NavigationBarItem12 = New DevExpress.XtraBars.Navigation.NavigationBarItem()
        Me.TimerShowClock = New System.Windows.Forms.Timer(Me.components)
        Me.TimerReportEmail = New System.Windows.Forms.Timer(Me.components)
        Me.AlertControl1 = New DevExpress.XtraBars.Alerter.AlertControl(Me.components)
        Me.ToastNotificationsManager1 = New DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(Me.components)
        Me.TimerDataProcess = New System.Windows.Forms.Timer(Me.components)
        Me.TimerSMS = New System.Windows.Forms.Timer(Me.components)
        Me.TimerCloud = New System.Windows.Forms.Timer(Me.components)
        Me.TimerAutoDownload = New System.Windows.Forms.Timer(Me.components)
        Me.TimerAutpBK = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SidePanel2.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        Me.SidePanel8.SuspendLayout()
        Me.SidePanel7.SuspendLayout()
        Me.SidePanel6.SuspendLayout()
        Me.SidePanel5.SuspendLayout()
        Me.SidePanel4.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.OfficeNavigationBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToastNotificationsManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.SidePanel2)
        Me.PanelControl1.Controls.Add(Me.SidePanel1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 45)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(913, 439)
        Me.PanelControl1.TabIndex = 3
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.PanelControl2)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel2.Location = New System.Drawing.Point(2, 2)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(909, 412)
        Me.SidePanel2.TabIndex = 1
        Me.SidePanel2.Text = "SidePanel2"
        '
        'PanelControl2
        '
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(909, 412)
        Me.PanelControl2.TabIndex = 0
        '
        'SidePanel1
        '
        Me.SidePanel1.AllowResize = False
        Me.SidePanel1.Controls.Add(Me.SidePanel8)
        Me.SidePanel1.Controls.Add(Me.SidePanel7)
        Me.SidePanel1.Controls.Add(Me.SidePanel6)
        Me.SidePanel1.Controls.Add(Me.SidePanel5)
        Me.SidePanel1.Controls.Add(Me.SidePanel4)
        Me.SidePanel1.Controls.Add(Me.SidePanel3)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(2, 414)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(909, 23)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SidePanel8
        '
        Me.SidePanel8.Controls.Add(Me.LabelControlCount)
        Me.SidePanel8.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel8.Location = New System.Drawing.Point(188, 1)
        Me.SidePanel8.Name = "SidePanel8"
        Me.SidePanel8.Size = New System.Drawing.Size(270, 22)
        Me.SidePanel8.TabIndex = 5
        Me.SidePanel8.Text = "SidePanel8"
        '
        'LabelControlCount
        '
        Me.LabelControlCount.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlCount.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlCount.Appearance.Options.UseFont = True
        Me.LabelControlCount.Appearance.Options.UseForeColor = True
        Me.LabelControlCount.Location = New System.Drawing.Point(4, 4)
        Me.LabelControlCount.Name = "LabelControlCount"
        Me.LabelControlCount.Size = New System.Drawing.Size(48, 14)
        Me.LabelControlCount.TabIndex = 4
        Me.LabelControlCount.Text = "            "
        '
        'SidePanel7
        '
        Me.SidePanel7.Controls.Add(Me.LabelControlUser)
        Me.SidePanel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel7.Location = New System.Drawing.Point(458, 1)
        Me.SidePanel7.Name = "SidePanel7"
        Me.SidePanel7.Size = New System.Drawing.Size(74, 22)
        Me.SidePanel7.TabIndex = 4
        Me.SidePanel7.Text = "SidePanel7"
        '
        'LabelControlUser
        '
        Me.LabelControlUser.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlUser.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlUser.Appearance.Options.UseFont = True
        Me.LabelControlUser.Appearance.Options.UseForeColor = True
        Me.LabelControlUser.Location = New System.Drawing.Point(5, 3)
        Me.LabelControlUser.Name = "LabelControlUser"
        Me.LabelControlUser.Size = New System.Drawing.Size(56, 14)
        Me.LabelControlUser.TabIndex = 3
        Me.LabelControlUser.Text = "              "
        '
        'SidePanel6
        '
        Me.SidePanel6.Controls.Add(Me.LabelControlStatus)
        Me.SidePanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel6.Location = New System.Drawing.Point(124, 1)
        Me.SidePanel6.Name = "SidePanel6"
        Me.SidePanel6.Size = New System.Drawing.Size(408, 22)
        Me.SidePanel6.TabIndex = 3
        Me.SidePanel6.Text = "SidePanel6"
        '
        'LabelControlStatus
        '
        Me.LabelControlStatus.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlStatus.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlStatus.Appearance.Options.UseFont = True
        Me.LabelControlStatus.Appearance.Options.UseForeColor = True
        Me.LabelControlStatus.Location = New System.Drawing.Point(4, 4)
        Me.LabelControlStatus.Name = "LabelControlStatus"
        Me.LabelControlStatus.Size = New System.Drawing.Size(32, 14)
        Me.LabelControlStatus.TabIndex = 3
        Me.LabelControlStatus.Text = "        "
        '
        'SidePanel5
        '
        Me.SidePanel5.Controls.Add(Me.LabelControlTime)
        Me.SidePanel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel5.Location = New System.Drawing.Point(532, 1)
        Me.SidePanel5.Name = "SidePanel5"
        Me.SidePanel5.Size = New System.Drawing.Size(100, 22)
        Me.SidePanel5.TabIndex = 2
        Me.SidePanel5.Text = "SidePanel5"
        '
        'LabelControlTime
        '
        Me.LabelControlTime.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlTime.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlTime.Appearance.Options.UseFont = True
        Me.LabelControlTime.Appearance.Options.UseForeColor = True
        Me.LabelControlTime.Location = New System.Drawing.Point(4, 3)
        Me.LabelControlTime.Name = "LabelControlTime"
        Me.LabelControlTime.Size = New System.Drawing.Size(56, 14)
        Me.LabelControlTime.TabIndex = 3
        Me.LabelControlTime.Text = "              "
        '
        'SidePanel4
        '
        Me.SidePanel4.Controls.Add(Me.LabelControl2)
        Me.SidePanel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel4.Location = New System.Drawing.Point(632, 1)
        Me.SidePanel4.Name = "SidePanel4"
        Me.SidePanel4.Size = New System.Drawing.Size(277, 22)
        Me.SidePanel4.TabIndex = 1
        Me.SidePanel4.Text = "SidePanel4"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Location = New System.Drawing.Point(4, 3)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "LabelControl2"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.LabelControl1)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanel3.Location = New System.Drawing.Point(0, 1)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(124, 22)
        Me.SidePanel3.TabIndex = 0
        Me.SidePanel3.Text = "SidePanel3"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(70, 16)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "iAS V 2.3.0"
        '
        'NavigationBarItem6
        '
        Me.NavigationBarItem6.Name = "NavigationBarItem6"
        Me.NavigationBarItem6.Text = "Leave Management"
        '
        'NavigationBarItem5
        '
        Me.NavigationBarItem5.Name = "NavigationBarItem5"
        Me.NavigationBarItem5.Text = "Data Process"
        Me.NavigationBarItem5.Visible = False
        '
        'NavigationBarItem4
        '
        Me.NavigationBarItem4.Name = "NavigationBarItem4"
        Me.NavigationBarItem4.Text = "Employee"
        Me.NavigationBarItem4.Visible = False
        '
        'NavigationBarItem3
        '
        Me.NavigationBarItem3.Name = "NavigationBarItem3"
        Me.NavigationBarItem3.Text = "Master"
        '
        'NavigationBarItem2
        '
        Me.NavigationBarItem2.Name = "NavigationBarItem2"
        Me.NavigationBarItem2.Text = "Device"
        '
        'NavigationBarItem1
        '
        Me.NavigationBarItem1.Name = "NavigationBarItem1"
        Me.NavigationBarItem1.Text = "Home"
        '
        'OfficeNavigationBar1
        '
        Me.OfficeNavigationBar1.AutoSize = False
        Me.OfficeNavigationBar1.CustomizationButtonVisibility = DevExpress.XtraBars.Navigation.CustomizationButtonVisibility.Hidden
        Me.OfficeNavigationBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.OfficeNavigationBar1.Items.AddRange(New DevExpress.XtraBars.Navigation.NavigationBarItem() {Me.NavigationBarItem1, Me.NavigationBarItem2, Me.NavigationBarItem3, Me.NavigationBarItem4, Me.NavigationBarItem5, Me.NavigationBarItem9, Me.NavigationBarItem6, Me.NavigationBarItem8, Me.NavigationBarItem7, Me.NavigationBarItem10, Me.NavigationBarItem11, Me.NavigationBarItem12})
        Me.OfficeNavigationBar1.Location = New System.Drawing.Point(0, 0)
        Me.OfficeNavigationBar1.LookAndFeel.SkinName = "iMaginary"
        Me.OfficeNavigationBar1.Name = "OfficeNavigationBar1"
        ButtonImageOptions1.Image = CType(resources.GetObject("ButtonImageOptions1.Image"), System.Drawing.Image)
        Me.OfficeNavigationBar1.OptionsPeekFormButtonPanel.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.Utils.PeekFormButton("Button", True, ButtonImageOptions1, True, Nothing), New DevExpress.Utils.PeekFormButton(), New DevExpress.Utils.PeekFormButton(), New DevExpress.Utils.PeekFormButton()})
        Me.OfficeNavigationBar1.PeekFormShowDelay = 1
        Me.OfficeNavigationBar1.SelectedItem = Me.NavigationBarItem1
        Me.OfficeNavigationBar1.Size = New System.Drawing.Size(913, 45)
        Me.OfficeNavigationBar1.TabIndex = 2
        Me.OfficeNavigationBar1.Text = "OfficeNavigationBar1"
        '
        'NavigationBarItem9
        '
        Me.NavigationBarItem9.Name = "NavigationBarItem9"
        Me.NavigationBarItem9.Text = "Transaction"
        '
        'NavigationBarItem8
        '
        Me.NavigationBarItem8.Name = "NavigationBarItem8"
        Me.NavigationBarItem8.Text = "Admin"
        '
        'NavigationBarItem7
        '
        Me.NavigationBarItem7.Name = "NavigationBarItem7"
        Me.NavigationBarItem7.Text = "Reports"
        '
        'NavigationBarItem10
        '
        Me.NavigationBarItem10.Name = "NavigationBarItem10"
        Me.NavigationBarItem10.Text = "Payroll"
        '
        'NavigationBarItem11
        '
        Me.NavigationBarItem11.Name = "NavigationBarItem11"
        Me.NavigationBarItem11.Text = "Canteen"
        Me.NavigationBarItem11.Visible = False
        '
        'NavigationBarItem12
        '
        Me.NavigationBarItem12.Name = "NavigationBarItem12"
        Me.NavigationBarItem12.Text = "Visitor"
        Me.NavigationBarItem12.Visible = False
        '
        'TimerShowClock
        '
        Me.TimerShowClock.Enabled = True
        Me.TimerShowClock.Interval = 1000
        '
        'TimerReportEmail
        '
        Me.TimerReportEmail.Enabled = True
        Me.TimerReportEmail.Interval = 60000
        '
        'AlertControl1
        '
        Me.AlertControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.AlertControl1.AppearanceCaption.Options.UseFont = True
        Me.AlertControl1.AppearanceHotTrackedText.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Underline)
        Me.AlertControl1.AppearanceHotTrackedText.Options.UseFont = True
        Me.AlertControl1.AppearanceText.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.AlertControl1.AppearanceText.Options.UseFont = True
        Me.AlertControl1.LookAndFeel.SkinName = "iMaginary"
        Me.AlertControl1.LookAndFeel.UseDefaultLookAndFeel = False
        '
        'ToastNotificationsManager1
        '
        Me.ToastNotificationsManager1.ApplicationId = "70e1a3f0-8af9-450c-8446-955b2f839bb3"
        Me.ToastNotificationsManager1.ApplicationName = "TimeWatchNewDeskTop"
        Me.ToastNotificationsManager1.Notifications.AddRange(New DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties() {New DevExpress.XtraBars.ToastNotifications.ToastNotification("4f8cfe13-3e07-48f8-bddf-9d5c77387978", Nothing, "Pellentesque lacinia tellus eget volutpat", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" & _
                    "ncididunt ut labore et dolore magna aliqua.", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" & _
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.Text01)})
        '
        'TimerDataProcess
        '
        Me.TimerDataProcess.Interval = 1000
        '
        'TimerSMS
        '
        Me.TimerSMS.Interval = 60000
        '
        'TimerCloud
        '
        '
        'TimerAutoDownload
        '
        Me.TimerAutoDownload.Interval = 60000
        '
        'TimerAutpBK
        '
        Me.TimerAutpBK.Enabled = True
        Me.TimerAutpBK.Interval = 30000
        '
        'XtraMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 484)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.OfficeNavigationBar1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.TouchUIMode = DevExpress.Utils.DefaultBoolean.[False]
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraMaster"
        Me.Text = "Integrated Attendance System"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.SidePanel2.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel8.ResumeLayout(False)
        Me.SidePanel8.PerformLayout()
        Me.SidePanel7.ResumeLayout(False)
        Me.SidePanel7.PerformLayout()
        Me.SidePanel6.ResumeLayout(False)
        Me.SidePanel6.PerformLayout()
        Me.SidePanel5.ResumeLayout(False)
        Me.SidePanel5.PerformLayout()
        Me.SidePanel4.ResumeLayout(False)
        Me.SidePanel4.PerformLayout()
        Me.SidePanel3.ResumeLayout(False)
        Me.SidePanel3.PerformLayout()
        CType(Me.OfficeNavigationBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToastNotificationsManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NavigationBarItem6 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem5 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem4 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem3 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem2 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem1 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents OfficeNavigationBar1 As DevExpress.XtraBars.Navigation.OfficeNavigationBar
    Friend WithEvents NavigationBarItem8 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem7 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem9 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NavigationBarItem10 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents TimerShowClock As System.Windows.Forms.Timer
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel4 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel5 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControlTime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel6 As DevExpress.XtraEditors.SidePanel
    Public WithEvents LabelControlStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TimerReportEmail As System.Windows.Forms.Timer
    Friend WithEvents AlertControl1 As DevExpress.XtraBars.Alerter.AlertControl
    Friend WithEvents ToastNotificationsManager1 As DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager
    Friend WithEvents TimerDataProcess As System.Windows.Forms.Timer
    Friend WithEvents NavigationBarItem11 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents NavigationBarItem12 As DevExpress.XtraBars.Navigation.NavigationBarItem
    Friend WithEvents SidePanel7 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControlUser As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TimerSMS As System.Windows.Forms.Timer
    Friend WithEvents TimerCloud As System.Windows.Forms.Timer
    Friend WithEvents TimerAutoDownload As System.Windows.Forms.Timer
    Friend WithEvents TimerAutpBK As System.Windows.Forms.Timer
    Friend WithEvents SidePanel8 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControlCount As DevExpress.XtraEditors.LabelControl
End Class
